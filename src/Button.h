/*
 * Button.h
 *
 *  Created on: 28/3/2019
 *      Author: electro1
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include <stdint.h>
#define NUM_KEYS  1                     /* Number of available keys           */

/* Keys for SAME70-XPLD */
#define KEY_SW0    (1 << 0)             /* Represent PA11  (SW0) */

class Button {
public:
	Button();
	virtual ~Button();
	int32_t  Buttons_Initialize   (void);
	int32_t  Buttons_Uninitialize (void);
	uint32_t Buttons_GetState     (void);
	uint32_t Buttons_GetCount     (void);
};

#endif /* BUTTON_H_ */
