/*
 * PuertoSerie.h
 *
 *  Created on: 31/3/2019
 *      Author: sebest
 */

#ifndef PUERTOSERIE_H_
#define PUERTOSERIE_H_

#include "chip.h"
#include "usart.h"
#include "sam.h"
#include "pio.h"

class PuertoSerieSetting
{
public:
	typedef enum{
		TTY_CHRL_8_BIT = (0x3u << 6),
		TTY_CHRL_7_BIT = (0x2u << 6),
		TTY_CHRL_6_BIT = (0x1u << 6),
		TTY_CHRL_5_BIT = (0x0u << 6),
	}BIT_LENGTH;

	typedef enum{
		TTY_PARITY_EVEN = (0x0u << 9),
		TTY_PARITY_ODD = (0x1u << 9),
		TTY_PARITY_SPACE = (0x2u << 9),
		TTY_PARITY_MARK = (0x3u << 9),
		TTY_PARITY_NONE = (0x4u << 9),
		TTY_PARITY_MULTIDROP = (0x6u << 9),
	}PARITY;

	typedef enum{
		TTY_STOP_BIT_1BIT = (0x0u << 12),
		TTY_STOP_BIT_1_5BIT = (0x1u << 12),
		TTY_STOP_BIT_2BIT = (0x2u << 12),
	}STOPBIT;

	BIT_LENGTH bitLength;
	PARITY parity;
	STOPBIT stopbit;
	uint32_t baudrate;
};

class PuertoSerie {
public:
	PuertoSerie(Usart *uart, uint32_t Peripheral_ID, Pin pines,PuertoSerieSetting setting);
	virtual ~PuertoSerie();
	void sendData(uint8_t *buffer, uint32_t len);
	void EnableRxInterrupt();
private:
	Usart *port;
	//Pin pines[2];
};

#endif /* PUERTOSERIE_H_ */
