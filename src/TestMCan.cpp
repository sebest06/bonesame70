/*
 * TestMCan.cpp
 *
 *  Created on: 16 abr. 2019
 *      Author: electro1
 */


#include <stdio.h>

#include <IsoAgLib/driver/can/impl/ident_c.h>
#include <IsoAgLib/driver/can/impl/canpkg_c.h>
#include <IsoAgLib/driver/system/impl/system_c.h>
#include <IsoAgLib/hal/generic_utils/can/canfifo_c.h>
#include <IsoAgLib/hal/generic_utils/can/canutils.h>
#include <IsoAgLib/hal/hal_can.h>


#include "diag/Trace.h"

#include <TestMCan.h>


#define BITRATE_TESTCAN 250000

extern "C"
void MCAN0_INT0_Handler( void )
{
	TestMCan::getInstance()->HandleInterrup();
}

TestMCan *TestMCan::self = 0;
TestMCan::TestMCan(): CanBus(MCAN0,MCAN0_INT0_IRQn,ramBuffer,RAM_BUFFER) {
	// TODO Auto-generated constructor stub

	getCanBusConfiguer()->setStdFilterElements(8);	//Probar poner menos
	getCanBusConfiguer()->setExtFilterElements(8);		//Probar poner menos
	getCanBusConfiguer()->setRxFifo0Config(64,same70::CanBusConfig::CAN_DATA_FIELD_8_BYTES);
	getCanBusConfiguer()->setRxFifo1Config(64,same70::CanBusConfig::CAN_DATA_FIELD_8_BYTES);
	getCanBusConfiguer()->setRxBufferConfig(64,same70::CanBusConfig::CAN_DATA_FIELD_8_BYTES);
	getCanBusConfiguer()->setTxBufferFifoConfig(24,8,same70::CanBusConfig::CAN_DATA_FIELD_8_BYTES,same70::CanBusConfig::CAN_DATA_FIELD_8_BYTES);

	ConfigBus(ARM_POWER_FULL);
	setMode(ARM_CAN_MODE_INITIALIZATION);
	setBitRate(BITRATE_TESTCAN);
	setMode(ARM_CAN_MODE_NORMAL);
	configInterrupt(CAN_INTRTYPEMODE_DISABLE, MCAN_IE_HPME);
	configInterrupt(CAN_INTRTYPEMODE_ENABLE, MCAN_IE_RF0NE | MCAN_IE_RF1NE | MCAN_IE_DRXE);
	//Copio la instancia
	self = this;


	/*getFilterManager()->setStdIdFilter(0x023,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO0);
	getFilterManager()->setStdIdFilter(0x024,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO1);
	getFilterManager()->setStdDualIdFilter(0x025,0x26,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO0);
	getFilterManager()->setStdMaskIdFilter(0x01,0x055,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO0);
	getFilterManager()->setStdIdFilter(0x028,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_BUFFER,8);*/
	getFilterManager()->setStdRangeIdFilter(0x00,0xff,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO0);

	getFilterManager()->setExtMaskIdFilter(0x00,0x00,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO1);
	/*getFilterManager()->setExtIdFilter(0x024,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO0);
	getFilterManager()->setExtDualIdFilter(0x025,0x26,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO1);
	getFilterManager()->setExtMaskIdFilter(0x01,0x055,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO1);
	getFilterManager()->setExtIdFilter(0x028,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_BUFFER,4);*/
	getFilterManager()->setExtRangeIdFilter(0x00,0xffffffff,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO1);
	//getFilterManager()->setExtIdFilter(0x12345678,same70::CanBusFilterManager::FILTER_CONF_STORE_RX_FIFO1);

	//trace_printf("Total RAM: %d\n\r", getCanBusConfiguer()->getTotalRamSize());

}

TestMCan::~TestMCan() {
	// TODO Auto-generated destructor stub
}

TestMCan* TestMCan::getInstance()
{
	return self;
}

void TestMCan::InterruptHPM()
{
	//trace_puts("Se llamo a HPM\n\r");
}

void TestMCan::InterruptNewMessageFifo0()
{
	volatile int32_t index = 0;
	uint8_t data[10];
	ARM_CAN_MSG_INFO msjInfo;
	//trace_puts("Se llamo a InterruptNewMessageFifo0\n\r");
	getLastRxFifoMsg(0,&msjInfo,data);

	msjInfo.id &= 0x3fffffff;

	__IsoAgLib::CanPkg_c p( msjInfo.id, 0, msjInfo.dlc, HAL::getTime() );
	p.setUint8Data( 0, data[0]);
	p.setUint8Data( 1, data[1]);
	p.setUint8Data( 2, data[2]);
	p.setUint8Data( 3, data[3]);
	p.setUint8Data( 4, data[4]);
	p.setUint8Data( 5, data[5]);
	p.setUint8Data( 6, data[6]);
	p.setUint8Data( 7, data[7]);

	HAL::CanFifos_c::get( 0 ).push( p );


	/*	trace_printf("DataFifo0[%d]: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n\r", msjInfo.dlc,data[0],data[1],data[2],data[3],
				data[4],data[5],data[6],data[7]);*/
}
void TestMCan::InterruptNewMessageFifo1()
{
	volatile int32_t index = 0;
	uint8_t data[10];
	ARM_CAN_MSG_INFO msjInfo;
	//trace_puts("Se llamo a InterruptNewMessageFifo1\n\r");
	getLastRxFifoMsg(1,&msjInfo,data);

	msjInfo.id &= 0x3fffffff;

	__IsoAgLib::CanPkg_c p( msjInfo.id, 1, msjInfo.dlc, HAL::getTime() );
		p.setUint8Data( 0, data[0]);
		p.setUint8Data( 1, data[1]);
		p.setUint8Data( 2, data[2]);
		p.setUint8Data( 3, data[3]);
		p.setUint8Data( 4, data[4]);
		p.setUint8Data( 5, data[5]);
		p.setUint8Data( 6, data[6]);
		p.setUint8Data( 7, data[7]);

		HAL::CanFifos_c::get( 0 ).push( p );

	//		trace_printf("DataFifo1[%d]: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n\r", msjInfo.dlc,data[0],data[1],data[2],data[3],
	//				data[4],data[5],data[6],data[7]);
}

void TestMCan::InterruptNewMessageRxBuffer()
{
	int32_t index = 0;
	uint8_t data[10];
	ARM_CAN_MSG_INFO msjInfo;
	//trace_puts("Se llamo a InterruptNewMessageRxBuffer\n\r");

	index = getNewDataFromRxBuffer(&msjInfo,data);

/*
		trace_printf("DataBuffered[%d][%d]: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n\r", index,msjInfo.dlc,data[0],data[1],data[2],data[3],
				data[4],data[5],data[6],data[7]);
				*/
}

