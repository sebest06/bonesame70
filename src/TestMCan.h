/*
 * TestMCan.h
 *
 *  Created on: 16 abr. 2019
 *      Author: electro1
 */

#ifndef TESTMCAN_H_
#define TESTMCAN_H_
#include "driver/CanBus.h"
#include <stdint.h>

#define RAM_BUFFER 920//4352

class TestMCan : public same70::CanBus {
public:
	TestMCan();
	virtual ~TestMCan();
	static TestMCan * getInstance();
protected:
	void InterruptHPM();
	void InterruptNewMessageFifo0();
	void InterruptNewMessageFifo1();
	void InterruptNewMessageRxBuffer();

private:
	uint32_t ramBuffer[RAM_BUFFER];
	static TestMCan *self;

};

#endif /* TESTMCAN_H_ */
