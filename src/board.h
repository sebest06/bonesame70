#ifndef BOARD_H_
#define BOARD_H_

#include "chip.h"
#define BOARD_MCK 300000000

#define SYS_CORE_CLK SystemCoreClock /* System Clock Frequency (Core Clock) */
#define MASTER_CLK (SYS_CORE_CLK/2)


#define CHIP_REV_A 1
// <e> CAN0 (Controller Area Network) [Driver_CAN0]
// <i> Configuration settings for Driver_CAN0 in component ::CMSIS Driver:CAN
#define RTE_CAN0                         1

//   <o> CAN0 RX Pin <0=>Not Used <1=>PB3
//   <i> CAN 0 receive pin configuration
#define RTE_CAN0_RX_PIN_ID               1
#if    (RTE_CAN0_RX_PIN_ID == 0)
#define RTE_CAN0_RX_PIN                  NULL
#elif  (RTE_CAN0_RX_PIN_ID == 1)
#define RTE_CAN0_RX_PIN                  {PIO_PB3, PIOB, ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#else
#error "Invalid CAN 0 Receive Pin Configuration!"
#endif

//   <o> CAN0 TX Pin <0=>Not Used <1=>PB2
//   <i> CAN 0 transmit pin configuration
#define RTE_CAN0_TX_PIN_ID               1
#if    (RTE_CAN0_TX_PIN_ID == 0)
#define RTE_CAN0_TX_PIN                  NULL
#elif  (RTE_CAN0_TX_PIN_ID == 1)
#define RTE_CAN0_TX_PIN                  {PIO_PB2, PIOB, ID_PIOB, PIO_PERIPH_A, PIO_DEFAULT}
#else
#error "Invalid CAN 0 Transmit Pin Configuration!"
#endif

// </e>

// <e> CAN1 (Controller Area Network) [Driver_CAN1]
// <i> Configuration settings for Driver_CAN1 in component ::CMSIS Driver:CAN
#define RTE_CAN1                         0

//   <o> CAN1 RX Pin <0=>Not Used <1=>PC12 <2=>PD28
//   <i> CAN 1 receive pin configuration
#define RTE_CAN1_RX_PIN_ID               1
#if    (RTE_CAN1_RX_PIN_ID == 0)
#define RTE_CAN1_RX_PIN                  NULL
#elif  (RTE_CAN1_RX_PIN_ID == 1)
#define RTE_CAN1_RX_PIN                  {PIO_PC12, PIOC, ID_PIOC, PIO_PERIPH_C, PIO_DEFAULT}
#elif  (RTE_CAN1_RX_PIN_ID == 2)
#define RTE_CAN1_RX_PIN                  {PIO_PD28, PIOD, ID_PIOD, PIO_PERIPH_B, PIO_DEFAULT}
#else
#error "Invalid CAN 1 Receive Pin Configuration!"
#endif

//   <o> CAN1 TX Pin <0=>Not Used <1=>PC14 <2=>PD12
//   <i> CAN 1 transmit pin configuration
#define RTE_CAN1_TX_PIN_ID               1
#if    (RTE_CAN1_TX_PIN_ID == 0)
#define RTE_CAN1_TX_PIN                  NULL
#elif  (RTE_CAN1_TX_PIN_ID == 1)
#define RTE_CAN1_TX_PIN                  {PIO_PC14, PIOC, ID_PIOC, PIO_PERIPH_C, PIO_DEFAULT}
#elif  (RTE_CAN1_TX_PIN_ID == 2)
#define RTE_CAN1_TX_PIN                  {PIO_PD12, PIOD, ID_PIOD, PIO_PERIPH_B, PIO_DEFAULT}
#else
#error "Invalid CAN 1 Transmit Pin Configuration!"
#endif

#endif // BOARD_H_
