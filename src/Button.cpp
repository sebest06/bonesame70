/*
 * Button.cpp
 *
 *  Created on: 28/3/2019
 *      Author: electro1
 */

#include "Button.h"
#include "sam.h"

Button::Button() {
	// TODO Auto-generated constructor stub
	Buttons_Initialize();
}

Button::~Button() {
	// TODO Auto-generated destructor stub
}

int32_t Button::Buttons_Initialize   (void)
{
	  PMC->PMC_PCER0 = (1UL << ID_PIOA);      /* enable clock for push button     */

	  PIOA->PIO_PUER =
	  PIOA->PIO_IDR  =
	  PIOA->PIO_ODR  =
	  PIOA->PIO_PER  = (PIO_PA11);            /* Setup PA11 for user button SW0   */

	  return (0);
}

int32_t Button::Buttons_Uninitialize (void)
{
	return (0);
}

uint32_t Button::Buttons_GetState     (void)
{
	  uint32_t val = 0;

	  if (~(PIOA->PIO_PDSR) & (PIO_PA11)) {
	    val |= KEY_SW0;
	  }

	  return (val);
}

uint32_t Button::Buttons_GetCount     (void)
{
	return (NUM_KEYS);
}
