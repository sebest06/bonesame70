/*
 * PuertoSerie.cpp
 *
 *  Created on: 31/3/2019
 *      Author: sebest
 */

#include "PuertoSerie.h"
#include "board.h"

/*#define US_MR_CHRL_8_BIT (0x3u << 6)
#define US_MR_PAR_NO (0x4u << 9)
#define US_MR_NBSTOP_1_BIT (0x0u << 12)
#define US_MR_CHMODE_NORMAL (0x0u << 14)*/

PuertoSerie::PuertoSerie(Usart *uart, uint32_t Peripheral_ID, Pin pines,PuertoSerieSetting setting) {
	// TODO Auto-generated constructor stub
	PMC_EnablePeripheral(Peripheral_ID);
	PIO_Configure(&pines,1);
	uint32_t configSerie = 0;
	configSerie = setting.bitLength | setting.parity | setting.stopbit;
	USART_Configure(uart,configSerie,setting.baudrate,MASTER_CLK);
	port = uart;
}

PuertoSerie::~PuertoSerie() {
	// TODO Auto-generated destructor stub
}

void PuertoSerie::sendData(uint8_t *buffer, uint32_t len)
{
	uint32_t i = 0;
	while(i < len)
	{
		USART_PutChar(port,buffer[i]);
		i++;
	}
}

void PuertoSerie::EnableRxInterrupt()
{
	//NVIC_SetPriority (USART0_IRQn, (1UL << __NVIC_PRIO_BITS) - 1UL); /* set Priority for usart Interrupt */
	port->US_IDR = 0xffffffff;
	USART_EnableIt(port,0x01u);
	NVIC_EnableIRQ(USART0_IRQn);

	//USART_EnableIt(port,0xffu);
}


