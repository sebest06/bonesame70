/* 
* ecuProjDataStorage_c.h
*
* Created: 14/09/2016 11:22:35
* Author: lab
*/


#ifndef __ECUPROJDATASTORAGE_C_H__
#define __ECUPROJDATASTORAGE_C_H__

#include <IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtclient_c.h>
#include <IsoAgLib/comm/Part6_VirtualTerminal_Client/ivttypes.h>

#include <IsoAgLib/comm/Part5_NetworkManagement/iidentitem_c.h>
#include <IsoAgLib/comm/Part6_VirtualTerminal_Client/ivtclient_c.h>
#include <IsoAgLib/comm/Part5_NetworkManagement/iisomonitor_c.h>     // parte 5 .. MANEJO DE DIRECCIONES DUPLICADAS ...


#include <list>


	class ecuProjDataStorage_c : public IsoAgLib::iIdentDataStorage_c, public IsoAgLib::iVtClientDataStorage_c
	{
    public:
      ecuProjDataStorage_c(const uint16_t a_baseAddr);

      //! method called by IsoAgLib to retrive a formerly stored source address
      uint8_t loadSa() {
        return _loadSa();
      }

      //! method called by IsoAgLib to hand over a claimed source address for
      //! non volatile storage
      void storeSa(const uint8_t a_sa) {
        _storeSa(a_sa);
      }

      void loadDtcs( IsoAgLib::iDtcContainer_c &arc_dtcContainer )//naty
      {
        _loadDtcs(arc_dtcContainer);
      }
      void storeDtcs( const IsoAgLib::iDtcContainer_c &arc_dtcContainer ) //naty
      {
        _storeDtcs(arc_dtcContainer);
      }

      void loadPreferredVt( IsoAgLib::iIsoName_c &arc_isoname, uint8_t &arui8_boottime_s )
      {
        _loadPreferredVt(arc_isoname, arui8_boottime_s);
      }
      void storePreferredVt( const IsoAgLib::iIsoName_c &arc_isoname, uint8_t aui8_bootTime )
      {
        _storePreferredVt(arc_isoname, aui8_bootTime);
      }

	  void loadPreferredAux2Assignment( IsoAgLib::iAux2Assignment_c& assignment )
	  {

	  }

	  void storePreferredAux2Assignment( uint16_t a_functionUid, const STL_NAMESPACE::list<IsoAgLib::iAux2InputData>& a_ref_preferred_assignment )
	  {

	  }



    private:
      static const uint32_t m_eepromMagicPattern = 0x150a61b;
      static const uint8_t m_defaultSa = 0x85;
      // block size: remember, that the eeprom ecu project allocates eeprom space
      // with the size of  pattern + block size + m_eepromBlockSize
      static const uint16_t m_eepromBlockSize = 1; // one byte payload needed for sa
      static const uint16_t m_eepromSaOffset = 0;

      //! protected implementation for loading the source address
      uint8_t _loadSa();
      //! protected implementation for storing the source address
      void _storeSa(const uint8_t a_sa);

      void _loadDtcs( IsoAgLib::iDtcContainer_c &arc_dtcContainer );//naty
      void _storeDtcs( const IsoAgLib::iDtcContainer_c &arc_dtcContainer ); //naty

      void _loadPreferredVt( IsoAgLib::iIsoName_c &arc_isoname, uint8_t &arui8_boottime_s );
      void _storePreferredVt( const IsoAgLib::iIsoName_c &arc_isoname, uint8_t aui8_bootTime );

#ifdef USE_EEPROM_IO
      IsoAgLibEcuProj::EcuProjEeprom_c mc_eeprom;
#endif

}; //ecuProjDataStorage_c



#endif //__ECUPROJDATASTORAGE_C_H__
