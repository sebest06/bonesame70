/*
 * TestUsart.h
 *
 *  Created on: 3/4/2019
 *      Author: electro1
 */

#ifndef TESTUSART_H_
#define TESTUSART_H_

#include "driver/Usart.h"

class TestUsart : public same70::UsartPort {
public:
	TestUsart();
	virtual ~TestUsart();

	void UsartInterruptRxReady();


	static TestUsart * getInstance();
private:
	static TestUsart *self;
};

#endif /* TESTUSART_H_ */
