/*
 * Led.h
 *
 *  Created on: 28/3/2019
 *      Author: electro1
 */

#ifndef LED_H_
#define LED_H_


#include <stdint.h>
#include <chip.h>
#include "pio.h"

#define NUM_LEDS  (1)

/*typedef enum {
	PIN_OUTPUT = 0, PIN_INPUT
}pin_type;
*/

class Led {
public:
	Led();
	virtual ~Led();
	int32_t  LED_Initialize   (void);
	int32_t  LED_Uninitialize (void);
	int32_t  LED_On           (uint32_t num);
	int32_t  LED_Off          (uint32_t num);
	int32_t  LED_SetOut       (uint32_t val);
	uint32_t LED_GetCount     (void);

	//bool GPIO_Config(unsigned int id, Pio* ptr_pio, unsigned int pin, pin_type type);

private:
	Pin pines[NUM_LEDS];
};

#endif /* LED_H_ */
