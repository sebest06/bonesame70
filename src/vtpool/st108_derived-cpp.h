#include "st108-variables.inc"

#if defined( USE_SECTION_VT_OBJECT_POOL )
// Begin section vt_object_pool
#  pragma section . vt_object_pool
#endif

#include "st108-defines.inc"
#include "st108-attributes.inc"
#include "st108-list.inc"
#include "st108-list_attributes.inc"
#include "st108-functions.inc"

#if defined( USE_SECTION_VT_OBJECT_POOL )
// End section vt_object_pool
#  pragma section
#endif
