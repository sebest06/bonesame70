#ifndef DECL_derived_iObjectPool_tutorialDisplay_c
#define DECL_derived_iObjectPool_tutorialDisplay_c

// forward declaration
extern IsoAgLib::iVtObject_c* HUGE_MEM * all_iVtObjectLists [];

class iObjectPool_tutorialDisplay_c : public IsoAgLib::iVtClientObjectPool_c {
public:
  void initAllObjectsOnce(MULTITON_INST_PARAMETER_DEF);
  iObjectPool_tutorialDisplay_c() : IsoAgLib::iVtClientObjectPool_c (all_iVtObjectLists, 121, 0,  ObjectPoolSettings_s(iVtClientObjectPool_c::ObjectPoolVersion2, 200, 60, 47) ) {}

};

#endif
