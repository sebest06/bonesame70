IsoAgLib::iVtObject_c* HUGE_MEM all_iVtObjects [] = {
 &iVtObjectwsSt108,
 &iVtObjectfont0,
 &iVtObjectlineattributes0,
 &iVtObjectfillattributes0,
 &iVtObjectline_x,
 &iVtObjectline_y,
 &iVtObjectlbPesoAux,
 &iVtObjecttitle,
 &iVtObjectmain,
 //&iVtObjectbutton_keys,
 //&iVtObjectbtCero,
 //&iVtObjectbtMenu,
 &iVtObjectlbPeso,
 &iVtObjectlarge,
 &iVtObjectmedium,
 &iVtObjectlbInfoParcial,
 &iVtObjectlbParcial,
 &iVtObjectlbEstable,
 &iVtObjectlbFiltro,
 &iVtObjectlbUnidad,
 &iVtObjectfillback,
 &iVtObjectfontWhite,
 &iVtObjectline_y2,
 &iVtObjectlbTotal,
 &iVtObjectlbModo,
 &iVtObjectfontHighL,
 &iVtObjectlbGuardar,
 &iVtObjectfontNormal
};

IsoAgLib::iVtObject_c* HUGE_MEM * all_iVtObjectLists [] = {
  all_iVtObjects,
  NULL // indicate end of list
};
