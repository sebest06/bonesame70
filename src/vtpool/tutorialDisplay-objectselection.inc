#ifndef USE_VTOBJECT_workingset
	#define USE_VTOBJECT_workingset
#endif
#ifndef USE_VTOBJECT_datamask
	#define USE_VTOBJECT_datamask
#endif
#ifndef USE_VTOBJECT_alarmmask
	#define USE_VTOBJECT_alarmmask
#endif
#ifndef USE_VTOBJECT_container
	#define USE_VTOBJECT_container
#endif
#ifndef USE_VTOBJECT_softkeymask
	#define USE_VTOBJECT_softkeymask
#endif
#ifndef USE_VTOBJECT_key
	#define USE_VTOBJECT_key
#endif
#ifndef USE_VTOBJECT_button
	#define USE_VTOBJECT_button
#endif
#ifndef USE_VTOBJECT_inputnumber
	#define USE_VTOBJECT_inputnumber
#endif
#ifndef USE_VTOBJECT_outputstring
	#define USE_VTOBJECT_outputstring
#endif
#ifndef USE_VTOBJECT_outputnumber
	#define USE_VTOBJECT_outputnumber
#endif
#ifndef USE_VTOBJECT_rectangle
	#define USE_VTOBJECT_rectangle
#endif
#ifndef USE_VTOBJECT_ellipse
	#define USE_VTOBJECT_ellipse
#endif
#ifndef USE_VTOBJECT_polygon
	#define USE_VTOBJECT_polygon
#endif
#ifndef USE_VTOBJECT_linearbargraph
	#define USE_VTOBJECT_linearbargraph
#endif
#ifndef USE_VTOBJECT_archedbargraph
	#define USE_VTOBJECT_archedbargraph
#endif
#ifndef USE_VTOBJECT_picturegraphic
	#define USE_VTOBJECT_picturegraphic
#endif
#ifndef USE_VTOBJECT_numbervariable
	#define USE_VTOBJECT_numbervariable
#endif
#ifndef USE_VTOBJECT_fontattributes
	#define USE_VTOBJECT_fontattributes
#endif
#ifndef USE_VTOBJECT_lineattributes
	#define USE_VTOBJECT_lineattributes
#endif
#ifndef USE_VTOBJECT_fillattributes
	#define USE_VTOBJECT_fillattributes
#endif
#ifndef USE_VTOBJECT_objectpointer
	#define USE_VTOBJECT_objectpointer
#endif
#ifndef USE_VTOBJECT_macro
	#define USE_VTOBJECT_macro
#endif
