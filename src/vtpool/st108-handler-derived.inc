#ifndef DECL_derived_iObjectPool_st108_c
#define DECL_derived_iObjectPool_st108_c

// forward declaration
extern IsoAgLib::iVtObject_c* HUGE_MEM * all_iVtObjectLists [];

class iObjectPool_st108_c : public IsoAgLib::iVtClientObjectPool_c {
public:
  void initAllObjectsOnce(MULTITON_INST_PARAMETER_DEF);
  iObjectPool_st108_c() : IsoAgLib::iVtClientObjectPool_c (all_iVtObjectLists, 25, 0,  ObjectPoolSettings_s(iVtClientObjectPool_c::ObjectPoolVersion3, 320, 60, 32) ) {}

};

#endif
