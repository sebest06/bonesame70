#ifndef USE_VTOBJECT_workingset
	#define USE_VTOBJECT_workingset
#endif
#ifndef USE_VTOBJECT_datamask
	#define USE_VTOBJECT_datamask
#endif
#ifndef USE_VTOBJECT_softkeymask
	#define USE_VTOBJECT_softkeymask
#endif
#ifndef USE_VTOBJECT_key
	#define USE_VTOBJECT_key
#endif
#ifndef USE_VTOBJECT_outputstring
	#define USE_VTOBJECT_outputstring
#endif
#ifndef USE_VTOBJECT_line
	#define USE_VTOBJECT_line
#endif
#ifndef USE_VTOBJECT_rectangle
	#define USE_VTOBJECT_rectangle
#endif
#ifndef USE_VTOBJECT_fontattributes
	#define USE_VTOBJECT_fontattributes
#endif
#ifndef USE_VTOBJECT_lineattributes
	#define USE_VTOBJECT_lineattributes
#endif
#ifndef USE_VTOBJECT_fillattributes
	#define USE_VTOBJECT_fillattributes
#endif
