/********************** (C) CIFASIS ********************
		USER >>  "component_vtclient_pool/tutorialDisplay_derived-cpp.h"
 *******************************************************
 
 * This is the VT-Client-component used in the Display ECU of the ECU Project Network.
 *
 * It handles complete communication with a Virtual Terminal including
 * receiving events from it and updating of values on it.
 */

//#include "HookIsoBus.h"

#include "component_vtclient.h"

#include <IsoAgLib/comm/Part5_NetworkManagement/iisomonitor_c.h>
#include "vtpool/st108_derived-cpp.h"
//#include "vtpool/tutorialDisplay_derived-cpp.h"


extern "C"
{
	//#include "balanza.h"	
};


//extern IsoAgLibEcuProjIsobusInit::TRender *p_display;
//extern ecuProjDataStorage_c* p_dataStorage;

namespace {

/*
 * Normally the application class could directly derive from "iDisplay_c",
 * but in this ecu project we would have to use multiple inheritance to do so,
 * which is bad on some exotic embedded compilers, so we let the events be
 * forwarded from the "static tutorialPool" to our "EcuProjIsobusInit_c" "p_display".
 */
//IsoAgLibEcuProjIsobusInit::iDisplayImplementation_c sc_display;

inline uint8_t depth1Colour(uint8_t aui8_colorValue)
{
  switch (aui8_colorValue) {
  case 19: return 15;
  case 20: return 9;
  case 40: return 10;
  case 42: return 3;
  case 209: return 8;
  case 222: return 1;
  case 229: return 7;
  default:;
  }
  return aui8_colorValue;
}

inline uint8_t depth0Colour(uint8_t aui8_colorValue)
{
  switch (aui8_colorValue) {
  case 3: return 1;
  default:;
  }
  return aui8_colorValue;
}

} //namespace

// --------------------------------------------
namespace IsoAgLibEcuProjIsobusInit {

/*
 * EcuProj Display implementation
 * -------------------------------
 *
 * Here the main work is done of this component
 *
 * */
TRender::TRender(IsoAgLib::iVtClientDataStorage_c* storageHandler)
  : mp_srcHandle( 0 )
  , mp_srcIdent( 0 )
  , mui_activeMaskId( 0xFFFF ) // use 0xFFFF as default because it's not a valid Mask-ID.
{
	_storageHandler = storageHandler;
}


TRender::~TRender()
{
  if (mp_srcIdent)
  { // registered, so we need to deregister
    IsoAgLib::getIvtClientInstance().deregisterObjectPool( *mp_srcIdent );
  }
}


void
TRender::init( IsoAgLib::iIdentItem_c& ar_ident )
{
  mp_srcHandle = IsoAgLib::getIvtClientInstance().initAndRegisterObjectPool( ar_ident, *this, NULL, *_storageHandler, RegisterPoolMode_MasterToAnyVt ); //Si cambio null por un nombre me lo guarda en la sd
  
  if (mp_srcHandle)
  { // registering succeeded, store IdentItem for deregistration
    mp_srcIdent = &ar_ident;
  }
}


TRender*
TRender::getDisplayHandler()
{
  return this;
}

void TRender::eventKeyCode (uint8_t keyActivationCode, uint16_t objId, uint16_t objIdMask, uint8_t keyCode, bool wasButton)
{
	if(1 == keyActivationCode){ //BUTTON PRESSED
		switch(keyCode)
		{
			case 1:
				//calcularCero();
			break;
			case 2:
				/*if(mui_activeMaskId != iVtObjectIDmenu)
				{
					//maskChanged( iVtObjectIDmenu );
					//iVtObjectmenuEdFactCorrect.setMaxValue(1234,true,true);
					//iVtObjectmenuEdFactCorrect.setValue(1234,true,true);
					iVtObjectwsSt108.changeActiveMask( &iVtObjectmenu, false, false);
					iVtObjecttestVar.setValue(10);
				}
				else
				{
					//maskChanged( iVtObjectIDmain );
					iVtObjectwsSt108.changeActiveMask( &iVtObjectmain, false, false );
				}*/
			break;
			case 8: //PTO Engage
//			IsoAgLibEcuProjIsobusInit::EcuProjIsobusInitPto_c::togglePto();
			break;
			case 5: //Headlights
//			IsoAgLibEcuProjIsobusInit::EcuProjIsobusInitLights_c::toggleLight( keyCode );
			break;
			case 6: //Work lights
//			IsoAgLibEcuProjIsobusInit::EcuProjIsobusInitLights_c::toggleLight( keyCode );
			break;
			case 7: //Beacon light
//			IsoAgLibEcuProjIsobusInit::EcuProjIsobusInitLights_c::toggleLight( keyCode );
			break;
		}
	}

}

void TRender::handleTestValue(int dato)
{
//	char buff[20];
//	snprintf(buff,20,"%.0f",getPesoFisico());
//	iVtObjectlbPeso.setValueCopy(buff);
}

void TRender::eventNumericValue( uint16_t objId, uint8_t oneByteValue, uint32_t fourByteValue)
{
	
}


/*
 * EcuProjIsobusInit VT-Client needed implementation
 * -----------------------------------------------
 *
 * Here the incoming events from the VT-Server are handled.
 *
 */

void
TRender::eventObjectPoolUploadedSuccessfully( bool /* ab_wasLanguageUpdate */, int8_t /* ai8_languageIndex */, uint16_t /* aui16_languageCode */ )
{
  setMetaInfo();
  iVtObjectwsSt108.changeActiveMask( &iVtObjectmain, false, false ); /* make sure about the mask */
  //iVtObjectwsProyecto.changeActiveMask( &iVtObjectdmInfo, false, false );

  maskChanged( iVtObjectIDmain ); // initialize the stored current mask
  //maskChanged( iVtObjectIDdmInfo  ); // initialize the stored current mask
  
  //iVtObjectonAddress.setValue(IsoAgLib::getIisoMonitorInstance().isoMemberInd(0).nr(),false,true);

}


void
TRender::eventVtStatusMsg()
{
  //! This function is needed to keep track of the active mask
  //! as it is changed via Macros in this somponent's objectpool.

		
  if ( 0 != mp_srcHandle ) {
	 const unsigned int dimension = mp_srcHandle->getVtServerInst().getVtHardwareDimension();
    const uint16_t amId = mp_srcHandle->getVtServerInst().getVtState().dataAlarmMask; //naty rempace -> por .
    if ( amId != mui_activeMaskId )
    { // a (macro-triggered) mask change has been detected
      maskChanged( amId );
    }
  }
}


/*
 * EcuProjIsobusInit VT-Client own implementation
 * --------------------------------------------
 *
 * Here the application specific logic is handled.
 *
 */

void
TRender::maskChanged( uint16_t aui_activeMaskId )
{
  // keep track of the current mask to avoid unnecessary updates to objects in hidden masks.
  mui_activeMaskId = aui_activeMaskId;
}


void
TRender::setMetaInfo()
{
  //iVtObjectosVersion.setValueCopy( "1.1", true );
  //iVtObjectosBuildDate.setValueCopy( "HOY" , true );
}


/*
 * EcuProj Display iDisplay implementation
 * ----------------------------------------
 *
 * Here the incoming value updates are handled.
 *
 * */

//void iDisplayImplementation_c::handleNewTimeValues( uint8_t aui_h, uint8_t aui_m, uint8_t aui_s ) { p_display->handleNewTimeValues( aui_h, aui_m, aui_s ); }
//void iDisplayImplementation_c::handleNewBatteryValues( int16_t ai_voltageActual, int16_t ai_voltageTarget ) { p_display->handleNewBatteryValues( ai_voltageActual, ai_voltageTarget ); }
//void iDisplayImplementation_c::handleNewOilValues( int16_t ai_pressureActual, int16_t ai_pressureTarget ) { p_display->handleNewOilValues( ai_pressureActual, ai_pressureTarget ); }
//void iDisplayImplementation_c::handleNewLightValues( bool Headlights, bool Worklights, bool Beacon) { p_display->handleNewLightValues( Headlights, Worklights, Beacon); }
//void iDisplayImplementation_c::handleNewPto( bool ab_ptoEngaged ) { p_display->handleNewPto( ab_ptoEngaged ); }

/*void
EcuProjIsobusInit_c::handleNewTimeValues( uint8_t aui_h, uint8_t aui_m, uint8_t aui_s )
{*/
/*  if ( mui_activeMaskId == iVtObjectIDdmInfo ) {
    iVtObjectonHours.setValue( aui_h, false, true );
    iVtObjectonMinutes.setValue( aui_m, false, true );
    iVtObjectonSeconds.setValue( aui_s, false, true );
  }*/
/*}*/

//void
//EcuProjIsobusInit_c::handleNewBatteryValues( int16_t ai_voltageActual, int16_t ai_voltageTarget )
//{
//  if ( mui_activeMaskId == iVtObjectIDdmTrac || mui_activeMaskId == iVtObjectIDamVoltage) {
//    iVtObjectnvVoltage.setValue( ai_voltageActual, false, true );
//    iVtObjectnvVoltageTarget.setValue( ai_voltageTarget, false, true );
//  }
//}

//void
//EcuProjIsobusInit_c::handleNewOilValues( int16_t ai_pressureActual, int16_t ai_pressureTarget )
//{
//  if ( mui_activeMaskId == iVtObjectIDdmTrac || mui_activeMaskId == iVtObjectIDamOilPressure) {
//    iVtObjectnvPressure.setValue( ai_pressureActual, false, true );
//    iVtObjectnvPressureTarget.setValue( ai_pressureTarget, false, true );
//  }
//}

//void EcuProjIsobusInit_c::handleNewLightValues ( bool Headlights, bool Worklights, bool Beacon )
//{
//  if ( mui_activeMaskId == iVtObjectIDdmLights ) {
//    Headlights? iVtObjectctnHeadLights.show() : iVtObjectctnHeadLights.hide();
//    Worklights? iVtObjectctnWorkLights.show() : iVtObjectctnWorkLights.hide();
//		Beacon? iVtObjectctnBeacon.show() : iVtObjectctnBeacon.hide();
//  }
//}

//void
//EcuProjIsobusInit_c::handleNewPto( bool ab_ptoEngaged)
//{
//  if ( mui_activeMaskId == iVtObjectIDdmTrac ) {
//    iVtObjectopPtoEngageStatus.setValue( ab_ptoEngaged ? &iVtObjectelStatusGreen : &iVtObjectelStatusRed, false, true );
//  }
//}


/* Convert "bad" colors before handing over to default color
 * mapping. */
uint8_t TRender::convertColour(
    uint8_t aui8_colorValue,
    uint8_t aui8_colorDepth,
    IsoAgLib::iVtObject_c *p_obj,
    IsoAgLib::e_vtColour ae_whichColour)
{
  uint8_t const cui8_convertedColorStage1 = aui8_colorDepth > 1 ?
    aui8_colorValue :
    depth1Colour(aui8_colorValue);

  uint8_t const cui8_convertedColorStage2 = aui8_colorDepth > 0 ?
    cui8_convertedColorStage1 :
    depth0Colour(cui8_convertedColorStage1);

  return convertColourDefault(
      cui8_convertedColorStage2,
      aui8_colorDepth,
      p_obj,
      ae_whichColour);
}

} //namespace IsoAgLibEcuProjIsobusInit

/* eof tutorialDisplay_c */
