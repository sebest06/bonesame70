/*
 * Led.cpp
 *
 *  Created on: 28/3/2019
 *      Author: electro1
 */

#include "Led.h"
#include "sam.h"


Led::Led() {
	// TODO Auto-generated constructor stub
	LED_Initialize();
}

Led::~Led() {
	// TODO Auto-generated destructor stub
}

int32_t Led::LED_Initialize   (void)
{
	PMC_EnablePeripheral(ID_PIOC);


	pines[0].mask = PIO_PC8;
	pines[0].pio = PIOC;
	pines[0].attribute = 0;
	pines[0].type = PIO_OUTPUT_0;

	PIO_Configure(pines,1);

	/* Setup PC8 for LED                */


	  LED_SetOut (0);                         /* switch LEDs off                  */

	  return 0;
}

int32_t Led::LED_Uninitialize (void)
{
	return 0;
}

int32_t Led::LED_On           (uint32_t num)
{
	if (num < NUM_LEDS) {
		if (num == 0) PIO_Clear(&pines[num]);
		return 1;
	}

	return 0;
}

int32_t Led::LED_Off          (uint32_t num)
{
	  if (num < NUM_LEDS) {
			if (num == 0) PIO_Set(&pines[num]);
			return 1;
	  }

	  return 0;
}

int32_t Led::LED_SetOut       (uint32_t value)
{
	  int i;

	  for (i = 0; i < NUM_LEDS; i++) {
	    if (value & (1<<i)) {
	      LED_On (i);
	    } else {
	      LED_Off(i);
	    }
	  }
	  return 0;
}

uint32_t Led::LED_GetCount     (void)
{
	return NUM_LEDS;
}
