/*
 * TestUsart.cpp
 *
 *  Created on: 3/4/2019
 *      Author: electro1
 */

#include "TestUsart.h"
#include "board.h"
#include "pio.h"
#include "driver/Usart.h"

TestUsart *TestUsart::self = 0;
TestUsart::TestUsart() : UsartPort(USART0,USART0_IRQn,MASTER_CLK) {

	//CONFIGURACION DE PINES DEL PUERTO!

	PMC_EnablePeripheral(ID_USART0);
	Pin SerialPortPin;
	SerialPortPin.mask = PIO_PB0 | PIO_PB1;
	SerialPortPin.pio = PIOB;
	SerialPortPin.attribute = PIO_DEFAULT;
	SerialPortPin.type = PIO_PERIPH_C;
	PIO_Configure(&SerialPortPin,1);

	//Configuracion del puerto
	same70::UsartSetting configSerialPort;
	configSerialPort.baudrate = 115200;
	configSerialPort.bitLength = same70::UsartSetting::TTY_CHRL_8_BIT;
	configSerialPort.parity = same70::UsartSetting::TTY_PARITY_NONE;
	configSerialPort.stopbit = same70::UsartSetting::TTY_STOP_BIT_1BIT;
	UsartConfig(configSerialPort);

	//Copio la instancia
	self = this;

	//Habilitar interrupciones
	UsartEnableInterrup(same70::TTY_INTERRUPT_RXRDY);
}

TestUsart::~TestUsart() {
	// TODO Auto-generated destructor stub
}

TestUsart* TestUsart::getInstance()
{
	return self;
}

void TestUsart::UsartInterruptRxReady()
{
	uint8_t dato;
	UsartGetByte(&dato);
	UsartWriteByte(dato);
}


extern "C"
void USART0_Handler( void )
{
	TestUsart::getInstance()->UsartHandleInterrup();
}
