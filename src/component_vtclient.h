/********************** (C) CIFASIS ********************
		USER >>  "component_vtclient_pool/tutorialDisplay_derived-h.h"
 *******************************************************
 */

#ifndef COMPONENT_VTCLIENT_H
#define COMPONENT_VTCLIENT_H

#include <IsoAgLib/scheduler/ischedulertask_c.h>
#include <IsoAgLib/comm/Part6_VirtualTerminal_Client/ivtclientconnection_c.h>
#include <IsoAgLib/comm/Part5_NetworkManagement/iidentitem_c.h>
#include <IsoAgLib/comm/Part5_NetworkManagement/iisomonitor_c.h>     // parte 5 .. MANEJO DE DIRECCIONES DUPLICADAS ...
#include <IsoAgLib/comm/Part5_NetworkManagement/iisoname_c.h>
#include <IsoAgLib/comm/Part5_NetworkManagement/impl/isoname_c.h>

#include "vtpool/st108_derived-h.h"
//#include "vtpool/tutorialDisplay_derived-h.h"


namespace IsoAgLibEcuProjIsobusInit {

  class TRender : public iObjectPool_st108_c {
	  //class TRender : public iObjectPool_tutorialDisplay_c {
    public:
      TRender(IsoAgLib::iVtClientDataStorage_c* storageHandler);
      ~TRender();

      void init( IsoAgLib::iIdentItem_c& ar_ident );

      //! Initializes run-time MetaInformation for this ECU
      void setMetaInfo();
      //! Notify this component on the current active mask
      void maskChanged( uint16_t aui_activeMaskId );

      TRender* getDisplayHandler();

      /* iObjectPool_tutorialDisplay_c */
      virtual void eventKeyCode (uint8_t keyActivationCode, uint16_t objId, uint16_t objIdMask, uint8_t keyCode, bool wasButton);
      virtual void eventStringValue( uint16_t , uint8_t , StreamInput_c& , uint8_t, bool , bool ) { /* not used in this simple ecu proj */ }
      virtual void eventNumericValue( uint16_t objId, uint8_t oneByteValue, uint32_t fourByteValue);
      virtual void eventVtStatusMsg();
      virtual void eventObjectPoolUploadedSuccessfully( bool ab_wasLanguageUpdate, int8_t ai8_languageIndex, uint16_t aui16_languageCode );
      virtual void eventEnterSafeState() { /* not used in this simple ecu proj */ }
      virtual uint8_t convertColour(uint8_t aui8_colorValue, uint8_t aui8_colorDepth, IsoAgLib::iVtObject_c *p_obj, IsoAgLib::e_vtColour ae_whichColour);

      /* iDisplayImplementation_c */
//      virtual void handleNewTimeValues( uint8_t aui_h, uint8_t aui_m, uint8_t aui_s );
//      virtual void handleNewBatteryValues( int16_t ai_voltageActual, int16_t ai_voltageTarget );
//      virtual void handleNewOilValues( int16_t ai_pressureActual, int16_t ai_pressureTarget );
//			virtual void handleNewLightValues ( bool Headlights, bool Worklights, bool Beacon );
//      virtual void handleNewPto( bool ab_Engage );
		void handleTestValue(int dato);
    private:
      IsoAgLib::iVtClientConnection_c* mp_srcHandle;
      IsoAgLib::iIdentItem_c* mp_srcIdent;

      uint16_t mui_activeMaskId;
	  IsoAgLib::iVtClientDataStorage_c* _storageHandler;
  };

} /* namespace IsoAgLibEcuProjIsobusInit */

#endif
/* eof */
