/*
 * This file is part of the µOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2014 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <sam.h>

#include <device/microchip/pio.h>

#include "system_same70.h"
#include "diag/Trace.h"
#include "driver/Timer.h"
#include "Led.h"
#include "Button.h"
#include "TestUsart.h"

#include "mcan.h"

#include "TestMCan.h"


#include <IsoAgLib/isoaglib_config.h>   //** IMPORTANTE INCLUIR  PARA QUE LLAME A LAS LIBRERIAS ANTES CONFIGURADAS ..
#include <IsoAgLib/util/iassert.h> //**  INCLUIR

#include <cstdlib>


#include <IsoAgLib/driver/system/isystem_c.h>    // IPORTANTE AGREGAR ESTAS LIB
#include <IsoAgLib/scheduler/ischeduler_c.h>
#include <IsoAgLib/util/iliberr_c.h>
#include <IsoAgLib/util/impl/bitfieldwrapper_c.h>

#include "component_vtclient.h"


/* If tractor stuff is present shutdown when keyswitch is reported
 * of via iTrackGeneral_c. Otherwise use switchedOn function
 * implemented in the platforms HAL.
 * To use the ISOBUS keyswitch information make sure you added
 *
 * PRJ_TRACTOR_GENERAL=1
 * PRJ_TRACTOR_MOVE=1
 *
 * to your config file. */
#if defined( USE_TRACTOR_GENERAL ) && defined( USE_TRACTOR_MOVE )   // ESTO ES PARA HABLAR CON EL TRACTOR
#include <IsoAgLib/comm/Part7_ApplicationLayer/itracgeneral_c.h>
#include <IsoAgLib/comm/Part7_ApplicationLayer/itracmove_c.h>
#endif

#include <IsoAgLib/comm/iisobus_c.h>
#include <IsoAgLib/comm/Part5_NetworkManagement/iidentitem_c.h>
#include <IsoAgLib/comm/Part5_NetworkManagement/iisomonitor_c.h>     // parte 5 .. MANEJO DE DIRECCIONES DUPLICADAS ...
#include "ecuProjDataStorage_c.h"




/* ISO identification */
static const uint16_t scui16_manfCode = 0x7FF;
static const uint32_t scui32_serNo = 0x22;
static const uint8_t scui8_devClass = 5;
static const uint8_t scui8_devClassInst = 0;
static const uint8_t scui8_ecuInst = 0;
static const uint8_t scui8_func = 25;
static const uint8_t scui8_funcInst = 1;
static const uint8_t scui8_indGroup = 0x2;
static const uint8_t scui8_selfConf = 0x1;

static const uint16_t scui16_eepromBaseAddress = 0x180;



// ----------------------------------------------------------------------------
//
// Print a greeting message on the trace device and enter a loop
// to count seconds.
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the STDOUT output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//
// ----------------------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


IsoAgLib::iIdentItem_c* p_ident;
ecuProjDataStorage_c* p_dataStorage;
IsoAgLibEcuProjIsobusInit::TRender* p_display = 0;


namespace IsoAgLibEcuProj {

	/** isobus bus number - specifies the can channel connected to the ISOBUS system */
	static const unsigned char scui_isoBusNumber = 0;

	/* channel number and baud rate of the internam bus (used in project tecu and internal can) */
	static const unsigned char scui_internalBusNumber = 1;

	static const char scp_isoaglibVersion[] = { '2','.','5','.','0' };   // VERSION DE LA LIBRERIA ISOAGLIB ...
	static const char scp_tutorialVersion[] = { '0','.','2','.','2' };

	/* proprietary can timing */
	static const unsigned int scui_propCanTimePeriod = 500;
	static const float scf_propCanEarlierTimeFactor = 0.75;
	static const float scf_propCanLatestTimeFactor = 0.5;

	/* gps sensor period timing */
	static const unsigned int scui_gpsSensorTimePeriod = 1000;
	static const float scf_gpsSensorEarlierTimeFactor = 0.75;
	static const float scf_gpsSensorLatestTimeFactor = 0.5;
}


bool applicationInit();
bool applicationLoop( int32_t maxLoopTime );
bool applicationClose();

#define MAXLOOPTIME -1  // MAX time (ms) allowed per loop so we can trigger the watchdog

bool initecu();
class ErrorHandler_c : public IsoAgLib::iErrorObserver_c
{
	virtual void onNonFatalError (IsoAgLib::iLibErr_c::TypeNonFatal_en type, int instance)
	{
		#if DEBUG_REGISTERERROR
			INTERNAL_DEBUG_DEVICE << "non fatal error: " << type << "inst: " << instance << INTERNAL_DEBUG_DEVICE_ENDL
		#else
			(void) type;
			(void) instance;
		#endif
	}
};
ErrorHandler_c sc_errorHandle;

int
main (int argc, char* argv[])
{

	WDT->WDT_MR = WDT_MR_WDDIS;

	/* Configure the system clock to 300 MHz */
	SystemCoreClockUpdate();

	/* Enable data and instruction caches */
	//SCB_EnableDCache(); //Lo comento porque no me permite debugear, aunque tampoco verifique que anduviera bien con esto.
	//SCB_EnableICache(); //Lo comento para probar, pero en principio lo puedo dejar.

  // Normally at this stage most of the microcontroller subsystems, including
  // the clock, were initialised by the CMSIS SystemInit() function invoked
  // from the startup file, before calling main().
  // (see system/src/cortexm/_initialize_hardware.c)
  // If further initialisations are required, customise __initialize_hardware()
  // or add the additional initialisation here, for example:
  //
  // HAL_Init();

  // In this sample the SystemInit() function is just a placeholder,
  // if you do not add the real one, the clock will remain configured with
  // the reset value, usually a relatively low speed RC clock (8-12MHz).

  // Send a greeting to the trace device (skipped on Release).
	//Si uso el trace despues me anda solo cuando lo debugeo (OJO)
  //trace_puts("Hello ARM World!");

  // At this stage the system clock should have already been configured
  // at high speed.
  //trace_printf("System clock: %u Hz\n", SystemCoreClock);

  /*int16_t i = 1;
     int8_t *p = (int8_t *) &i;

     if (p[0] == 1) trace_puts("Little Endian\n");
     else           trace_puts("Big Endian\n");


     union {
            short s;
            char c[sizeof(short)];
        } un;
        un.s = 0x0102;
        if(sizeof(short) == 2)
        {
            if(un.c[0] == 1 && un.c[1] == 2)
            	trace_puts("big-endian\n");
            else if(un.c[0] == 2 && un.c[1] == 1)
            	trace_puts("little-endian\n");
            else
            	trace_puts("unknown\n");
        }
        else
        {
        	trace_printf("sizeof(short) = %d\n", sizeof(short));
        }
*/
  Timer timer;
  timer.start();

  TestUsart usart1;

  Led led;
  Button button;

  IsoAgLib::getIsystemInstance().init();

  // Init Scheduler
  IsoAgLib::getISchedulerInstance().init( &sc_errorHandle );
  initecu();

  const bool initialized = 1;//applicationInit();
  	//isoaglib_assert( initialized ); ( void )initialized;
  	// Keep it running...
  while ( applicationLoop( MAXLOOPTIME ) );
  // Cierro Aplicacion
  const bool closed = applicationClose();

  while(1)
  {

  }
  // Infinite loop
  /*while (1)
    {
      timer.sleep(Timer::FREQUENCY_HZ);

      //serie.sendData(dato,4);
      if(button.Buttons_GetState())
      {

      }else
      {
    	  led.LED_SetOut(seconds%2);
    	  ++seconds;
      }

      // Count seconds on the trace device.
      //trace_printf ("Second %d\n", seconds);
    }*/
  // Infinite loop, never return.
}

bool applicationLoop( int32_t maxLoopTime )
{
	/*
	#if defined( USE_TRACTOR_GENERAL ) && defined( USE_TRACTOR_MOVE ) && ! defined( TUTORIAL_IGNORE_KEY_SWITCH )
	IsoAgLib::iTracGeneral_c &rc_tracGeneral = IsoAgLib::getITracGeneralInstance();
	if( IsoAgLib::IsoInactive == rc_tracGeneral.keySwitch() )
	#else
	if( !IsoAgLib::iSystem_c::switchedOn() )
	#endif
	return false;
*/
	const int32_t startTime = IsoAgLib::iSystem_c::getTime();

	int32_t waitTime = IsoAgLib::getISchedulerInstance().timeEvent();

	if( waitTime > 0 )
	{
		const int32_t nowTime = IsoAgLib::iSystem_c::getTime();
		const int32_t nextTime = nowTime + waitTime;
		if( ( maxLoopTime >= 0 ) // do limit sleeping?
		&& ( nextTime > startTime + maxLoopTime ) )
		{
			waitTime = startTime + maxLoopTime - nowTime;
		}
		if( waitTime > 0 )
		{
			IsoAgLib::getISchedulerInstance().waitUntilCanReceiveOrTimeout( waitTime );
		}
	}

	return true;
}

// --------------------------------------------
// APPLICATION CLOSE --------------------------
// --------------------------------------------
bool applicationClose()
{
	// --------------------------------------------
	/// Shutdown Application
	//if( !ecuProjClose() )
	//return false;
	// --------------------------------------------
	//delete p_display;
	delete p_dataStorage;
	delete p_ident;
	//return true;
return true;
}


bool initecu()
{
	if ( ! IsoAgLib::getIIsoBusInstance().init( IsoAgLibEcuProj::scui_isoBusNumber ) ) {
		return false;
	}




	p_dataStorage = new ecuProjDataStorage_c( scui16_eepromBaseAddress );   // CREAR LA CLASE P_DATASTORAGE TODO:

	p_ident = new IsoAgLib::iIdentItem_c();   // CREA UN NUEVO OBJETO ISOAGLIB

	const IsoAgLib::iIsoName_c c_isoname (
	scui8_selfConf,
	scui8_indGroup,
	scui8_devClass,
	scui8_devClassInst,
	scui8_func,
	scui16_manfCode,
	scui32_serNo,
	scui8_funcInst,
	scui8_ecuInst );

	p_ident->init( c_isoname, *p_dataStorage, 0, NULL, true );

	p_ident->setEcuIdentification( "PartNr T", "Serial 127", "Frontside", "PD Supplier", "cifasis", "1.0.0"); // dummy values
	p_ident->setSwIdentification( "IsoAgLib Data Sink ECU Tutorial*" );
	p_ident->setCertificationData(
	2009, // certification year
	IsoAgLib::CertificationRevisionNotAvailable,
	IsoAgLib::CertificationLabTypeNotAvailable,
	42, // dummy laboratory id
	8); // dummy reference number

	if ( ! IsoAgLib::getIisoMonitorInstance().registerIdentItem( *p_ident ) ) {
		return false;
	}

	p_display = new IsoAgLibEcuProjIsobusInit::TRender(p_dataStorage);


	p_display->init( *p_ident );

	return true;
}


#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
