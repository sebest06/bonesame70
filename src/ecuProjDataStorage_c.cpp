/* 
* ecuProjDataStorage_c.cpp
*
* Created: 14/09/2016 11:22:35
* Author: lab
*/


#include "ecuProjDataStorage_c.h"

#include <IsoAgLib/isoaglib_config.h>

	// default constructor
  ecuProjDataStorage_c::ecuProjDataStorage_c(const uint16_t a_baseAddr) {
	  #ifdef USE_EEPROM_IO
	  mc_eeprom.init(a_baseAddr, m_eepromBlockSize, m_eepromMagicPattern);
	  #else
	  (void)a_baseAddr;
	  #endif
  }

  uint8_t ecuProjDataStorage_c::_loadSa() {
	  #ifdef USE_EEPROM_IO
	  uint8_t sa = m_defaultSa;
	  if (mc_eeprom.valid() && mc_eeprom.read(m_eepromSaOffset, sa)) {
		  return sa;
	  }
	  #endif
	  return m_defaultSa;
  }

  void ecuProjDataStorage_c::_storeSa(const uint8_t a_sa) {
	  #ifdef USE_EEPROM_IO
	  mc_eeprom.write(m_eepromSaOffset, a_sa);
	  mc_eeprom.setValid();
	  #else
	  (void)a_sa;
	  #endif
  }

  void ecuProjDataStorage_c::_loadDtcs( IsoAgLib::iDtcContainer_c&  ) //naty
  {
	  // @TODO load DTCs
  }

  void ecuProjDataStorage_c::_storeDtcs( const IsoAgLib::iDtcContainer_c&  ) //naty
  {
	  // @TODO save DTCs
  }

  void ecuProjDataStorage_c::_loadPreferredVt( IsoAgLib::iIsoName_c& , uint8_t&  )
  {
	  // @TODO load Preferred VT infos
  }

  void ecuProjDataStorage_c::_storePreferredVt( const IsoAgLib::iIsoName_c& , uint8_t  )
  {
	  // @TODO save Preferred VT infos
  }
