/*
  can_driver_sys.cpp: CAN interface between BIOS/OS and IsoAgLib to
    concentrate CAN handling abstraction within one module

  (C) Copyright 2009 - 2019 by OSB AG

  See the repository-log for details on the authors and file-history.
  (Repository information can be found at <http://isoaglib.com/download>)

  Usage under Commercial License:
  Licensees with a valid commercial license may use this file
  according to their commercial license agreement. (To obtain a
  commercial license contact OSB AG via <http://isoaglib.com/en/contact>)

  Usage under GNU General Public License with exceptions for ISOAgLib:
  Alternatively (if not holding a valid commercial license)
  use, modification and distribution are subject to the GNU General
  Public License with exceptions for ISOAgLib. (See accompanying
  file LICENSE.txt or copy at <http://isoaglib.com/download/license>)
*/


#include "../typedef.h"
#include "../config.h"
#include "../errcodes.h"
#include <stdio.h>

#include <IsoAgLib/driver/can/impl/ident_c.h>
#include <IsoAgLib/driver/can/impl/canpkg_c.h>
#include <IsoAgLib/driver/system/impl/system_c.h>
#include <IsoAgLib/hal/generic_utils/can/canfifo_c.h>
#include <IsoAgLib/hal/generic_utils/can/canutils.h>
#include <IsoAgLib/hal/hal_can.h>

#include "device/microchip/pio.h"
#include "driver/CanBus.h"
#include "TestMCan.h"

namespace __HAL {

  bool canConfigMsgObj( unsigned channel, unsigned msgObj, const __IsoAgLib::Ident_c& arc_ident, bool tx ) {

/*    configData.dwId = arc_ident.ident();
    configData.bXtd = ( arc_ident.identType() == __IsoAgLib::Ident_c::ExtendedIdent ) ? TRUE : FALSE;
    configData.wPause = 0;

    if ( tx ) {
      configData.bMsgType = TX;
      configData.bTimeStamped = FALSE;
      configData.pfIrqFunction = 0;
      configData.wNumberMsgs = configTxBufferSize;
    } else {
      configData.bMsgType = RX;
      configData.bTimeStamped = TRUE;
      configData.pfIrqFunction = rxIrq;
      configData.wNumberMsgs = configRxBufferSize;
    }

    return ( C_NO_ERR == config_can_obj( channel, msgObj, &configData ) );
    */
	  return true;
  }
}


namespace HAL {

static TestMCan *canPort;

  bool canInit( unsigned channel, unsigned baudrate ) {

	  PMC_EnablePeripheral(ID_MCAN0);
	    Pin CanPin;
	    CanPin.mask = PIO_PB2 | PIO_PB3;
	    CanPin.pio = PIOB;
	    CanPin.attribute = PIO_DEFAULT;
	    CanPin.type = PIO_PERIPH_A;
	    PIO_Configure(&CanPin,1);


	    canPort = new TestMCan();

	    /*ARM_CAN_MSG_INFO msjInfo;
	    msjInfo.dlc = 6;
	    msjInfo.id = 0x12345678 | ARM_CAN_ID_IDE_Msk;
	    msjInfo.rtr = 0;
	    msjInfo.brs = 0;
	    msjInfo.esi = 0;

	    uint8_t data[9] = {0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c};

	    for(int i = 0; i < 64; i++)
	    {
	  	  data[0] = i;
	  	  canPort->sendMsg(msjInfo,data);
	  	  while(canPort->isTxFifoFull()){}
	    }

	    data[1] = 0xaa;

	    canPort->sendMsgBuffered(msjInfo,data,0);

	    data[1] = 0x55;
	    canPort->sendMsgBuffered(msjInfo,data,1);

	    int seconds = 0;
	    uint8_t dato[5] = {"OK\r\n"};
*/
    return true;
  }


  bool canClose( unsigned channel ) {
    //return ( C_NO_ERR == __HAL::close_can( channel ) );
	  return true;
  }


  bool canState( unsigned channel, canState_t& state ) {
    state = e_canNoError;
    return true;
  }


  bool canTxSend( unsigned channel, const __IsoAgLib::CanPkg_c& msg ) {
    /*msg.getData( __HAL::sendData.dwId, __HAL::sendData.bXtd, __HAL::sendData.bDlc, __HAL::sendData.abData );
    return ( C_NO_ERR == __HAL::send_can_msg( channel, __HAL::configTxMsgObjNum, &__HAL::sendData ) );*/

	  ARM_CAN_MSG_INFO msjInfo;
	  uint32_t id;
	  uint8_t bxtd;
	  uint8_t len;
	  uint8_t data[9];
	  msg.getData(id,bxtd,len,data);
	  //msg.getData(msjInfo.id,bxtd,len,data);

	  msjInfo.id = id;
	  msjInfo.dlc = len;

	  if(bxtd)
	  {
		  msjInfo.id |= ARM_CAN_ID_IDE_Msk;
	  }

	  msjInfo.rtr = 0;
	  msjInfo.brs = 0;
	  msjInfo.esi = 0;


	  canPort->sendMsg(msjInfo,data);
	  while(canPort->isTxFifoFull()){}

	  return true;
  }


  void canRxPoll( unsigned ) {
    /* nothing here - irq driven... */
  }


  bool canRxWait( unsigned timeout_ms ) {

    const ecutime_t endTime_ms = __IsoAgLib::System_c::getTime() + timeout_ms;

    while ( __IsoAgLib::System_c::getTime() < endTime_ms ) {
      for ( unsigned int c = 0; c < ( HAL_CAN_MAX_BUS_NR + 1 ); ++c ) {
        if ( ! HAL::CanFifos_c::get( c ).empty() ) {
          return true;
        }
      }
    }
    return false;
  }


  int canTxQueueFree( unsigned channel ) {
	  //TODO: REVISAR ACA!
    /*const int r = __HAL::get_can_msg_buf_count( channel, __HAL::configTxMsgObjNum );
    if( r >= 0 ) {
      return ( __HAL::configTxBufferSize - r );
    }*/
    return -1;

  }

  void defineRxFilter( unsigned, bool, uint32_t, uint32_t ) {}
  void deleteRxFilter( unsigned, bool, uint32_t, uint32_t ) {}

}


// eof
