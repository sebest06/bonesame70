APP=SAME70
VERBOSE=y
OPT = g

#CROSS TOOLCHAIN PATH
TOOLCHAIN_PATH = /home/electro1/ciaa/gcc-arm-none-eabi-8-2018-q4-major-linux/gcc-arm-none-eabi-8-2018-q4-major/bin

#PREFIJO DE BINARIOS DEL TOOLCHAIN
TOOLCHAIN_PREFIX = arm-none-eabi-

OOCD_PATH = /home/electro1/ciaa/openocd-0.10.0/src

#CARPETA DE SALIDA PARA LOS BINARIOS
OUT=bin

#MODULOS FUENTES
MODULES_SOURCES = src system/src/cortexm system/src/device system/src/device/driver system/src/device/microchip system/src/diag system/src/newlib xgpl_src xgpl_src/IsoAgLib

#MODULOS CABECERAS
MODULES_HEADERS = src system/include system/include/arm system/include/cmsis system/include/cmsis/driver system/include/device system/include/device/component  system/include/device/driver system/include/device/instance system/include/device/microchip system/include/device/pio xgpl_src xgpl_src/IsoAgLib src/vtpool

#SYMBOLOS PARA CADA HERRAMIENTA C,CPP,AS
CC_DEFINES = DEBUG OS_USE_TRACE_SEMIHOSTING_DEBUG __SAME70Q21B__ TRACE
CPP_DEFINES = DEBUG OS_USE_TRACE_SEMIHOSTING_DEBUG __SAME70Q21B__ TRACE
AS_DEFINES= DEBUG OS_USE_TRACE_SEMIHOSTING_DEBUG TRACE

#FLAGS DE LA ARQUITECTURA
ARCH_FLAGS=-mcpu=cortex-m7 -mthumb 
ifeq ($(USE_FPU),y)
ARCH_FLAGS+=-mfloat-abi=hard -mfpu=fpv4-sp-d16
endif

#SCRIPTS PARA EL LINKER
LD_SCRIPT = mem.ld libs.ld sections.ld

#PATH DE LOS SCRIPTS DEL LINKER
LD_SCRIPT_PATH = ldscripts

#NOMBRES PARA LOS ARCHIVOS DE SALIDA
TARGET=$(OUT)/$(APP).elf
TARGET_BIN=$(basename $(TARGET)).bin
TARGET_LST=$(basename $(TARGET)).lst
TARGET_MAP=$(basename $(TARGET)).map

#HERRAMIENTAS
CROSS = ${TOOLCHAIN_PATH}/${TOOLCHAIN_PREFIX}
CC=$(CROSS)gcc
CPP=$(CROSS)g++
AS=$(CROSS)gcc
LD=$(CROSS)g++
SIZE=$(CROSS)size
LIST=$(CROSS)objdump -xdS
OBJCOPY=$(CROSS)objcopy
GDB=$(CROSS)gdb
OOCD=$(OOCD_PATH)/openocd
OOCD_SCRIPT?=/home/electro1/ciaa/atmel_same70_xplained.cfg

ifeq ($(VERBOSE),y)
Q=
else
Q=@
endif

#ACONDICIONADORES, NO EDITAR
_CC_DEFINES=$(foreach m, $(CC_DEFINES), -D$(m))
_CPP_DEFINES=$(foreach m, $(CPP_DEFINES), -D$(m))
_AS_DEFINES=$(foreach m, $(AS_DEFINES), -D$(m))
_LD_SCRIPT=$(foreach m, $(LD_SCRIPT), -T$(m))

_C_SRC=$(foreach m, $(MODULES_SOURCES), $(wildcard $(m)/*.c))
_CPP_SRC=$(foreach m, $(MODULES_SOURCES), $(wildcard $(m)/*.cpp))
_AS_SRC=$(foreach m, $(MODULES_SOURCES), $(wildcard $(m)/*.S))

_INCLUDES=$(foreach m, $(MODULES_HEADERS), -I$(m))

#ISOAGLIB PROJECT GENERATOR OUTPUT
_CPP_SRC += xgpl_src/supplementary_driver/driver/datastreams/volatilememorywithsize_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/scheduler/impl/schedulertask_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/scheduler/impl/scheduler_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/driver/can/impl/canpkg_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/driver/can/impl/canio_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/driver/can/impl/filterbox_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/driver/can/impl/ident_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/driver/system/impl/system_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/chunk_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/multisendpkg_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/sendstream_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/canpkgext_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/streamchunk_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/multireceive_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/multisend_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part3_DataLink/impl/stream_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/iproprietarybus_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/ivtclientobjectpool_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtserverinstance_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtclientconnection_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectmacro_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectcolourmap_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectauxiliaryinput_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectoutputlist_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/uploadpoolstate_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectnumbervariable_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectline_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectarchedbargraph_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectlinearbargraph_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectfontattributes_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectoutputnumber_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectfillattributes_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/commandhandler_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectpolygon_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/aux2functions_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectpicturegraphic_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectstring_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/sendupload_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectinputnumber_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtclient_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectlineattributes_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectinputstring_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectinputboolean_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectrectangle_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobject_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectoutputstring_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/objectpoolstreamer_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectauxiliaryfunction2_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectdatamask_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectobjectpointer_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectbutton_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectkey_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectworkingset_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectalarmmask_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectinputattributes_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/multiplevt_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectellipse_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectinputlist_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectauxiliaryinput2_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/aux2inputs_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtservermanager_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectstringvariable_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectauxiliaryfunction_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectmeter_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectcontainer_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectauxiliarypointer_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/impl/vtobjectsoftkeymask_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part6_VirtualTerminal_Client/ivtobject_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/impl/isobus_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/isorequestpgn_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/baseitem_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/istate_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/isofilterbox_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/isoname_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/isofiltermanager_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/identitem_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/isomonitor_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part5_NetworkManagement/impl/isoitem_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part12_DiagnosticsServices/impl/diagnosticpgnhandler_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part12_DiagnosticsServices/impl/dtccontainer_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part12_DiagnosticsServices/impl/diagnosticfunctionalities_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/comm/Part12_DiagnosticsServices/impl/diagnosticsservices_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/util/iliberr_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/util/impl/util_funcs.cpp
_CPP_SRC += xgpl_src/IsoAgLib/util/impl/flexiblebytestrings.cpp
_CPP_SRC += xgpl_src/IsoAgLib/util/iassert.cpp
_CPP_SRC += xgpl_src/IsoAgLib/hal/generic_utils/can/canutils.cpp
_CPP_SRC += xgpl_src/IsoAgLib/hal/generic_utils/can/canfifo_c.cpp
_CPP_SRC += xgpl_src/IsoAgLib/hal/generic_utils/system/ThreadWrapper_pthread.cpp
_CPP_SRC += xgpl_src/IsoAgLib/hal/same70/can/can_driver_sys.cpp
_CPP_SRC += xgpl_src/IsoAgLib/hal/same70/system/system_target_extensions.cpp


#FLAGS PARA EL LINKER
LDFLAGS=$(ARCH_FLAGS) $(_LD_SCRIPT) -nostartfiles -Xlinker --gc-sections -L$(LD_SCRIPT_PATH) -Wl,-Map=$(TARGET_MAP) -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants  -g3 --specs=nano.specs


CFLAGS=$(ARCH_FLAGS) $(_INCLUDES) $(_CC_DEFINES) -O$(OPT) -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -g3 -std=gnu11

CPPFLAGS=$(ARCH_FLAGS) $(_INCLUDES) $(_CPP_DEFINES) -O$(OPT) -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -g3 -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics



#OBJETOS Y DEPENDENCIAS
OBJECTS=$(addprefix $(OUT)/, $(_C_SRC:.c=.o) $(_CPP_SRC:.cpp=.o) $(_AS_SRC:.S=.o))
DEPS=$(_C_SRC:.c=.d) $(_CPP_SRC:.cpp=.d)


all: $(TARGET) $(TARGET_BIN) $(TARGET_LST) size

help:
	@echo $(OBJECTS)

-include $(DEPS)

$(OUT)/%.o: %.cpp
	@echo CPP $<
	@mkdir -p $(dir $@)
	$(Q)$(CPP) -MMD -MP -MF"$@.d" -MT"$@" $(CPPFLAGS) -c -o $@ $<

$(OUT)/%.o: %.c
	@echo CC $<
	@mkdir -p $(dir $@)
	$(Q)$(CC) -MMD -MP -MF"$@.d" -MT"$@" $(CFLAGS) -c -o $@ $<

$(OUT)/%.o: %.S
	@echo AS $<
	@mkdir -p $(dir $@)
	$(Q)$(AS) -MMD $(CFLAGS) -c -o $@ $<

$(TARGET): $(OBJECTS)
	@echo LD $@
	@mkdir -p $(dir $@)
	$(Q)$(LD) $(LDFLAGS) -o $@ $(OBJECTS)

$(TARGET_BIN): $(TARGET)
	@echo BIN
	@mkdir -p $(dir $@)
	$(Q)$(OBJCOPY) -v -O binary $< $@

$(TARGET_LST): $(TARGET)
	@echo LIST
	@mkdir -p $(dir $@)
	$(Q)$(LIST) $< > $@

size: $(TARGET)
	$(Q)$(SIZE) $<

program: $(TARGET_BIN)
	@echo PROG
	$(Q)$(OOCD) -f $(OOCD_SCRIPT) 
		-c "init" 
		-c "halt 0" \
		-c "flash write_image erase unlock $< 0x1A000000 bin" \
		-c "reset run" \
		-c "shutdown" 2>&1

debug: $(TARGET_BIN)
	@echo DEBUG
	$(Q)$(OOCD) -f $(OOCD_SCRIPT) 2>&1
	
clean:
	@echo CLEAN
	$(Q)rm -fR $(OUT)

.PHONY: all size clean program debug
