/* -----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 ARM Ltd.
 *
 * This software is provided 'as-is', without any express or implied warranty. 
 * In no event will the authors be held liable for any damages arising from 
 * the use of this software. Permission is granted to anyone to use this 
 * software for any purpose, including commercial applications, and to alter 
 * it and redistribute it freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not 
 *    claim that you wrote the original software. If you use this software in
 *    a product, an acknowledgment in the product documentation would be 
 *    appreciated but is not required. 
 * 
 * 2. Altered source versions must be plainly marked as such, and must not be 
 *    misrepresented as being the original software. 
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *   
 * $Date:        19. October 2017
 * $Revision:    V1.1
 *  
 * Project:      Header file for CMSIS CAN Driver for Atmel SAM-ESV7
 * --------------------------------------------------------------------------*/

#ifndef __CAN_SAM_ESV7_H
#define __CAN_SAM_ESV7_H

#ifdef BORRARDO_CAN

#include <stdint.h>
#include <string.h>

#include "chip.h"

#include "Driver_CAN.h"

//#include "RTE_Device.h"
//#include "RTE_Components.h"
#include "board.h"

#ifndef   RTE_CAN0
#define   RTE_CAN0     (0U)
#endif
#ifndef   RTE_CAN1
#define   RTE_CAN1     (0U)
#endif

#if      (RTE_CAN1 == 1U)
#define   CAN_CTRL_NUM (2U)
#else
#define   CAN_CTRL_NUM (1U)
#endif

// Use REV_MCAN from mcan.h to determine which CAN revision is used
#define   CAN_REV_J             1
#define   CAN_REV_N             2
#define  _CAN_REV_DEF(rev)      CAN_REV_##rev
#define   CAN_REV_DEF(rev)     _CAN_REV_DEF(rev)

#if      (CAN_REV_DEF(REV_MCAN) == CAN_REV_J)
#define   CHIP_REV_A            1
#endif
#if      (CAN_REV_DEF(REV_MCAN) == CAN_REV_N)
#define   CHIP_REV_B            1
#endif

#ifndef   CHIP_REV_A
#define   CHIP_REV_A            0
#endif
#ifndef   CHIP_REV_B
#define   CHIP_REV_B            0
#endif


#endif

#endif // __CAN_SAM_ESV7_H

