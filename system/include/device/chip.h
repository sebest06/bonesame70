/* ---------------------------------------------------------------------------- */
/*                  Atmel Microcontroller Software Support                      */
/*                       SAM Software Package License                           */
/* ---------------------------------------------------------------------------- */
/* Copyright (c) 2015, Atmel Corporation                                        */
/*                                                                              */
/* All rights reserved.                                                         */
/*                                                                              */
/* Redistribution and use in source and binary forms, with or without           */
/* modification, are permitted provided that the following condition is met:    */
/*                                                                              */
/* - Redistributions of source code must retain the above copyright notice,     */
/* this list of conditions and the disclaimer below.                            */
/*                                                                              */
/* Atmel's name may not be used to endorse or promote products derived from     */
/* this software without specific prior written permission.                     */
/*                                                                              */
/* DISCLAIMER:  THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR   */
/* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE   */
/* DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,      */
/* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT */
/* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,  */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    */
/* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING         */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, */
/* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                           */
/* ---------------------------------------------------------------------------- */

#ifndef SAMS7_CHIP_H
#define SAMS7_CHIP_H

#include "sam.h"

#ifndef __ASSEMBLY__

#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#define ctz(u)              ((u) & (1ul <<  0) ?  0 : \
							 (u) & (1ul <<  1) ?  1 : \
							 (u) & (1ul <<  2) ?  2 : \
							 (u) & (1ul <<  3) ?  3 : \
							 (u) & (1ul <<  4) ?  4 : \
							 (u) & (1ul <<  5) ?  5 : \
							 (u) & (1ul <<  6) ?  6 : \
							 (u) & (1ul <<  7) ?  7 : \
							 (u) & (1ul <<  8) ?  8 : \
							 (u) & (1ul <<  9) ?  9 : \
							 (u) & (1ul << 10) ? 10 : \
							 (u) & (1ul << 11) ? 11 : \
							 (u) & (1ul << 12) ? 12 : \
							 (u) & (1ul << 13) ? 13 : \
							 (u) & (1ul << 14) ? 14 : \
							 (u) & (1ul << 15) ? 15 : \
							 (u) & (1ul << 16) ? 16 : \
							 (u) & (1ul << 17) ? 17 : \
							 (u) & (1ul << 18) ? 18 : \
							 (u) & (1ul << 19) ? 19 : \
							 (u) & (1ul << 20) ? 20 : \
							 (u) & (1ul << 21) ? 21 : \
							 (u) & (1ul << 22) ? 22 : \
							 (u) & (1ul << 23) ? 23 : \
							 (u) & (1ul << 24) ? 24 : \
							 (u) & (1ul << 25) ? 25 : \
							 (u) & (1ul << 26) ? 26 : \
							 (u) & (1ul << 27) ? 27 : \
							 (u) & (1ul << 28) ? 28 : \
							 (u) & (1ul << 29) ? 29 : \
							 (u) & (1ul << 30) ? 30 : \
							 (u) & (1ul << 31) ? 31 : \
							 32)


#endif


/*************************************************
 *      Memory type and its attribute
 *************************************************/
#define SHAREABLE       1
#define NON_SHAREABLE   0
/*********************************************************************************************************************************************************************
*   Memory Type Definition                          Memory TEX attribute            C attribute                     B attribute                     S attribute
**********************************************************************************************************************************************************************/

#define STRONGLY_ORDERED_SHAREABLE_TYPE      ((0x00 << MPU_RASR_TEX_Pos) | (DISABLE << MPU_RASR_C_Pos) | (DISABLE << MPU_RASR_B_Pos))     // DO not care //
#define SHAREABLE_DEVICE_TYPE                ((0x00 << MPU_RASR_TEX_Pos) | (DISABLE << MPU_RASR_C_Pos) | (ENABLE  << MPU_RASR_B_Pos))     // DO not care //
#define INNER_OUTER_NORMAL_WT_NWA_TYPE(x)   ((0x00 << MPU_RASR_TEX_Pos) | (ENABLE  << MPU_RASR_C_Pos) | (DISABLE << MPU_RASR_B_Pos) | (x << MPU_RASR_S_Pos))
#define INNER_OUTER_NORMAL_WB_NWA_TYPE(x)   ((0x00 << MPU_RASR_TEX_Pos) | (ENABLE  << MPU_RASR_C_Pos) | (ENABLE  << MPU_RASR_B_Pos) | (x << MPU_RASR_S_Pos))
#define INNER_OUTER_NORMAL_NOCACHE_TYPE(x)  ((0x01 << MPU_RASR_TEX_Pos) | (DISABLE << MPU_RASR_C_Pos) | (DISABLE << MPU_RASR_B_Pos) | (x << MPU_RASR_S_Pos))
#define INNER_OUTER_NORMAL_WB_RWA_TYPE(x)   ((0x01 << MPU_RASR_TEX_Pos) | (ENABLE  << MPU_RASR_C_Pos) | (ENABLE  << MPU_RASR_B_Pos) | (x << MPU_RASR_S_Pos))
#define NON_SHAREABLE_DEVICE_TYPE            ((0x02 << MPU_RASR_TEX_Pos) | (DISABLE << MPU_RASR_C_Pos) | (DISABLE << MPU_RASR_B_Pos))     // DO not care //

/*  Normal memory attributes with outer capability rules to Non_Cacable */

#define INNER_NORMAL_NOCACHE_TYPE(x)  ((0x04 << MPU_RASR_TEX_Pos) | (DISABLE  << MPU_RASR_C_Pos) | (DISABLE  << MPU_RASR_B_Pos) | (x << MPU_RASR_S_Pos))
#define INNER_NORMAL_WB_RWA_TYPE(x)   ((0x04 << MPU_RASR_TEX_Pos) | (DISABLE  << MPU_RASR_C_Pos) | (ENABLE  << MPU_RASR_B_Pos)  | (x << MPU_RASR_S_Pos))
#define INNER_NORMAL_WT_NWA_TYPE(x)   ((0x04 << MPU_RASR_TEX_Pos) | (ENABLE  << MPU_RASR_C_Pos)  | (DISABLE  << MPU_RASR_B_Pos) | (x << MPU_RASR_S_Pos))
#define INNER_NORMAL_WB_NWA_TYPE(x)   ((0x04 << MPU_RASR_TEX_Pos) | (ENABLE  << MPU_RASR_C_Pos)  | (ENABLE  << MPU_RASR_B_Pos)  | (x << MPU_RASR_S_Pos))

/* SCB Interrupt Control State Register Definitions */

#ifndef SCB_VTOR_TBLBASE_Pos
	#define SCB_VTOR_TBLBASE_Pos               29                                             /*!< SCB VTOR: TBLBASE Position */
	#define SCB_VTOR_TBLBASE_Msk               (1UL << SCB_VTOR_TBLBASE_Pos)                  /*!< SCB VTOR: TBLBASE Mask */
#endif


/*
 * Peripherals
 */
#include "microchip/acc.h"
#include "microchip/aes.h"
#include "microchip/afec.h"
#include "microchip/efc.h"
#include "microchip/pio.h"
#include "microchip/pio_it.h"
#include "microchip/efc.h"
#include "microchip/rstc.h"
#include "microchip/mpu.h"
#ifdef GMAC
#include "microchip/gmac.h"
#include "microchip/gmacd.h"
#endif
#include "microchip/video.h"
#include "microchip/icm.h"
#include "microchip/isi.h"
#include "microchip/exceptions.h"
#include "microchip/pio_capture.h"
#include "microchip/rtc.h"
#include "microchip/rtt.h"
#include "microchip/tc.h"
#include "microchip/timetick.h"
#include "microchip/twi.h"
#include "microchip/flashd.h"
#include "microchip/pmc.h"
#include "microchip/pwmc.h"
#ifdef MCAN0
#include "microchip/mcan.h"
#endif
#include "microchip/supc.h"
#include "microchip/usart.h"
#include "microchip/uart.h"
#include "microchip/isi.h"
#include "microchip/hsmci.h"
#include "microchip/ssc.h"
#include "microchip/twi.h"
#include "microchip/trng.h"
#include "microchip/wdt.h"
#include "microchip/spi.h"
#include "microchip/qspi.h"
#include "microchip/trace.h"
#include "microchip/xdmac.h"
#include "microchip/xdma_hardware_interface.h"
#include "microchip/xdmad.h"
#include "microchip/mcid.h"
#include "microchip/twid.h"
#include "microchip/spi_dma.h"
#include "microchip/qspi_dma.h"
#include "microchip/uart_dma.h"
#include "microchip/usart_dma.h"
#include "microchip/twid.h"
#include "microchip/afe_dma.h"
#include "microchip/dac_dma.h"
#include "microchip/usbhs.h"

//#define ENABLE_PERIPHERAL(dwId)         PMC_EnablePeripheral(dwId)
//#define DISABLE_PERIPHERAL(dwId)        PMC_DisablePeripheral(dwId)

#endif /* SAMS7_CHIP_H */
