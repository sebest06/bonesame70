/*
 * This file is part of the µOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2014 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef CORTEXM_EXCEPTION_HANDLERS_H_
#define CORTEXM_EXCEPTION_HANDLERS_H_

#include <stdint.h>

#if defined(DEBUG)
#define __DEBUG_BKPT()  asm volatile ("bkpt 0")
#endif

// ----------------------------------------------------------------------------

#if defined(__cplusplus)
extern "C"
{
#endif

// External references to cortexm_handlers.c

  extern void
  Reset_Handler (void);
  extern void
  NMI_Handler (void);
  extern void
  HardFault_Handler (void);

#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
  extern void
  MemManage_Handler (void);
  extern void
  BusFault_Handler (void);
  extern void
  UsageFault_Handler (void);
  extern void
  DebugMon_Handler (void);
#endif

  extern void
  SVC_Handler (void);

  extern void
  PendSV_Handler (void);
  extern void
  SysTick_Handler (void);

  extern void SUPC_Handler   ( void );
  extern void RSTC_Handler   ( void );
  extern void RTC_Handler    ( void );
  extern void RTT_Handler    ( void );
  extern void WDT_Handler    ( void );
  extern void PMC_Handler    ( void );
  extern void EFC_Handler    ( void );
  extern void UART0_Handler  ( void );
  extern void UART1_Handler  ( void );
  extern void PIOA_Handler   ( void );
  extern void PIOB_Handler   ( void );
  #ifdef _SAME70_PIOC_INSTANCE_
  extern void PIOC_Handler   ( void );
  #endif /* _SAME70_PIOC_INSTANCE_ */
  #ifdef _SAME70_USART0_INSTANCE_
  extern void USART0_Handler ( void );
  #endif /* _SAME70_USART0_INSTANCE_ */
  #ifdef _SAME70_USART1_INSTANCE_
  extern void USART1_Handler ( void );
  #endif /* _SAME70_USART1_INSTANCE_ */
  #ifdef _SAME70_USART2_INSTANCE_
  extern void USART2_Handler ( void );
  #endif /* _SAME70_USART2_INSTANCE_ */
  extern void PIOD_Handler   ( void );
  #ifdef _SAME70_PIOE_INSTANCE_
  extern void PIOE_Handler   ( void );
  #endif /* _SAME70_PIOE_INSTANCE_ */
  #ifdef _SAME70_HSMCI_INSTANCE_
  extern void HSMCI_Handler  ( void );
  #endif /* _SAME70_HSMCI_INSTANCE_ */
  extern void TWIHS0_Handler ( void );
  extern void TWIHS1_Handler ( void );
  #ifdef _SAME70_SPI0_INSTANCE_
  extern void SPI0_Handler   ( void );
  #endif /* _SAME70_SPI0_INSTANCE_ */
  extern void SSC_Handler    ( void );
  extern void TC0_Handler    ( void );
  extern void TC1_Handler    ( void );
  extern void TC2_Handler    ( void );
  #ifdef _SAME70_TC1_INSTANCE_
  extern void TC3_Handler    ( void );
  #endif /* _SAME70_TC1_INSTANCE_ */
  #ifdef _SAME70_TC1_INSTANCE_
  extern void TC4_Handler    ( void );
  #endif /* _SAME70_TC1_INSTANCE_ */
  #ifdef _SAME70_TC1_INSTANCE_
  extern void TC5_Handler    ( void );
  #endif /* _SAME70_TC1_INSTANCE_ */
  extern void AFEC0_Handler  ( void );
  #ifdef _SAME70_DACC_INSTANCE_
  extern void DACC_Handler   ( void );
  #endif /* _SAME70_DACC_INSTANCE_ */
  extern void PWM0_Handler   ( void );
  extern void ICM_Handler    ( void );
  extern void ACC_Handler    ( void );
  extern void USBHS_Handler  ( void );
  extern void MCAN0_INT0_Handler   ( void );
  extern void MCAN0_INT1_Handler   ( void );
  #ifdef _SAME70_MCAN1_INSTANCE_
  extern void MCAN1_INT0_Handler   ( void );
  extern void MCAN1_INT1_Handler   ( void );
  #endif /* _SAME70_MCAN1_INSTANCE_ */
  extern void GMAC_Handler   ( void );
  extern void AFEC1_Handler  ( void );
  #ifdef _SAME70_TWIHS2_INSTANCE_
  extern void TWIHS2_Handler ( void );
  #endif /* _SAME70_TWIHS2_INSTANCE_ */
  #ifdef _SAME70_SPI1_INSTANCE_
  extern void SPI1_Handler   ( void );
  #endif /* _SAME70_SPI1_INSTANCE_ */
  extern void QSPI_Handler   ( void );
  extern void UART2_Handler  ( void );
  #ifdef _SAME70_UART3_INSTANCE_
  extern void UART3_Handler  ( void );
  #endif /* _SAME70_UART3_INSTANCE_ */
  #ifdef _SAME70_UART4_INSTANCE_
  extern void UART4_Handler  ( void );
  #endif /* _SAME70_UART4_INSTANCE_ */
  #ifdef _SAME70_TC2_INSTANCE_
  extern void TC6_Handler    ( void );
  #endif /* _SAME70_TC2_INSTANCE_ */
  #ifdef _SAME70_TC2_INSTANCE_
  extern void TC7_Handler    ( void );
  #endif /* _SAME70_TC2_INSTANCE_ */
  #ifdef _SAME70_TC2_INSTANCE_
  extern void TC8_Handler    ( void );
  #endif /* _SAME70_TC2_INSTANCE_ */
  extern void TC9_Handler    ( void );
  extern void TC10_Handler   ( void );
  extern void TC11_Handler   ( void );
  extern void AES_Handler    ( void );
  extern void TRNG_Handler   ( void );
  extern void XDMAC_Handler  ( void );
  extern void ISI_Handler    ( void );
  extern void PWM1_Handler   ( void );
  extern void FPU_Handler    ( void );
  #ifdef _SAME70_SDRAMC_INSTANCE_
  extern void SDRAMC_Handler ( void );
  #endif /* _SAME70_SDRAMC_INSTANCE_ */
  extern void RSWDT_Handler  ( void );
  extern void CCW_Handler    ( void );
  extern void CCF_Handler    ( void );
  extern void GMAC_Q1_Handler     ( void );
  extern void GMAC_Q2_Handler     ( void );
  extern void IXC_Handler    ( void );
  #ifdef _SAME70_I2SC0_INSTANCE_
  extern void I2SC0_Handler  ( void );
  #endif /* _SAME70_I2SC0_INSTANCE_ */
  #ifdef _SAME70_I2SC1_INSTANCE_
  extern void I2SC1_Handler  ( void );
  #endif /* _SAME70_I2SC1_INSTANCE_ */
  #if (__SAM_M7_REVB == 1)
  extern void GMAC_Q3_Handler     ( void );
  extern void GMAC_Q4_Handler     ( void );
  extern void GMAC_Q5_Handler     ( void );
  #endif

  // Exception Stack Frame of the Cortex-M3 or Cortex-M4 processor.
  typedef struct
  {
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t psr;
#if  defined(__ARM_ARCH_7EM__)
    uint32_t s[16];
#endif
  } ExceptionStackFrame;

#if defined(TRACE)
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
  void
  dumpExceptionStack (ExceptionStackFrame* frame, uint32_t cfsr, uint32_t mmfar,
                      uint32_t bfar, uint32_t lr);
#endif // defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
#if defined(__ARM_ARCH_6M__)
  void
  dumpExceptionStack (ExceptionStackFrame* frame, uint32_t lr);
#endif // defined(__ARM_ARCH_6M__)
#endif // defined(TRACE)

  void
  HardFault_Handler_C (ExceptionStackFrame* frame, uint32_t lr);

#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
  void
  UsageFault_Handler_C (ExceptionStackFrame* frame, uint32_t lr);
  void
  BusFault_Handler_C (ExceptionStackFrame* frame, uint32_t lr);
#endif // defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)

#if defined(__cplusplus)
}
#endif

// ----------------------------------------------------------------------------

#endif // CORTEXM_EXCEPTION_HANDLERS_H_
