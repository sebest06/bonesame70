/* ---------------------------------------------------------------------------- */
/*                  Atmel Microcontroller Software Support                      */
/*                       SAM Software Package License                           */
/* ---------------------------------------------------------------------------- */
/* Copyright (c) 2015, Atmel Corporation                                        */
/*                                                                              */
/* All rights reserved.                                                         */
/*                                                                              */
/* Redistribution and use in source and binary forms, with or without           */
/* modification, are permitted provided that the following condition is met:    */
/*                                                                              */
/* - Redistributions of source code must retain the above copyright notice,     */
/* this list of conditions and the disclaimer below.                            */
/*                                                                              */
/* Atmel's name may not be used to endorse or promote products derived from     */
/* this software without specific prior written permission.                     */
/*                                                                              */
/* DISCLAIMER:  THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR   */
/* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE   */
/* DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,      */
/* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT */
/* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,  */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    */
/* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING         */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, */
/* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                           */
/* ---------------------------------------------------------------------------- */

/**
 *  \file
 *
 *  \section Purpose
 *
 *  Interface for configuring and using Timer Counter (TC) peripherals.
 *
 *  \section Usage
 *  -# Optionally, use TC_FindMckDivisor() to let the program find the best
 *     TCCLKS field value automatically.
 *  -# Configure a Timer Counter in the desired mode using TC_Configure().
 *  -# Start or stop the timer clock using TC_Start() and TC_Stop().
 */

#ifndef _MCAN_
#define _MCAN_

/*------------------------------------------------------------------------------
 *         Headers
 *------------------------------------------------------------------------------*/

#include "chip.h"

#include <stdint.h>
#include "Driver_Common.h"
#include "Driver_CAN.h"

/*------------------------------------------------------------------------------
 *         Global functions
 *------------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _MCAN_DRV_CONFIG {// Compile time configuration of driver structure
  uint32_t FILTER_STD_NUM;      // Number of Standard IDs
  uint32_t FILTER_EXT_NUM;      // Number of Extended IDs
  uint32_t RX_FIFO0_ELEM_NUM;   // Number of Rx FIFO0 elements
  uint32_t RX_FIFO1_ELEM_NUM;   // Number of Rx FIFO1 elements
  uint32_t TX_FIFO_ELEM_NUM;    // Number of Tx FIFO elements
  uint32_t RX_FIFO_OBJ_NUM;     // Number of Rx FIFO objects
  uint32_t RX_FIFO_OBJ_IDX_MAX; // Maximum index of Rx FIFO object (0=no max index, 1..n=index+1)
  uint32_t RX_BUF_OBJ_NUM;      // Number of Rx buffer objects
  uint32_t RX_BUF_OBJ_IDX_MAX;  // Maximum index of Rx buffer object (0=no max index, 1..n=index+1)
  uint32_t RX_TOT_OBJ_NUM;      // Number of all Rx FIFO and buffer objects
  uint32_t TX_FIFO_OBJ_NUM;     // Number of Tx FIFO objects
  uint32_t TX_FIFO_OBJ_IDX_MAX; // Maximum index of Tx FIFO object (0=no max index, 1..n=index+1)
  uint32_t TX_BUF_OBJ_NUM;      // Number of Tx buffer objects
  uint32_t TX_BUF_OBJ_IDX_MAX;  // Maximum index of Tx buffer object (0=no max index, 1..n=index+1)
  uint32_t TX_TOT_OBJ_NUM;      // Number of all Tx FIFO and buffer objects
  uint32_t TOT_OBJ_NUM;         // Number of all Rx + Tx objects
} MCAN_DRV_CONFIG_t;

typedef struct _MCAN_RAM_SIZES_CONFIG
{
	uint32_t FILTER_STD_NUM;      // Number of Standard IDs
	uint32_t FILTER_EXT_NUM;      // Number of Extended IDs
	uint32_t RX_FIFO0_ELEM_NUM;   // Number of Rx FIFO0 elements
	uint32_t RX_FIFO1_ELEM_NUM;   // Number of Rx FIFO1 elements
	uint32_t RX_BUF_OBJ_NUM;      // Number of Rx buffer objects
	uint32_t TX_FIFO_ELEM_NUM;    // Number of Tx FIFO elements
	uint32_t TX_BUF_OBJ_NUM;      // Number of Tx buffer objects

	uint32_t RX_FIFO0_ELEM_SIZE;   // Size of Rx FIFO0 elements
	uint32_t RX_FIFO1_ELEM_SIZE;   // Size of Rx FIFO1 elements
	uint32_t RX_BUF_OBJ_SIZE;      // Size of Rx buffer objects
	uint32_t TX_FIFO_ELEM_SIZE;    // Size of Tx FIFO elements
	uint32_t TX_BUF_OBJ_SIZE;      // Size of Tx buffer objects

}MCAN_RAM_SIZES_CONFIG;

typedef struct _MCAN_RAM_PTR {   // RAM pointers to areas of filters and buffers structure
  uint32_t *PTR_STD_ID;         // Pointer to Standard IDs RAM
  uint32_t *PTR_EXT_ID;         // Pointer to Extended IDs RAM
  uint32_t *PTR_RX_FIFO0;       // Pointer to Rx FIFO0 RAM
  uint32_t *PTR_RX_FIFO1;       // Pointer to Rx FIFO1 RAM
  uint32_t *PTR_RX_BUFFER;      // Pointer to Rx buffers RAM
  //uint32_t *PTR_TX_EVENT_FIFO;  // Pointer to Tx Event FIFO RAM
  uint32_t *PTR_TX_BUFFER;      // Pointer to Tx buffers RAM
} MCAN_RAM_PTR_t;

typedef enum {
	CAN_STD_ID = 0,
	CAN_EXT_ID = 1
} MCan_IdType;

typedef enum {
	CAN_DLC_0 = 0,
	CAN_DLC_1 = 1,
	CAN_DLC_2 = 2,
	CAN_DLC_3 = 3,
	CAN_DLC_4 = 4,
	CAN_DLC_5 = 5,
	CAN_DLC_6 = 6,
	CAN_DLC_7 = 7,
	CAN_DLC_8 = 8,
	CAN_DLC_12 = 9,
	CAN_DLC_16 = 10,
	CAN_DLC_20 = 11,
	CAN_DLC_24 = 12,
	CAN_DLC_32 = 13,
	CAN_DLC_48 = 14,
	CAN_DLC_64 = 15
} MCan_DlcType;

typedef enum {
	CAN_FIFO_0 = 0,
	CAN_FIFO_1 = 1
} MCan_FifoType;

typedef enum {
	CAN_INTR_LINE_0 = 0,
	CAN_INTR_LINE_1 = 1
} MCan_IntrLineType;

typedef enum
{
	CAN_RETURN_OK = 0,
	CAN_RETURN_FAIL
}MCan_RETURN_Code;

typedef enum
{
	CAN_INTRTYPEMODE_ENABLE = 0,
	CAN_INTRTYPEMODE_DISABLE,
}MCan_IntrTypeMode;


typedef struct MCan_MsgRamPntrsTag {
	uint32_t *pStdFilts;
	uint32_t *pExtFilts;
	uint32_t *pRxFifo0;
	uint32_t *pRxFifo1;
	uint32_t *pRxDedBuf;
	uint32_t *pTxEvtFifo;
	uint32_t *pTxDedBuf;
	uint32_t *pTxFifoQ;
} MCan_MsgRamPntrs;

typedef struct MCan_ConfigTag {
	Mcan             *pMCan;
	uint32_t          bitTiming;
	uint32_t          fastBitTiming;
	uint32_t          nmbrStdFilts;
	uint32_t          nmbrExtFilts;
	uint32_t          nmbrFifo0Elmts;
	uint32_t          nmbrFifo1Elmts;
	uint32_t          nmbrRxDedBufElmts;
	uint32_t          nmbrTxEvtFifoElmts;
	uint32_t          nmbrTxDedBufElmts;
	uint32_t          nmbrTxFifoQElmts;
	uint32_t          rxFifo0ElmtSize;
	uint32_t          rxFifo1ElmtSize;
	uint32_t          rxBufElmtSize;
	// Element sizes and data sizes (encoded element size)
	uint32_t          txBufElmtSize;
	// Element size and data size (encoded element size)
	MCan_MsgRamPntrs  msgRam;
} MCan_ConfigType;

extern const MCan_ConfigType mcan0Config;
extern const MCan_ConfigType mcan1Config;

__STATIC_INLINE uint32_t MCAN_IsTxComplete(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;
	return (mcan->MCAN_IR & MCAN_IR_TC);
}

__STATIC_INLINE void MCAN_ClearTxComplete(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;
	mcan->MCAN_IR = MCAN_IR_TC;
}

__STATIC_INLINE uint32_t MCAN_IsMessageStoredToRxDedBuffer(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;

	return (mcan->MCAN_IR & MCAN_IR_DRX);
}

__STATIC_INLINE void MCAN_ClearMessageStoredToRxBuffer(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;
	mcan->MCAN_IR = MCAN_IR_DRX;
}

__STATIC_INLINE uint32_t MCAN_IsMessageStoredToRxFifo0(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;
	return (mcan->MCAN_IR & MCAN_IR_RF0N);
}

__STATIC_INLINE void MCAN_ClearMessageStoredToRxFifo0(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;
	mcan->MCAN_IR = MCAN_IR_RF0N;
}

__STATIC_INLINE uint32_t MCAN_IsMessageStoredToRxFifo1(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;
	return (mcan->MCAN_IR & MCAN_IR_RF1N);
}

__STATIC_INLINE void MCAN_ClearMessageStoredToRxFifo1(
	const MCan_ConfigType *mcanConfig)
{
	Mcan *mcan = mcanConfig->pMCan;
	mcan->MCAN_IR = MCAN_IR_RF1N;
}


int32_t MCAN_Initialize ();
int32_t MCAN_PowerControlConfig(Mcan *ptr_CAN, IRQn_Type ID_IRQn, ARM_POWER_STATE state, MCAN_RAM_PTR_t can_ram, MCAN_RAM_SIZES_CONFIG can_config_sizes);
int32_t MCAN_SetMode (Mcan *ptr_CAN, ARM_CAN_MODE mode);
uint32_t MCAN_GetClock (void);
int32_t MCAN_SetBitrate (Mcan *ptr_CAN, ARM_CAN_BITRATE_SELECT select, uint32_t bitrate, uint32_t bit_segments, uint32_t clock_tolerance);
int32_t MCAN_MessageSend (Mcan *ptr_CAN, uint32_t obj_idx, ARM_CAN_MSG_INFO *msg_info, const uint8_t *data, uint8_t size, MCAN_DRV_CONFIG_t can_conf, MCAN_RAM_PTR_t can_ram, uint32_t ram_len, uint8_t isFdMode);
int32_t MCAN_MessageRead (Mcan *ptr_CAN, uint32_t obj_idx, ARM_CAN_MSG_INFO *msg_info, uint8_t *data, uint8_t size, MCAN_DRV_CONFIG_t can_conf, MCAN_RAM_PTR_t can_ram, uint32_t ram_len, uint8_t isFdMode);

void MCAN_SendMsg(Mcan *ptr_CAN,ARM_CAN_MSG_INFO *msg_info,const uint8_t *data, MCAN_RAM_PTR_t can_ram);
void MCAN_WriteBuffer(Mcan *ptr_CAN,ARM_CAN_MSG_INFO *msg_info,const uint8_t *data, int32_t indexBuffer, MCAN_RAM_PTR_t can_ram);

int32_t MCAN_isFifoTxFull(Mcan *ptr_CAN);

int32_t MCAN_getNewMsgBuffered(Mcan *ptr_CAN,ARM_CAN_MSG_INFO *msg_info,uint8_t *data,MCAN_RAM_PTR_t can_ram);
void MCAN_ReadMsgBuffered(Mcan *ptr_CAN, uint32_t buffer_idx, ARM_CAN_MSG_INFO *msg_info, uint8_t *data, MCAN_RAM_PTR_t can_ram);

int32_t MCAN_GetLastBufferedData(Mcan *ptr_CAN);
int32_t MCAN_GetLastFifoRxData(Mcan *ptr_CAN,int32_t fifo_num, ARM_CAN_MSG_INFO *msg_info, uint8_t *data, MCAN_RAM_PTR_t can_ram);
int32_t MCAN_GetHPMStatus(Mcan *ptr_CAN);
int32_t MCAN_GetRxFifoStatus(Mcan *ptr_CAN, int32_t fifo_num);

void MCAN_ConfigureRam(Mcan *ptr_CAN, MCan_IntrTypeMode obj_cfg, uint32_t interruptType);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef _MCAN_ */

