/*
 * Usart.h
 *
 *  Created on: 3/4/2019
 *      Author: electro1
 */

#ifndef USART_H_
#define USART_H_

#include "chip.h"
#include "usart.h"
#include "sam.h"
#include "pio.h"

namespace same70 {

typedef enum{
	TTY_INTERRUPT_RXRDY = (0x1u << 0),
	TTY_INTERRUPT_TXRDY = (0x1u << 1),
	TTY_INTERRUPT_OVERRUN = (0x1u << 5),
	TTY_INTERRUPT_TIMEOUT = (0x1u << 8),
	TTY_INTERRUPT_TXEMPTY = (0x1u << 9)
}INTERRUPT_MASK;

class UsartSetting
{
public:
	typedef enum{
		TTY_CHRL_8_BIT = (0x3u << 6),
		TTY_CHRL_7_BIT = (0x2u << 6),
		TTY_CHRL_6_BIT = (0x1u << 6),
		TTY_CHRL_5_BIT = (0x0u << 6),
	}BIT_LENGTH;

	typedef enum{
		TTY_PARITY_EVEN = (0x0u << 9),
		TTY_PARITY_ODD = (0x1u << 9),
		TTY_PARITY_SPACE = (0x2u << 9),
		TTY_PARITY_MARK = (0x3u << 9),
		TTY_PARITY_NONE = (0x4u << 9),
		TTY_PARITY_MULTIDROP = (0x6u << 9),
	}PARITY;

	typedef enum{
		TTY_STOP_BIT_1BIT = (0x0u << 12),
		TTY_STOP_BIT_1_5BIT = (0x1u << 12),
		TTY_STOP_BIT_2BIT = (0x2u << 12),
	}STOPBIT;


	BIT_LENGTH bitLength;
	PARITY parity;
	STOPBIT stopbit;
	uint32_t baudrate;
};

class UsartPort {
public:

	UsartPort(Usart *port,IRQn_Type ID_IRQn, uint32_t masterClock);
	virtual ~UsartPort();

	void UsartConfig(UsartSetting setting);

	int8_t UsartIsDataAvailable();
	int8_t UsartGetByte(uint8_t *byte);
	int8_t UsartWriteByte(uint8_t byte);
	uint32_t UsartGetStatus();
	void UsartReset();

	void UsartEnableInterrup(INTERRUPT_MASK mask);

	void UsartHandleInterrup();

	virtual void UsartInterruptRxReady(){};
	virtual void UsartInterruptTxReady(){};
	virtual void UsartInterruptTxEmpty(){};
	virtual void UsartInterruptOverrun(){};
	virtual void UsartInterruptTimeout(){};

private:
	Usart *_port;
	uint32_t _masterClock;
	IRQn_Type _ID_IRQn;

};

} /* namespace same70 */

#endif /* USART_H_ */
