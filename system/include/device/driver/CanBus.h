#ifndef CANBUS_H_
#define CANBUS_H_

#include "chip.h"
#include "mcan.h"
#include "sam.h"
#include "pio.h"

namespace same70 {
class CanBusFilterManager
{
public:
	typedef enum
	{
			FILTER_CONF_DISABLE = 0,
			FILTER_CONF_STORE_RX_FIFO0,
			FILTER_CONF_STORE_RX_FIFO1,
			FILTER_CONF_REJECT,
			FILTER_CONF_SET_PRIORITY,
			FILTER_CONF_SET_PRIORITY_AND_STORE_RX_FIFO0,
			FILTER_CONF_SET_PRIORITY_AND_STORE_RX_FIFO1,
			FILTER_CONF_STORE_RX_BUFFER
	}FILTER_ELEMENT_CONFIGURATION;

	typedef enum
	{
		FILTER_STD = 0,
		FILTER_EXT
	}FILTER_ELEMENT_TYPE;

	CanBusFilterManager(MCAN_RAM_PTR_t *memory, int32_t StdFilterSize,int32_t ExtFilterSize);
	int32_t setStdIdFilter(int32_t id,FILTER_ELEMENT_CONFIGURATION conf,int32_t indexBuffered=0);
	int32_t setStdDualIdFilter(int32_t id1,int32_t id2,FILTER_ELEMENT_CONFIGURATION conf);
	int32_t setStdMaskIdFilter(int32_t id,int32_t mask,FILTER_ELEMENT_CONFIGURATION conf);
	int32_t setStdRangeIdFilter(int32_t id1,int32_t id2,FILTER_ELEMENT_CONFIGURATION conf);

	int32_t setExtIdFilter(int32_t id, FILTER_ELEMENT_CONFIGURATION conf,int32_t indexBuffered=0);
	int32_t setExtDualIdFilter(int32_t id1,int32_t id2, FILTER_ELEMENT_CONFIGURATION conf);
	int32_t setExtMaskIdFilter(int32_t id,int32_t mask, FILTER_ELEMENT_CONFIGURATION conf);
	int32_t setExtMaskIdFilterWithoutGlobalMask(int32_t id,int32_t mask, FILTER_ELEMENT_CONFIGURATION conf);
	int32_t setExtRangeIdFilter(int32_t id1,int32_t id2, FILTER_ELEMENT_CONFIGURATION conf);


private:
	int32_t getFreeFilterIndex(FILTER_ELEMENT_TYPE type);
	MCAN_RAM_PTR_t *_memory;
	int32_t _StdFilterSize;
	int32_t _ExtFilterSize;
};

class CanBusConfig
{
public:
	typedef enum
	{
		CAN_DATA_FIELD_8_BYTES = 0,
		CAN_DATA_FIELD_12_BYTES,
		CAN_DATA_FIELD_16_BYTES,
		CAN_DATA_FIELD_20_BYTES,
		CAN_DATA_FIELD_24_BYTES,
		CAN_DATA_FIELD_32_BYTES,
		CAN_DATA_FIELD_48_BYTES,
		CAN_DATA_FIELD_64_BYTES,
		CAN_DATA_FIELD_ERROR,
	}CAN_DATA_FIELD_SIZE;

	CanBusConfig();
	void setRxFifo0Config(int32_t elements, CAN_DATA_FIELD_SIZE size);
	void setRxFifo1Config(int32_t elements, CAN_DATA_FIELD_SIZE size);
	void setRxBufferConfig(int32_t elements, CAN_DATA_FIELD_SIZE size);
	void setTxBufferFifoConfig(int32_t elementsBuffer,int32_t elementsFifo, CAN_DATA_FIELD_SIZE sizeBuffer, CAN_DATA_FIELD_SIZE sizeFifo);
	void setStdFilterElements(int32_t elements);
	void setExtFilterElements(int32_t elements);

	int32_t getRamSizeOfElement(CAN_DATA_FIELD_SIZE elementSize);

	int32_t getRxFifo0Elements();
	int32_t getRxFifo1Elements();
	int32_t getRxBufferElements();
	int32_t getTxBufferElements();
	int32_t getTxFifoElements();
	int32_t getStdFilterElements();
	int32_t getExtFilterElements();

	int32_t getRxFifo0Size();
	int32_t getRxFifo1Size();
	int32_t getRxBufferSize();
	int32_t getTxBufferSize();
	int32_t getTxFifoSize();

	int32_t getStdFilterSize(){ return 1;}
	int32_t getExtFilterSize(){ return 2;}

	int32_t getOffsetStdId();
	int32_t getOffsetExtId();
	int32_t getOffsetRxFifo0();
	int32_t getOffsetRxFifo1();
	int32_t getOffsetRxBuffer();
	int32_t getOffsetTxBuffer();

	int32_t getTotalRamSize();

private:
	int32_t rxFifo0Elements;
	int32_t rxFifo1Elements;
	int32_t rxBufferElements;
	int32_t txBufferElements;
	int32_t txFifoElements;
	int32_t StdFilterElements;
	int32_t ExtFilterElements;

	CAN_DATA_FIELD_SIZE rxFifo0;
	CAN_DATA_FIELD_SIZE rxFifo1;
	CAN_DATA_FIELD_SIZE rxBuffer;
	CAN_DATA_FIELD_SIZE txBuffer;
	CAN_DATA_FIELD_SIZE txFifo;

};

class CanBus
{
public:
	CanBus(Mcan *ptr_CAN, IRQn_Type ID_IRQn, uint32_t *canBusRamBuffer, uint32_t sizeRamBuffer);
	virtual ~CanBus();

	int32_t ConfigBus(ARM_POWER_STATE state);
	void setMode(ARM_CAN_MODE mode);
	int32_t setBitRate(uint32_t bitRateNominal);
	void configInterrupt(MCan_IntrTypeMode obj_cfg, uint32_t interruptType);
	void sendMsg(ARM_CAN_MSG_INFO packet, const uint8_t *data);

	void sendMsgBuffered(ARM_CAN_MSG_INFO packet, const uint8_t *data, int32_t indexBuffer);

	void HandleInterrup();
	int32_t getBufferedRxIndex();
	int32_t getHPMStatus();
	int32_t getRxFifoStatus(int32_t fifo_num);
	int32_t getLastRxFifoMsg(int32_t fifo_num,ARM_CAN_MSG_INFO *msg_info, uint8_t *data);

	int32_t getNewDataFromRxBuffer(ARM_CAN_MSG_INFO *msg_info, uint8_t *data);

	int32_t isTxFifoFull();


protected:
	CanBusFilterManager* getFilterManager();
	CanBusConfig* getCanBusConfiguer();
	virtual void InterruptHPM(){}; //High Priority Message
	virtual void InterruptNewMessageFifo0(){};
	virtual void InterruptNewMessageFifo1(){};
	virtual void InterruptNewMessageRxBuffer(){};
	/*virtual void InterruptTxReady(){};
	virtual void InterruptTxEmpty(){};
	virtual void InterruptOverrun(){};
	virtual void InterruptTimeout(){};*/

private:
	virtual uint32_t getClockTolerance();
	virtual uint32_t getBitRate();

	/*virtual*/ bool getTransmitionDelayCompensation();
	virtual uint32_t getTransmitionDelayOffset();
	virtual uint32_t getTransmitionDelayFilter();

	bool isRxFifoEnable();
	bool isRxBufferEnable();
	bool isTxFifoEnable();
	bool isTxBufferEnable();


	uint32_t *_canBusRamBuffer;
	MCAN_RAM_PTR_t _can_ram_ptr;
	//MCAN_DRV_CONFIG_t _can_confCan;
	Mcan *_ptr_CAN;
	IRQn_Type _ID_IRQn;
	CanBusFilterManager *filterManager;
	CanBusConfig *_canConfig;

};

}
#endif
