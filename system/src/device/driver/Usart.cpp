/*
 * Usart.cpp
 *
 *  Created on: 3/4/2019
 *      Author: electro1
 */

#include "driver/Usart.h"

namespace same70 {

UsartPort::UsartPort(Usart *port, IRQn_Type ID_IRQn, uint32_t masterClock) {
	_port = port;
	_masterClock = masterClock;
	_ID_IRQn = ID_IRQn;
}

UsartPort::~UsartPort() {
	// TODO Auto-generated destructor stub
}

void UsartPort::UsartConfig(UsartSetting setting)
{
	uint32_t configSerie = 0;
	configSerie = setting.bitLength | setting.parity | setting.stopbit;
	USART_Configure(_port,configSerie,setting.baudrate,_masterClock);
}

int8_t UsartPort::UsartIsDataAvailable()
{
	return USART_IsDataAvailable(_port);
}

int8_t UsartPort::UsartGetByte(uint8_t *byte)
{
	*byte = USART_GetChar(_port);
	return 1;
}

int8_t UsartPort::UsartWriteByte(uint8_t byte)
{
	USART_PutChar(_port,byte);
	return 1;
}

void UsartPort::UsartReset()
{

}

void UsartPort::UsartEnableInterrup(INTERRUPT_MASK mask)
{
	_port->US_IDR = 0xffffffff;
	USART_EnableIt(_port,mask);
	NVIC_EnableIRQ(_ID_IRQn);
}
uint32_t UsartPort::UsartGetStatus()
{
	return USART_GetStatus(_port);
}

void UsartPort::UsartHandleInterrup()
{
	volatile uint32_t status;
	status = UsartGetStatus();
	if(status | TTY_INTERRUPT_OVERRUN)
	{
		UsartInterruptOverrun();
	}
	if(status | TTY_INTERRUPT_RXRDY)
	{
		UsartInterruptRxReady();
	}
	if(status | TTY_INTERRUPT_TIMEOUT)
	{
		UsartInterruptTimeout();
	}
	if(status | TTY_INTERRUPT_TXEMPTY)
	{
		UsartInterruptTxEmpty();
	}
	if(status | TTY_INTERRUPT_TXRDY)
	{
		UsartInterruptTxReady();
	}
}

} /* namespace same70 */
