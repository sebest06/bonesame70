#include "driver/CanBus.h"
#include "device/ExceptionHandlers.h"

#include <string.h>

#define MCAN_DEFAULT_CLOCK_TOLERANCE 15
#define MCAN_DEFAULT_BITRATE 250000
/*#define MCAN_DEFAULT_RX_FIFO0_ELEM_NUM 64
#define MCAN_DEFAULT_RX_FIFO1_ELEM_NUM 64
#define MCAN_DEFAULT_RX_BUF_NUM 64
#define MCAN_DEFAULT_TX_FIFO_ELEM_NUM 8
#define MCAN_DEFAULT_TX_BUF_NUM 24
#define MCAN_DEFAULT_FILTER_STD_NUM 128
#define MCAN_DEFAULT_FILTER_EXT_NUM 64
*/
#define MCAN_DEFAULT_TRANSMITTER_DELAY_COMPENSATION_EN 0
#define MCAN_DEFAULT_TRANSMITTER_DELAY_OFFSET 0
#define MCAN_DEFAULT_TRANSMITTER_DELAY_FILTER 0

namespace same70 {

static const uint8_t MCAN_DATAFIELD_SIZE_DECODE[8] = {4U,5U,6U,7U,8U,10U,14U,18U};

CanBusFilterManager::CanBusFilterManager(MCAN_RAM_PTR_t *memory, int32_t StdFilterSize,int32_t ExtFilterSize) //128 y 64
{
	_memory = memory;
	_StdFilterSize = StdFilterSize;
	_ExtFilterSize = ExtFilterSize;
	memset(_memory->PTR_STD_ID,0,StdFilterSize*4);
	memset(_memory->PTR_EXT_ID,0,ExtFilterSize*2*4);
}

int32_t CanBusFilterManager::getFreeFilterIndex(FILTER_ELEMENT_TYPE type)
{
	uint32_t *ptr_filter_RAM;
	int32_t maxIndex = 0;

		if(type == FILTER_STD)
		{
			ptr_filter_RAM = _memory->PTR_STD_ID;
			maxIndex = _StdFilterSize;
			for(int i = 0; i < maxIndex; i++)
			{
				if((ptr_filter_RAM[i] & (3<<27)) == 0)
				{
					return i;
				}
			}
		}
		else if(type == FILTER_EXT)
		{
			ptr_filter_RAM = _memory->PTR_EXT_ID;
			maxIndex = _ExtFilterSize*2;
			for(int i = 0; i < maxIndex; i+=2)
			{
				if((ptr_filter_RAM[i] & (3<<29)) == 0)
				{
					return i;
				}
			}
		}

		return -1;
}

int32_t CanBusFilterManager::setStdIdFilter(int32_t id,FILTER_ELEMENT_CONFIGURATION conf, int32_t indexBuffered)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_STD_ID;

	int32_t i = getFreeFilterIndex(FILTER_STD);

	if(i != -1)
	{
		ptr_filter_RAM[i] = 0;
		ptr_filter_RAM[i] |= (1 << 30);	// Dual ID Filter for SF1ID or SF2ID
		ptr_filter_RAM[i] |= (conf << 27);
		ptr_filter_RAM[i] |= ((id & 0x7FFU) << 16);
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] |= ((id & 0x7FFU) << 0);
		}
		else
		{
			ptr_filter_RAM[i] |= (indexBuffered & 0x3f);
			//Bits 0:5 defines the index of the dedicated Rx Buffer element to which a matching message is stored
		}
	}
	return i;
}

int32_t CanBusFilterManager::setStdDualIdFilter(int32_t id1,int32_t id2,FILTER_ELEMENT_CONFIGURATION conf)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_STD_ID;

	int32_t i = getFreeFilterIndex(FILTER_STD);

	if(i != -1)
	{
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] = 0;
			ptr_filter_RAM[i] |= (1 << 30);	// Dual ID Filter for SF1ID or SF2ID
			ptr_filter_RAM[i] |= (conf << 27);
			ptr_filter_RAM[i] |= ((id1 & 0x7FFU) << 16);
			ptr_filter_RAM[i] |= ((id2 & 0x7FFU) << 0);
		}
		else
		{
			return -1;
			//ptr_filter_RAM[i] |= (indexBuffered & 0x3f);
			//Bits 0:5 defines the index of the dedicated Rx Buffer element to which a matching message is stored
		}
	}
	return i;
}

int32_t CanBusFilterManager::setStdMaskIdFilter(int32_t id,int32_t mask,FILTER_ELEMENT_CONFIGURATION conf)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_STD_ID;

	int32_t i = getFreeFilterIndex(FILTER_STD);

	if(i != -1)
	{
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] = 0;
			ptr_filter_RAM[i] |= (2 << 30);	// Dual ID Filter for SF1ID or SF2ID
			ptr_filter_RAM[i] |= (conf << 27);
			ptr_filter_RAM[i] |= ((id & 0x7FFU) << 16);
			ptr_filter_RAM[i] |= ((mask & 0x7FFU) << 0);
		}
		else
		{
			return -1;
			//Bits 0:5 defines the index of the dedicated Rx Buffer element to which a matching message is stored
		}
	}
	return i;

}

int32_t CanBusFilterManager::setStdRangeIdFilter(int32_t id1,int32_t id2, FILTER_ELEMENT_CONFIGURATION conf)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_STD_ID;
	int32_t idx;

	if(id1 > id2)
	{
		idx = id1;
		id1 = id2;
		id2 = idx;
	}

	int32_t i = getFreeFilterIndex(FILTER_STD);

	if(i != -1)
	{
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] = 0;
			ptr_filter_RAM[i] |= (0 << 30);	// Dual ID Filter for SF1ID or SF2ID
			ptr_filter_RAM[i] |= (conf << 27);
			ptr_filter_RAM[i] |= ((id1 & 0x7FFU) << 16);
			ptr_filter_RAM[i] |= ((id2 & 0x7FFU) << 0);
		}
		else
		{
			return -1;
		}
	}
	return i;

}

int32_t CanBusFilterManager::setExtIdFilter(int32_t id, FILTER_ELEMENT_CONFIGURATION conf,int32_t indexBuffered)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_EXT_ID;
	int32_t i = getFreeFilterIndex(FILTER_EXT);
	if(i != -1)
	{
		ptr_filter_RAM[i] = 0;
		ptr_filter_RAM[i+1] = 0;
		ptr_filter_RAM[i] |= (conf << 29);
		ptr_filter_RAM[i] |= (id & 0x1fffffff);
		ptr_filter_RAM[i+1] |= (1 << 30);
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i+1] |= (id & 0x1fffffff);
		}
		else
		{
			ptr_filter_RAM[i+1] |= (indexBuffered);
		}
	}

	return i;
}

int32_t CanBusFilterManager::setExtDualIdFilter(int32_t id1,int32_t id2, FILTER_ELEMENT_CONFIGURATION conf)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_EXT_ID;
	int32_t i = getFreeFilterIndex(FILTER_EXT);

	if(i != -1)
	{
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] = 0;
			ptr_filter_RAM[i+1] = 0;
			ptr_filter_RAM[i] |= (conf << 29);
			ptr_filter_RAM[i] |= (id1 & 0x1fffffff);
			ptr_filter_RAM[i+1] |= (1 << 30);
			ptr_filter_RAM[i+1] |= (id2 & 0x1fffffff);
		}
		else
		{
			return -1;
		}

	}
	return i;
}

int32_t CanBusFilterManager::setExtMaskIdFilter(int32_t id,int32_t mask, FILTER_ELEMENT_CONFIGURATION conf)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_EXT_ID;
	int32_t i = getFreeFilterIndex(FILTER_EXT);

	if(i != -1)
	{
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] = 0;
			ptr_filter_RAM[i+1] = 0;
			ptr_filter_RAM[i] |= (conf << 29);
			ptr_filter_RAM[i] |= (id & 0x1fffffff);
			ptr_filter_RAM[i+1] |= (2 << 30);
			ptr_filter_RAM[i+1] |= (mask & 0x1fffffff);
		}
		else
		{
			return -1;
		}
	}
	return i;
}

int32_t CanBusFilterManager::setExtMaskIdFilterWithoutGlobalMask(int32_t id,int32_t mask, FILTER_ELEMENT_CONFIGURATION conf)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_EXT_ID;
	int32_t i = getFreeFilterIndex(FILTER_EXT);

	if(i != -1)
	{
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] = 0;
			ptr_filter_RAM[i+1] = 0;
			ptr_filter_RAM[i] |= (conf << 29);
			ptr_filter_RAM[i] |= (id & 0x1fffffff);
			ptr_filter_RAM[i+1] |= (3 << 30);
			ptr_filter_RAM[i+1] |= (mask & 0x1fffffff);
		}
		else
		{
			return -1;
		}
	}
	return i;
}

int32_t CanBusFilterManager::setExtRangeIdFilter(int32_t id1,int32_t id2, FILTER_ELEMENT_CONFIGURATION conf)
{
	uint32_t *ptr_filter_RAM;
	ptr_filter_RAM = _memory->PTR_EXT_ID;
	int32_t i = getFreeFilterIndex(FILTER_EXT);
	int32_t idx;

	if(id1 > id2)
	{
		idx = id1;
		id1 = id2;
		id2 = idx;
	}

	if(i != -1)
	{
		if(conf != FILTER_CONF_STORE_RX_BUFFER)
		{
			ptr_filter_RAM[i] = 0;
			ptr_filter_RAM[i+1] = 0;
			ptr_filter_RAM[i] |= (conf << 29);
			ptr_filter_RAM[i] |= (id1 & 0x1fffffff);
			ptr_filter_RAM[i+1] |= (0 << 30);
			ptr_filter_RAM[i+1] |= (id2 & 0x1fffffff);
		}
		else
		{
			return -1;
		}
	}
	return i;
}

CanBusConfig::CanBusConfig()
{
	rxFifo0Elements = 64;
	rxFifo1Elements = 64;
	rxBufferElements = 64;
	txBufferElements = 24;
	txFifoElements  = 8;
	StdFilterElements  = 128;
	ExtFilterElements  = 64;

	rxFifo0 = CAN_DATA_FIELD_8_BYTES;
	rxFifo1 = CAN_DATA_FIELD_8_BYTES;
	rxBuffer = CAN_DATA_FIELD_8_BYTES;
	txBuffer = CAN_DATA_FIELD_8_BYTES;
	txFifo = CAN_DATA_FIELD_8_BYTES;
}

void CanBusConfig::setRxFifo0Config(int32_t elements, CAN_DATA_FIELD_SIZE size)
{
	rxFifo0Elements = elements;
	rxFifo0 = size;
}

void CanBusConfig::setRxFifo1Config(int32_t elements, CAN_DATA_FIELD_SIZE size)
{
	rxFifo1Elements = elements;
	rxFifo1 = size;
}

void CanBusConfig::setRxBufferConfig(int32_t elements, CAN_DATA_FIELD_SIZE size)
{
	rxBufferElements = elements;
	rxBuffer = size;
}

void CanBusConfig::setTxBufferFifoConfig(int32_t elementsBuffer,int32_t elementsFifo, CAN_DATA_FIELD_SIZE sizeBuffer, CAN_DATA_FIELD_SIZE sizeFifo)
{
	if((elementsFifo + elementsBuffer) > 32)
	{
		elementsBuffer = 32;
		elementsFifo = 0;
	}
	txFifoElements = elementsFifo;
	txFifo = sizeBuffer;
	txBuffer = sizeBuffer;
	txBufferElements = elementsBuffer;
}

void CanBusConfig::setStdFilterElements(int32_t elements)
{
	StdFilterElements = elements;
}

void CanBusConfig::setExtFilterElements(int32_t elements)
{
	ExtFilterElements = elements;
}

int32_t CanBusConfig::getRxFifo0Elements()
{
	if(rxFifo0Elements > 64)
	{
		return 64;
	}
	return rxFifo0Elements;
}
int32_t CanBusConfig::getRxFifo1Elements()
{
	if(rxFifo1Elements > 64)
	{
		return 64;
	}
	return rxFifo1Elements;
}
int32_t CanBusConfig::getRxBufferElements()
{
	if(rxBufferElements > 64)
	{
		return 64;
	}
	return rxBufferElements;
}
int32_t CanBusConfig::getTxBufferElements()
{
	if((txFifoElements + txBufferElements) > 32)
	{
		return 32;
	}
	return txBufferElements;
}
int32_t CanBusConfig::getTxFifoElements()
{
	if((txFifoElements + txBufferElements) > 32)
	{
		return 0;
	}
	return txFifoElements;
}

int32_t CanBusConfig::getStdFilterElements()
{
	if(StdFilterElements > 128)
	{
		return 128;
	}
	return StdFilterElements;
}

int32_t CanBusConfig::getExtFilterElements()
{
	if(ExtFilterElements > 64)
	{
		return 64;
	}
	return ExtFilterElements;
}

int32_t CanBusConfig::getRxFifo0Size()
{
	if(rxFifo0 >= CAN_DATA_FIELD_ERROR)
	{
		return CAN_DATA_FIELD_64_BYTES;
	}
	return rxFifo0;
}

int32_t CanBusConfig::getRxFifo1Size()
{
	if(rxFifo1 >= CAN_DATA_FIELD_ERROR)
	{
		return CAN_DATA_FIELD_64_BYTES;
	}
	return rxFifo1;
}

int32_t CanBusConfig::getRxBufferSize()
{
	if(rxBuffer >= CAN_DATA_FIELD_ERROR)
	{
		return CAN_DATA_FIELD_64_BYTES;
	}
	return rxBuffer;
}

int32_t CanBusConfig::getTxBufferSize()
{
	if(txBuffer >= CAN_DATA_FIELD_ERROR)
	{
		return CAN_DATA_FIELD_64_BYTES;
	}
	return txBuffer;
}

int32_t CanBusConfig::getTxFifoSize()
{
	if(txFifo >= CAN_DATA_FIELD_ERROR)
	{
		return CAN_DATA_FIELD_64_BYTES;
	}
	return txFifo;
}

int32_t CanBusConfig::getOffsetStdId()
{
	return 0;
}

int32_t CanBusConfig::getOffsetExtId()
{
	return getOffsetStdId() + getStdFilterElements();
}

int32_t CanBusConfig::getOffsetRxFifo0()
{
	return getOffsetExtId() + (getExtFilterElements() * getExtFilterSize());
}

int32_t CanBusConfig::getOffsetRxFifo1()
{
	return getOffsetRxFifo0() + (getRxFifo0Elements() * MCAN_DATAFIELD_SIZE_DECODE[getRxFifo0Size()]);
}

int32_t CanBusConfig::getOffsetRxBuffer()
{
	return getOffsetRxFifo1() + (getRxFifo1Elements() * MCAN_DATAFIELD_SIZE_DECODE[getRxFifo1Size()]);
}

int32_t CanBusConfig::getOffsetTxBuffer()
{
	return getOffsetRxBuffer() + (getRxBufferElements() * MCAN_DATAFIELD_SIZE_DECODE[getRxBufferSize()]);
}

int32_t CanBusConfig::getTotalRamSize()
{

	return 	( getStdFilterSize() * getStdFilterElements() ) + \
			( getExtFilterSize() * getExtFilterElements() ) + \
			( MCAN_DATAFIELD_SIZE_DECODE[getRxFifo0Size()] * getRxFifo0Elements() ) + \
			( MCAN_DATAFIELD_SIZE_DECODE[getRxFifo1Size()] * getRxFifo1Elements() ) + \
			( MCAN_DATAFIELD_SIZE_DECODE[getRxBufferSize()] * getRxBufferElements() ) + \
			( MCAN_DATAFIELD_SIZE_DECODE[getTxBufferSize()] * getTxBufferElements() ) + \
			( MCAN_DATAFIELD_SIZE_DECODE[getTxFifoSize()] * getTxFifoElements() );
}

int32_t CanBusConfig::getRamSizeOfElement(CAN_DATA_FIELD_SIZE elementSize)
{
	int32_t sizeinword[8] = {4,5,6,7,8,10,14,18};
	if(elementSize >= CAN_DATA_FIELD_ERROR)
	{
		elementSize = CAN_DATA_FIELD_64_BYTES;
	}
	return sizeinword[elementSize];
}


CanBus::CanBus(Mcan *ptr_CAN, IRQn_Type ID_IRQn, uint32_t *canBusRamBuffer, uint32_t sizeRamBuffer)
{

	_canBusRamBuffer = canBusRamBuffer;
	_ptr_CAN = ptr_CAN;
	_ID_IRQn = ID_IRQn;
	_canConfig = new CanBusConfig();

}

CanBusConfig* CanBus::getCanBusConfiguer()
{
	return _canConfig;
}

CanBusFilterManager* CanBus::getFilterManager()
{
	return filterManager;
}

void CanBus::HandleInterrup()
{
	volatile uint32_t status;
	status = _ptr_CAN->MCAN_IR;
	if(status & MCAN_IE_HPME)
	{
		InterruptHPM();
	}

	if(status & MCAN_IE_RF0NE)
	{
		InterruptNewMessageFifo0();
	}

	if(status & MCAN_IE_RF1NE)
	{
		InterruptNewMessageFifo1();
	}
	if(status & MCAN_IE_DRXE)
	{
		InterruptNewMessageRxBuffer();
	}

	_ptr_CAN->MCAN_IR = 0xffffffff;
}

int32_t CanBus::isTxFifoFull()
{
	return MCAN_isFifoTxFull(_ptr_CAN);
}

int32_t CanBus::ConfigBus(ARM_POWER_STATE state)
{
	MCAN_RAM_SIZES_CONFIG config;

	config.FILTER_STD_NUM = getCanBusConfiguer()->getStdFilterElements();
	config.FILTER_EXT_NUM = getCanBusConfiguer()->getExtFilterElements();
	config.RX_BUF_OBJ_NUM = getCanBusConfiguer()->getRxBufferElements();
	config.RX_FIFO0_ELEM_NUM = getCanBusConfiguer()->getRxFifo0Elements();
	config.RX_FIFO1_ELEM_NUM = getCanBusConfiguer()->getRxFifo1Elements();
	config.TX_FIFO_ELEM_NUM = getCanBusConfiguer()->getTxFifoElements();
	config.TX_BUF_OBJ_NUM = getCanBusConfiguer()->getTxBufferElements();

	config.RX_BUF_OBJ_SIZE = getCanBusConfiguer()->getRxBufferSize();
	config.RX_FIFO0_ELEM_SIZE = getCanBusConfiguer()->getRxFifo0Size();
	config.RX_FIFO1_ELEM_SIZE = getCanBusConfiguer()->getRxFifo1Size();
	config.TX_FIFO_ELEM_SIZE  = getCanBusConfiguer()->getTxFifoSize();
	config.TX_BUF_OBJ_SIZE = getCanBusConfiguer()->getTxBufferSize();


	_can_ram_ptr.PTR_STD_ID = _canBusRamBuffer + getCanBusConfiguer()->getOffsetStdId();
	_can_ram_ptr.PTR_EXT_ID = _canBusRamBuffer + getCanBusConfiguer()->getOffsetExtId();
	_can_ram_ptr.PTR_RX_FIFO0 = _canBusRamBuffer + getCanBusConfiguer()->getOffsetRxFifo0();
	_can_ram_ptr.PTR_RX_FIFO1 = _canBusRamBuffer + getCanBusConfiguer()->getOffsetRxFifo1();
	_can_ram_ptr.PTR_RX_BUFFER = _canBusRamBuffer + getCanBusConfiguer()->getOffsetRxBuffer();
	_can_ram_ptr.PTR_TX_BUFFER = _canBusRamBuffer + getCanBusConfiguer()->getOffsetTxBuffer();

	filterManager = new CanBusFilterManager(&_can_ram_ptr,getCanBusConfiguer()->getStdFilterElements(),getCanBusConfiguer()->getExtFilterElements());//getFilterStdElementsChecked(),getFilterExtElementsChecked());

	MCAN_PowerControlConfig(_ptr_CAN,_ID_IRQn,state,_can_ram_ptr,config);

	return 1;
}

void CanBus::sendMsg(ARM_CAN_MSG_INFO packet, const uint8_t *data)
{
	MCAN_SendMsg(_ptr_CAN,&packet,data,_can_ram_ptr);
}

void CanBus::sendMsgBuffered(ARM_CAN_MSG_INFO packet, const uint8_t *data, int32_t indexBuffer)
{
	MCAN_WriteBuffer(_ptr_CAN,&packet,data,indexBuffer,_can_ram_ptr);
}

int32_t CanBus::getNewDataFromRxBuffer(ARM_CAN_MSG_INFO *msg_info, uint8_t *data)
{
	return MCAN_getNewMsgBuffered(_ptr_CAN,msg_info,data,_can_ram_ptr);
}

int32_t CanBus::getHPMStatus()
{
	return MCAN_GetHPMStatus(_ptr_CAN);
}

int32_t CanBus::getBufferedRxIndex()
{
	return MCAN_GetLastBufferedData(_ptr_CAN);
}

void CanBus::setMode(ARM_CAN_MODE mode)
{
	MCAN_SetMode(_ptr_CAN,mode);
}

int32_t CanBus::getRxFifoStatus(int32_t fifo_num)
{
	return MCAN_GetRxFifoStatus(_ptr_CAN,fifo_num);
}

int32_t CanBus::getLastRxFifoMsg(int32_t fifo_num,ARM_CAN_MSG_INFO *msg_info, uint8_t *data)
{
	return MCAN_GetLastFifoRxData(_ptr_CAN,fifo_num,msg_info,data,_can_ram_ptr);
}

int32_t CanBus::setBitRate(uint32_t bitRateNominal)
{
	uint32_t clock = MCAN_GetClock();
	  int32_t                  status;

	  if ((clock % (8U*bitRateNominal)) == 0U) {                               // If CAN base clock is divisible by 8 * nominal bitrate without remainder
		  status = MCAN_SetBitrate    (MCAN0,ARM_CAN_BITRATE_NOMINAL,                    // Set nominal bitrate
				  bitRateNominal,                        // Set nominal bitrate to configured constant value
				  ARM_CAN_BIT_PROP_SEG  (5U) |                // Set propagation segment to 5 time quanta
				  ARM_CAN_BIT_PHASE_SEG1(1U) |                // Set phase segment 1 to 1 time quantum (sample point at 87.5% of bit time)
				  ARM_CAN_BIT_PHASE_SEG2(1U) |                // Set phase segment 2 to 1 time quantum (total bit is 8 time quanta long)
				  ARM_CAN_BIT_SJW       (1U),getClockTolerance());                // Resynchronization jump width is same as phase segment 2
	  } else if ((clock % (10U*bitRateNominal)) == 0U) {                       // If CAN base clock is divisible by 10 * nominal bitrate without remainder
		  status = MCAN_SetBitrate    (MCAN0,ARM_CAN_BITRATE_NOMINAL,                    // Set nominal bitrate
				  bitRateNominal,                        // Set nominal bitrate to configured constant value
				  ARM_CAN_BIT_PROP_SEG  (7U) |                // Set propagation segment to 7 time quanta
				  ARM_CAN_BIT_PHASE_SEG1(1U) |                // Set phase segment 1 to 1 time quantum (sample point at 90% of bit time)
				  ARM_CAN_BIT_PHASE_SEG2(1U) |                // Set phase segment 2 to 1 time quantum (total bit is 10 time quanta long)
				  ARM_CAN_BIT_SJW       (1U),getClockTolerance());                // Resynchronization jump width is same as phase segment 2
	  } else if ((clock % (12U*bitRateNominal)) == 0U) {                       // If CAN base clock is divisible by 12 * nominal bitrate without remainder
		  status = MCAN_SetBitrate    (MCAN0,ARM_CAN_BITRATE_NOMINAL,                    // Set nominal bitrate
				  bitRateNominal,                        // Set nominal bitrate to configured constant value
				  ARM_CAN_BIT_PROP_SEG  (7U) |                // Set propagation segment to 7 time quanta
				  ARM_CAN_BIT_PHASE_SEG1(2U) |                // Set phase segment 1 to 2 time quantum (sample point at 83.3% of bit time)
				  ARM_CAN_BIT_PHASE_SEG2(2U) |                // Set phase segment 2 to 2 time quantum (total bit is 12 time quanta long)
				  ARM_CAN_BIT_SJW       (2U),getClockTolerance());                // Resynchronization jump width is same as phase segment 2
	  }
	  return status;
}

void CanBus::configInterrupt(MCan_IntrTypeMode obj_cfg, uint32_t interruptType)
{
	//_ptr_CAN->US_IDR = 0xffffffff;
	//USART_EnableIt(_port,mask);

	MCAN_ConfigureRam(_ptr_CAN,obj_cfg,interruptType);
	NVIC_EnableIRQ(_ID_IRQn);
}

CanBus::~CanBus()
{

}
uint32_t CanBus::getClockTolerance()
{
	return MCAN_DEFAULT_CLOCK_TOLERANCE;
}

uint32_t CanBus::getBitRate()
{
	return MCAN_DEFAULT_BITRATE;
}

bool CanBus::getTransmitionDelayCompensation()
{
	return MCAN_DEFAULT_TRANSMITTER_DELAY_COMPENSATION_EN;
}

uint32_t CanBus::getTransmitionDelayOffset()
{
	return MCAN_DEFAULT_TRANSMITTER_DELAY_OFFSET;
}

uint32_t CanBus::getTransmitionDelayFilter()
{
	return MCAN_DEFAULT_TRANSMITTER_DELAY_FILTER;
}

}
