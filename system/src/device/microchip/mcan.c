#include "chip.h"
#include "mcan.h"
#include "Driver_Common.h"
#include "Driver_CAN.h"
#include "board.h"

#include <stdint.h>
#include <string.h>

static MCAN_RAM_SIZES_CONFIG _can_config_sizes;
static const  uint8_t MCAN_DLC_ENCODE [16] =  { 0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 12U, 16U, 20U, 24U, 32U, 48U, 64U };
static const uint8_t MCAN_DATAFIELD_SIZE_DECODE[8] = {4U,5U,6U,7U,8U,10U,14U,18U};

int32_t MCAN_Initialize ()
{
  return ARM_DRIVER_OK;
}

int32_t MCAN_PowerControlConfig(Mcan *ptr_CAN, IRQn_Type ID_IRQn, ARM_POWER_STATE state, MCAN_RAM_PTR_t can_ram, MCAN_RAM_SIZES_CONFIG can_config_sizes)
{
	uint32_t tout;
	uint32_t peripherical_id;

	_can_config_sizes = can_config_sizes;
	if(ptr_CAN == MCAN0)
	{
		peripherical_id = ID_MCAN0;
	}
	else if(ptr_CAN == MCAN1)
	{
		peripherical_id = ID_MCAN1;
	}
	else
	{
		return ARM_DRIVER_ERROR;
	}

	switch (state) {
	    case ARM_POWER_OFF:
	      PMC_EnablePeripheral  (peripherical_id);
	      ptr_CAN->MCAN_IE   =   0U;
	      ptr_CAN->MCAN_IR   =   0xFFFFFFFFU;
	      ptr_CAN->MCAN_ILE &=  ~MCAN_ILE_EINT0;
	      NVIC_ClearPendingIRQ  (ID_IRQn);
	      NVIC_DisableIRQ       (ID_IRQn);
	      PMC_DisablePeripheral (peripherical_id);
	      break;

	    case ARM_POWER_FULL:
	      // Not used: Select MCANx clock to = MCK/1 clock = 150 MHz
	      // PMC->PMC_PCK[5] = PMC_PCK_PRES(0U) | PMC_PCK_CSS_MCK;

	      // Select MCANx clock to = UPLL/6 clock = 80 MHz
	      if ((PMC->PMC_SR & PMC_SR_LOCKU) == 0U) {                         // If UPLL is not already running
	        PMC->CKGR_UCKR = CKGR_UCKR_UPLLEN | CKGR_UCKR_UPLLCOUNT(0x0FU); // Enable UPLL 480 MHz
	        tout = SystemCoreClock / 2U;
	        while(((PMC->PMC_SR & PMC_SR_LOCKU) == 0U) && (tout > 0U)) {    // Wait that UPLL is considered locked by the PMC
	          tout --;
	        }
	        if (tout == 0U) { return ARM_DRIVER_ERROR; }
	      }
	      PMC->PMC_PCK[5] = PMC_PCK_PRES(5U) | PMC_PCK_CSS_UPLL_CLK;

	      PMC->PMC_SCER   = PMC_SCER_PCK5;

	      PMC_EnablePeripheral (peripherical_id);

	      // Set peripheral to initialization and configuration change enable to set RAM pointers
	      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT; while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
	      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;


	      if(ptr_CAN == MCAN0)
	      {
	#if (defined(RTE_CAN0) && (RTE_CAN0 == 1U))

	    	  MATRIX->CCFG_CAN0  = (MATRIX->CCFG_CAN0  & 0x0000FFFFU) | ((uint32_t)((can_ram.PTR_STD_ID)) & 0xFFFF0000U);

	    	  // Clear RAM for CAN0
	    	  //memset((void *)(&can_ram), 0U, ram_len);
	#endif

	      }
	      else if(ptr_CAN == MCAN1)
	      {
	#if (defined(RTE_CAN1) && (RTE_CAN1 == 1U))
	    	  MATRIX->CCFG_SYSIO = (MATRIX->CCFG_SYSIO & 0x0000FFFFU) | (((uint32_t)CAN1_RAM) & 0xFFFF0000U);

	    	  // Clear RAM for CAN1
	    	  memset((void *)CAN1_RAM, 0U, sizeof(CAN1_RAM));
	#endif
	      }

	      // Configure memory areas for filters and Rx/Tx buffers, FIFOs for Rx and Tx are not used
	      // Rx Buffers are configured for 64 byte data field to accommodate maximum CAN FD message
	      // Tx Buffers are configured for 64 byte data field to accommodate maximum CAN FD message
	      ptr_CAN->MCAN_SIDFC = MCAN_SIDFC_LSS  (can_config_sizes.FILTER_STD_NUM)                    |
	                            MCAN_SIDFC_FLSSA(((uint32_t)(can_ram.PTR_STD_ID))        >> 2) ;
	      ptr_CAN->MCAN_XIDFC = MCAN_XIDFC_LSE  (can_config_sizes.FILTER_EXT_NUM)                    |
	                            MCAN_XIDFC_FLESA(((uint32_t)(can_ram.PTR_EXT_ID))        >> 2) ;
	      ptr_CAN->MCAN_RXF0C = MCAN_RXF0C_F0S  (can_config_sizes.RX_FIFO0_ELEM_NUM)                 |
	                            MCAN_RXF0C_F0SA (((uint32_t)(can_ram.PTR_RX_FIFO0))      >> 2) ;
	      ptr_CAN->MCAN_RXF1C = MCAN_RXF1C_F1S  (can_config_sizes.RX_FIFO1_ELEM_NUM)                 |
	                            MCAN_RXF1C_F1SA (((uint32_t)(can_ram.PTR_RX_FIFO1))      >> 2) ;
	      ptr_CAN->MCAN_RXBC  = MCAN_RXBC_RBSA  (((uint32_t)(can_ram.PTR_RX_BUFFER))     >> 2) ;
	      ptr_CAN->MCAN_RXESC = MCAN_RXESC_F0DS (can_config_sizes.RX_FIFO0_ELEM_SIZE)                                                  |
	                            MCAN_RXESC_F1DS (can_config_sizes.RX_FIFO1_ELEM_SIZE)                                                  |
	                            MCAN_RXESC_RBDS (can_config_sizes.RX_BUF_OBJ_SIZE)                                                  ;
	      ptr_CAN->MCAN_TXEFC = MCAN_TXEFC_EFS  (0)                  |
	                            MCAN_TXEFC_EFSA (0) ;
	      ptr_CAN->MCAN_TXBC  = MCAN_TXBC_NDTB  (can_config_sizes.TX_BUF_OBJ_NUM)                    |
	                            MCAN_TXBC_TFQS  (can_config_sizes.TX_FIFO_ELEM_NUM)                  |
	                            MCAN_TXBC_TBSA  (((uint32_t)(can_ram.PTR_TX_BUFFER))     >> 2) ;
	      ptr_CAN->MCAN_TXESC = MCAN_TXESC_TBDS (can_config_sizes.TX_FIFO_ELEM_SIZE)                                                  ;

	      // Reject non-matching frames
	      ptr_CAN->MCAN_GFC   = MCAN_GFC_ANFE(3U) | MCAN_GFC_ANFS(3U);

	      // Route all interrupt sources to m_can_int0 and enable it
	      ptr_CAN->MCAN_ILS = 0U;
	      ptr_CAN->MCAN_ILE = MCAN_ILE_EINT0;

	      // Enable relevant interrupts
	      ptr_CAN->MCAN_IR  = 0xFFFFFFFFU;
	      ptr_CAN->MCAN_IE  = MCAN_IE_TCE | MCAN_IE_DRXE | MCAN_IE_EPE | MCAN_IE_EWE | MCAN_IE_BOE;


	      NVIC_ClearPendingIRQ (ID_IRQn);

	      //NO ACTIVO LA INTERRUPCION POR DEFECTO!!!
	//      if ((CAN_SignalUnitEvent[x] != NULL) || (CAN_SignalObjectEvent[x] != NULL)) {
	//        NVIC_EnableIRQ     (ID_IRQn);
	//      }

	      break;

	    default:
	      return ARM_DRIVER_ERROR;
	  }

	  return ARM_DRIVER_OK;
}

int32_t MCAN_SetMode (Mcan *ptr_CAN, ARM_CAN_MODE mode) {

	uint32_t  event;

	uint32_t peripherical_id;
	if(ptr_CAN == MCAN0)
	{
		peripherical_id = ID_MCAN0;
	}
	else if(ptr_CAN == MCAN1)
	{
		peripherical_id = ID_MCAN1;
	}
	else
	{
		return ARM_DRIVER_ERROR;
	}

  event = 0U;
  switch (mode) {
    case ARM_CAN_MODE_INITIALIZATION:
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE | MCAN_CCCR_TEST_ENABLED;
      ptr_CAN->MCAN_TEST = 0U;
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;
      event              = ARM_CAN_EVENT_UNIT_BUS_OFF;
      break;
    case ARM_CAN_MODE_NORMAL:
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE | MCAN_CCCR_TEST_ENABLED;
      ptr_CAN->MCAN_TEST = 0U;
      ptr_CAN->MCAN_CCCR = 0U;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 1U);
      event              = ARM_CAN_EVENT_UNIT_ACTIVE;
      break;
    case ARM_CAN_MODE_RESTRICTED:
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE | MCAN_CCCR_TEST_ENABLED | MCAN_CCCR_ASM_RESTRICTED;
      ptr_CAN->MCAN_TEST = 0U;
      ptr_CAN->MCAN_CCCR =                                                           MCAN_CCCR_ASM_RESTRICTED;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 1U);
      event              = ARM_CAN_EVENT_UNIT_ACTIVE;
      break;
    case ARM_CAN_MODE_MONITOR:
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE | MCAN_CCCR_TEST_ENABLED | MCAN_CCCR_MON_ENABLED;
      ptr_CAN->MCAN_TEST = 0U;
      ptr_CAN->MCAN_CCCR =                                                           MCAN_CCCR_MON_ENABLED;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 1U);
      event              = ARM_CAN_EVENT_UNIT_PASSIVE;
      break;
    case ARM_CAN_MODE_LOOPBACK_INTERNAL:
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE | MCAN_CCCR_TEST_ENABLED | MCAN_CCCR_MON_ENABLED;
      ptr_CAN->MCAN_TEST = MCAN_TEST_LBCK_ENABLED;
      ptr_CAN->MCAN_CCCR =                                  MCAN_CCCR_TEST_ENABLED | MCAN_CCCR_MON_ENABLED;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 1U);
      event              = ARM_CAN_EVENT_UNIT_PASSIVE;
      break;
    case ARM_CAN_MODE_LOOPBACK_EXTERNAL:
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE;
      ptr_CAN->MCAN_CCCR = MCAN_CCCR_INIT | MCAN_CCCR_CCE | MCAN_CCCR_TEST_ENABLED;
      ptr_CAN->MCAN_TEST = MCAN_TEST_LBCK_ENABLED;
      ptr_CAN->MCAN_CCCR =                                  MCAN_CCCR_TEST_ENABLED;
      while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 1U);
      event              = ARM_CAN_EVENT_UNIT_ACTIVE;
      break;
    default:
      return ARM_DRIVER_ERROR_PARAMETER;
  }

  return ARM_DRIVER_OK;
}

uint32_t MCAN_GetClock (void) {
  uint32_t clk;

  switch ((PMC->PMC_PCK[5] & PMC_PCK_CSS_Msk) >> PMC_PCK_CSS_Pos) {
    case PMC_PCK_CSS_SLOW_CLK:
      if ((SUPC->SUPC_SR & SUPC_SR_OSCSEL) != 0U) {
        clk = CHIP_FREQ_XTAL_32K;
      } else {
        clk = CHIP_FREQ_SLCK_RC;
      }
      break;
    case PMC_PCK_CSS_MAIN_CLK:
      if ((PMC->CKGR_MOR & CKGR_MOR_MOSCSEL) != 0U) {
        clk = CHIP_FREQ_XTAL_12M;
      } else {
        clk = CHIP_FREQ_MAINCK_RC_4MHZ;

        switch (PMC->CKGR_MOR & CKGR_MOR_MOSCRCF_Msk) {
          case CKGR_MOR_MOSCRCF_4_MHz:
            break;
          case CKGR_MOR_MOSCRCF_8_MHz:
            clk *= 2U;
            break;
          case CKGR_MOR_MOSCRCF_12_MHz:
            clk *= 3U;
            break;
          default:
            break;
        }
      }

      if ((PMC->PMC_MCKR & PMC_MCKR_PRES_Msk) == PMC_MCKR_PRES_CLK_3) {
        clk /= 3U;
      } else {
        clk >>= ((PMC->PMC_MCKR & PMC_MCKR_PRES_Msk) >> PMC_MCKR_PRES_Pos);
      }
      break;
    case PMC_PCK_CSS_PLLA_CLK:
      if ((PMC->CKGR_MOR & CKGR_MOR_MOSCSEL) != 0U) {
        clk = CHIP_FREQ_XTAL_12M;
      } else {
        clk = CHIP_FREQ_MAINCK_RC_4MHZ;

        switch (PMC->CKGR_MOR & CKGR_MOR_MOSCRCF_Msk) {
          case CKGR_MOR_MOSCRCF_4_MHz:
            break;
          case CKGR_MOR_MOSCRCF_8_MHz:
            clk *= 2U;
            break;
          case CKGR_MOR_MOSCRCF_12_MHz:
            clk *= 3U;
            break;
          default:
            break;
        }
      }

      clk *= ((((PMC->CKGR_PLLAR) & CKGR_PLLAR_MULA_Msk) >> CKGR_PLLAR_MULA_Pos) + 1U);
      clk /= ((((PMC->CKGR_PLLAR) & CKGR_PLLAR_DIVA_Msk) >> CKGR_PLLAR_DIVA_Pos));
      break;
    case PMC_PCK_CSS_MCK:
      clk = SystemCoreClock / (((PMC->PMC_MCKR & PMC_MCKR_MDIV_Msk) >> PMC_MCKR_MDIV_Pos) + 1U);
      break;
    case PMC_PCK_CSS_UPLL_CLK:
      clk = 480000000U / (((PMC->PMC_MCKR & PMC_MCKR_UPLLDIV2) >> 13) + 1U);
      break;
    default:
      clk = 0U;
      break;
  }
  clk /= ((PMC->PMC_PCK[5] & PMC_PCK_PRES_Msk) >> PMC_PCK_PRES_Pos) + 1U;

  return clk;
}

int32_t MCAN_SetBitrate (Mcan *ptr_CAN, ARM_CAN_BITRATE_SELECT select, uint32_t bitrate, uint32_t bit_segments, uint32_t clock_tolerance) {

  uint32_t  cccr, sjw, prop_seg, phase_seg1, phase_seg2, pclk, brp, brp_max, tq_num;


  uint32_t peripherical_id;
  	if(ptr_CAN == MCAN0)
  	{
  		peripherical_id = ID_MCAN0;
  	}
  	else if(ptr_CAN == MCAN1)
  	{
  		peripherical_id = ID_MCAN1;
  	}
  	else
  	{
  		return ARM_DRIVER_ERROR;
  	}

  if ((select != ARM_CAN_BITRATE_NOMINAL) && (select != ARM_CAN_BITRATE_FD_DATA)  ) { return ARM_CAN_INVALID_BITRATE_SELECT; }

  prop_seg   = (bit_segments & ARM_CAN_BIT_PROP_SEG_Msk  ) >> ARM_CAN_BIT_PROP_SEG_Pos;
  phase_seg1 = (bit_segments & ARM_CAN_BIT_PHASE_SEG1_Msk) >> ARM_CAN_BIT_PHASE_SEG1_Pos;
  phase_seg2 = (bit_segments & ARM_CAN_BIT_PHASE_SEG2_Msk) >> ARM_CAN_BIT_PHASE_SEG2_Pos;
  sjw        = (bit_segments & ARM_CAN_BIT_SJW_Msk       ) >> ARM_CAN_BIT_SJW_Pos;

#if (CHIP_REV_A == 1)                           // Chip revision MRL A
  if (select == ARM_CAN_BITRATE_NOMINAL) {
    if (((prop_seg + phase_seg1) < 2U) || ((prop_seg + phase_seg1) >  64U)) { return ARM_CAN_INVALID_BIT_PROP_SEG;   }
    if (( phase_seg2             < 1U) || ( phase_seg2             >  16U)) { return ARM_CAN_INVALID_BIT_PHASE_SEG2; }
    if (( sjw                    < 1U) || ( sjw                    >  16U)) { return ARM_CAN_INVALID_BIT_SJW;        }
    brp_max = 1024U;
  } else {
    if (((prop_seg + phase_seg1) < 2U) || ((prop_seg + phase_seg1) >  16U)) { return ARM_CAN_INVALID_BIT_PROP_SEG;   }
    if (( phase_seg2             < 1U) || ( phase_seg2             >   8U)) { return ARM_CAN_INVALID_BIT_PHASE_SEG2; }
    if (( sjw                    < 1U) || ( sjw                    >   4U)) { return ARM_CAN_INVALID_BIT_SJW;        }
    brp_max = 32U;
  }
#else                                           // Chip revision MRL B
  if (select == ARM_CAN_BITRATE_NOMINAL) {
    if (((prop_seg + phase_seg1) < 2U) || ((prop_seg + phase_seg1) > 256U)) { return ARM_CAN_INVALID_BIT_PROP_SEG;   }
    if (( phase_seg2             < 1U) || ( phase_seg2             > 128U)) { return ARM_CAN_INVALID_BIT_PHASE_SEG2; }
    if (( sjw                    < 1U) || ( sjw                    > 128U)) { return ARM_CAN_INVALID_BIT_SJW;        }
    brp_max = 512U;
  } else {
    if (((prop_seg + phase_seg1) < 2U) || ((prop_seg + phase_seg1) >  32U)) { return ARM_CAN_INVALID_BIT_PROP_SEG;   }
    if (( phase_seg2             < 1U) || ( phase_seg2             >  16U)) { return ARM_CAN_INVALID_BIT_PHASE_SEG2; }
    if (( sjw                    < 1U) || ( sjw                    >   8U)) { return ARM_CAN_INVALID_BIT_SJW;        }
    brp_max = 32U;
  }
#endif

  tq_num = 1U + prop_seg + phase_seg1 + phase_seg2;
  pclk   = MCAN_GetClock ();           if (pclk == 0U)    { return ARM_DRIVER_ERROR;        }
  brp    = pclk / (tq_num * bitrate); if (brp > brp_max) { return ARM_CAN_INVALID_BITRATE; }
  if (pclk > (brp * tq_num * bitrate)) {
    if ((((pclk - (brp * tq_num * bitrate)) * 1024U) / pclk) > clock_tolerance) { return ARM_CAN_INVALID_BITRATE; }
  } else if (pclk < (brp * tq_num * bitrate)) {
    if (((((brp * tq_num * bitrate) - pclk) * 1024U) / pclk) > clock_tolerance) { return ARM_CAN_INVALID_BITRATE; }
  }

  cccr = ptr_CAN->MCAN_CCCR;
  if ((cccr & (MCAN_CCCR_CCE | MCAN_CCCR_INIT)) != (MCAN_CCCR_CCE | MCAN_CCCR_INIT)) {
    ptr_CAN->MCAN_CCCR = MCAN_CCCR_CCE  |       // Configuration change enable
                         MCAN_CCCR_INIT ;       // Initialization
    while ((ptr_CAN->MCAN_CCCR & MCAN_CCCR_INIT) == 0U);
  }
#if (CHIP_REV_A == 1)                           // Chip revision MRL A
  if (select == ARM_CAN_BITRATE_NOMINAL) {
    ptr_CAN->MCAN_BTP  = MCAN_BTP_BRP  (brp - 1U) | MCAN_BTP_TSEG1  (prop_seg + phase_seg1 - 1U) | MCAN_BTP_TSEG2  (phase_seg2 - 1U) | MCAN_BTP_SJW  (sjw - 1U);
  } else {
    ptr_CAN->MCAN_FBTP = MCAN_FBTP_FBRP(brp - 1U) | MCAN_FBTP_FTSEG1(prop_seg + phase_seg1 - 1U) | MCAN_FBTP_FTSEG2(phase_seg2 - 1U) | MCAN_FBTP_FSJW(sjw - 1U);
  }
#else                                           // Chip revision MRL B
  if (select == ARM_CAN_BITRATE_NOMINAL) {
    // Register was renamed to NBTP but it is on same location as BTP
    ptr_CAN->MCAN_NBTP = ((brp - 1U) << 16) | ((prop_seg + phase_seg1 - 1U) << 8) |  (phase_seg2 - 1U)       | ((sjw - 1U) << 25);
  } else {
    // Register was renamed to DBTP but it is on same location as FBTP
    ptr_CAN->MCAN_DBTP = ((brp - 1U) << 16) | ((prop_seg + phase_seg1 - 1U) << 8) | ((phase_seg2 - 1U) << 4) |  (sjw - 1U);
  }
#endif
  ptr_CAN->MCAN_CCCR = cccr;

  return ARM_DRIVER_OK;
}


int32_t MCAN_isFifoTxFull(Mcan *ptr_CAN)
{
	return (ptr_CAN->MCAN_TXFQS & MCAN_TXFQS_TFQF);
}

void MCAN_SendMsg(Mcan *ptr_CAN,ARM_CAN_MSG_INFO *msg_info,const uint8_t *data,MCAN_RAM_PTR_t can_ram)
{
	uint32_t *ptr_buffer_RAM;
	uint32_t *cache_addr;
	uint32_t  t0, i, cache_size, tx_obj_idx, buf_ofs_idx;
	int32_t size = msg_info->dlc;

	int32_t datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.TX_BUF_OBJ_SIZE];

	// Send using Tx FIFO
	buf_ofs_idx = ((ptr_CAN->MCAN_TXFQS & MCAN_TXFQS_TFQPI_Msk) >> MCAN_TXFQS_TFQPI_Pos);
	ptr_buffer_RAM = can_ram.PTR_TX_BUFFER + (buf_ofs_idx * datafieldsize);

	if ((msg_info->id & ARM_CAN_ID_IDE_Msk) == 0U) {      // Standard ID
		t0 = (msg_info->id & 0x7FFU) << 18;
	} else {                                              // Extended ID
		t0 = (msg_info->id & 0x1FFFFFFFU) | (1U << 30);
	}

	if (msg_info->rtr != 0U) {                            // RTR message
		t0 |= 1U << 29;
		ptr_buffer_RAM[0] = t0;
	} else {                                              // Data message
		ptr_buffer_RAM[0] = t0;
		// If not FD mode
		if (size > 8U) { size = 8U; }
		ptr_buffer_RAM[1] = size << 16;
		// Send using Tx FIFO
		ptr_buffer_RAM[1] |= (1U << 23);

		memcpy ((uint8_t *)(ptr_buffer_RAM + 2U), data, size);
	}

#if (defined(__DCACHE_PRESENT) && (__DCACHE_PRESENT == 1U))
	if ((SCB->CCR & SCB_CCR_DC_Msk) != 0U) {              // If Data Cache is enabled
		cache_addr = (uint32_t *)((uint32_t)ptr_buffer_RAM & (~0x1FU));
		cache_size = (uint32_t)ptr_buffer_RAM + 8U + size - (uint32_t)cache_addr;
		SCB_CleanDCache_by_Addr(cache_addr, cache_size);
	}
#endif

	ptr_CAN->MCAN_TXBAR = 1U << buf_ofs_idx;

}

void MCAN_WriteBuffer(Mcan *ptr_CAN,ARM_CAN_MSG_INFO *msg_info,const uint8_t *data, int32_t indexBuffer, MCAN_RAM_PTR_t can_ram)
{
	uint32_t *ptr_buffer_RAM;
	uint32_t *cache_addr;
	uint32_t  t0, i, cache_size, tx_obj_idx;
	int32_t size = msg_info->dlc;

	int32_t datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.TX_BUF_OBJ_SIZE];

	// Send using Tx Buffer
	ptr_buffer_RAM = can_ram.PTR_TX_BUFFER + (indexBuffer * datafieldsize); //Estos son los 8 fifos supuestamente.

	if ((msg_info->id & ARM_CAN_ID_IDE_Msk) == 0U) {      // Standard ID
		t0 = (msg_info->id & 0x7FFU) << 18;
	} else {                                              // Extended ID
		t0 = (msg_info->id & 0x1FFFFFFFU) | (1U << 30);
	}

	if (msg_info->rtr != 0U) {                            // RTR message
		t0 |= 1U << 29;
		ptr_buffer_RAM[0] = t0;
	} else {                                              // Data message
		ptr_buffer_RAM[0] = t0;
		// If not FD mode
		if (size > 8U) { size = 8U; }
		ptr_buffer_RAM[1] = size << 16;
		// Send using Tx FIFO
		ptr_buffer_RAM[1] |= (1U << 23);

		memcpy ((uint8_t *)(ptr_buffer_RAM + 2U), data, size);
	}

#if (defined(__DCACHE_PRESENT) && (__DCACHE_PRESENT == 1U))
	if ((SCB->CCR & SCB_CCR_DC_Msk) != 0U) {              // If Data Cache is enabled
		cache_addr = (uint32_t *)((uint32_t)ptr_buffer_RAM & (~0x1FU));
		cache_size = (uint32_t)ptr_buffer_RAM + 8U + size - (uint32_t)cache_addr;
		SCB_CleanDCache_by_Addr(cache_addr, cache_size);
	}
#endif

	ptr_CAN->MCAN_TXBAR = 1U << indexBuffer;

}

int32_t MCAN_MessageSend (Mcan *ptr_CAN, uint32_t obj_idx, ARM_CAN_MSG_INFO *msg_info, const uint8_t *data, uint8_t size, MCAN_DRV_CONFIG_t can_conf, MCAN_RAM_PTR_t can_ram, uint32_t ram_len, uint8_t isFdMode) {

  uint32_t *ptr_buffer_RAM;
  uint32_t *cache_addr;
  uint32_t  t0, i, cache_size, tx_obj_idx, buf_ofs_idx;

  int32_t datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.TX_BUF_OBJ_SIZE];

  tx_obj_idx = (obj_idx - can_conf.RX_TOT_OBJ_NUM) & 0x1FU;

  if (obj_idx < can_conf.TX_FIFO_OBJ_IDX_MAX) {
    // Send using Tx FIFO
    buf_ofs_idx = ((ptr_CAN->MCAN_TXFQS & MCAN_TXFQS_TFQPI_Msk) >> MCAN_TXFQS_TFQPI_Pos);
  } else {
    // Send using Tx Buffer
    buf_ofs_idx = tx_obj_idx;
  }
  ptr_buffer_RAM = can_ram.PTR_TX_BUFFER + (buf_ofs_idx * datafieldsize);

  if ((msg_info->id & ARM_CAN_ID_IDE_Msk) == 0U) {      // Standard ID
    t0 = (msg_info->id & 0x7FFU) << 18;
  } else {                                              // Extended ID
    t0 = (msg_info->id & 0x1FFFFFFFU) | (1U << 30);
  }

  if (msg_info->rtr != 0U) {                            // RTR message
    t0 |= 1U << 29;
    ptr_buffer_RAM[0] = t0;
  } else {                                              // Data message
    ptr_buffer_RAM[0] = t0;
    if (isFdMode != 0U) {                  // If FD mode
      ptr_buffer_RAM[1] = (1U << 21) | (1U << 20);      // Set FDF (FD Format) and BRS (Bit Rate Switching)
      if (size > 64U) { size = 64U; }
      for (i = 0U; i < 16U; i++) {
        if (MCAN_DLC_ENCODE[i] >= size) {
          ptr_buffer_RAM[1] |= i << 16;
          break;
        }
      }
    } else {                                            // If not FD mode
      if (size > 8U) { size = 8U; }
      ptr_buffer_RAM[1] = size << 16;
    }
    if (obj_idx < can_conf.TX_FIFO_OBJ_IDX_MAX) {
      // Send using Tx FIFO
      ptr_buffer_RAM[1] |= (1U << 23);
    }

    memcpy ((uint8_t *)(ptr_buffer_RAM + 2U), data, size);
  }

#if (defined(__DCACHE_PRESENT) && (__DCACHE_PRESENT == 1U))
  if ((SCB->CCR & SCB_CCR_DC_Msk) != 0U) {              // If Data Cache is enabled
    cache_addr = (uint32_t *)((uint32_t)ptr_buffer_RAM & (~0x1FU));
    cache_size = (uint32_t)ptr_buffer_RAM + 8U + size - (uint32_t)cache_addr;
    SCB_CleanDCache_by_Addr(cache_addr, cache_size);
  }
#endif

  ptr_CAN->MCAN_TXBAR = 1U << buf_ofs_idx;

  return ((int32_t)size);
}

int32_t MCAN_GetHPMStatus(Mcan *ptr_CAN)
{
	return ptr_CAN->MCAN_HPMS;
}

int32_t MCAN_GetLastBufferedData(Mcan *ptr_CAN)
{
	int32_t index = 0;

	while(index < 32)
	{
		if(ptr_CAN->MCAN_NDAT2 & (1 << (31 - index)) )
		{
			return ((31 - index)) + 32;
		}
		index++;
	}

	index = 0;

	while(index < 32)
	{
		if(ptr_CAN->MCAN_NDAT1 & (1 << (31 - index)) )
		{
			return ((31 - index));
		}
		index++;
	}

	return -1;
}

int32_t MCAN_getNewMsgBuffered(Mcan *ptr_CAN,ARM_CAN_MSG_INFO *msg_info,uint8_t *data,MCAN_RAM_PTR_t can_ram)
{
	int32_t indexND = MCAN_GetLastBufferedData(ptr_CAN);
	if(indexND != -1)
	{
		MCAN_ReadMsgBuffered(ptr_CAN,indexND,msg_info,data,can_ram);
	}
	return indexND;
}

void MCAN_ReadMsgBuffered(Mcan *ptr_CAN, uint32_t buffer_idx, ARM_CAN_MSG_INFO *msg_info, uint8_t *data, MCAN_RAM_PTR_t can_ram)
{
	uint32_t *ptr_buffer_RAM;
	uint32_t *cache_addr;
	uint32_t  r, cache_size;
	uint32_t size = 8;
    // Read message received with Rx Buffer

	int32_t datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.RX_BUF_OBJ_SIZE];

	ptr_buffer_RAM = can_ram.PTR_RX_BUFFER + ((buffer_idx) * datafieldsize);

#if (defined(__DCACHE_PRESENT) && (__DCACHE_PRESENT == 1U))
  if ((SCB->CCR & SCB_CCR_DC_Msk) != 0U) {      // If Data Cache is enabled
	cache_addr = (uint32_t *)((uint32_t)ptr_buffer_RAM & (~0x1FU));
	cache_size = (uint32_t)ptr_buffer_RAM + size - (uint32_t)cache_addr;
	SCB_InvalidateDCache_by_Addr(cache_addr, cache_size);
  }
#endif

	  r = ptr_buffer_RAM[0];                // Read R0
	  msg_info->esi = (r >> 31);            // Get Error Status Indicator flag
	  if ((r & (1U << 30)) != 0U) {         // If Extended ID
	    msg_info->id = ((r      ) & 0x1FFFFFFFU) | ARM_CAN_ID_IDE_Msk;
	  } else {                              // If Standard ID
	    msg_info->id = ((r >> 18) & 0x7FFU);
	  }
	  if ((r & (1U << 29)) != 0U) {         // If RTR message
	    msg_info->rtr = 1U;
	  } else {                              // If Data message
	    msg_info->rtr = 0U;
	  }
	  r = ptr_buffer_RAM[1];                // Read R1
	  msg_info->edl = (r >> 21) &  1U;      // Get Extended Data Length flag
	  msg_info->brs = (r >> 20) &  1U;      // Get Baud Rate Switching flag
	  msg_info->dlc = (r >> 16) & 15U;      // Get Data Length Code
	  size = msg_info->dlc;

	  if (size > 0U) {
	    memcpy (data, (uint8_t *)(ptr_buffer_RAM + 2U), size);
	  }
	// Clear new data flag and release Rx buffer
	if (buffer_idx < 32U) {
	  ptr_CAN->MCAN_NDAT1 = 1 <<  buffer_idx;
	} else {
	  ptr_CAN->MCAN_NDAT2 = 1 << (buffer_idx - 32U);
	}

}

int32_t MCAN_GetRxFifoStatus(Mcan *ptr_CAN, int32_t fifo_num)
{
	if(fifo_num == 0)
	{
		return ptr_CAN->MCAN_RXF0S;
	}
	else if(fifo_num == 1)
	{
		return ptr_CAN->MCAN_RXF1S;
	}
	return -1;
}

int32_t MCAN_GetLastFifoRxData(Mcan *ptr_CAN,int32_t fifo_num, ARM_CAN_MSG_INFO *msg_info, uint8_t *data, MCAN_RAM_PTR_t can_ram)
{
	  uint32_t *ptr_buffer_RAM;
	  uint32_t *cache_addr;
	  uint32_t  r, cache_size;
	  int32_t size;

	  int32_t datafieldsize;

	    if (fifo_num == 0U) {
	      datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.RX_FIFO0_ELEM_SIZE];
	      ptr_buffer_RAM = can_ram.PTR_RX_FIFO0 + (((ptr_CAN->MCAN_RXF0S & MCAN_RXF0S_F0GI_Msk) >> MCAN_RXF0S_F0GI_Pos) * datafieldsize);
	    } else {
	      datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.RX_FIFO1_ELEM_SIZE];
	      ptr_buffer_RAM = can_ram.PTR_RX_FIFO1 + (((ptr_CAN->MCAN_RXF1S & MCAN_RXF1S_F1GI_Msk) >> MCAN_RXF1S_F1GI_Pos) * datafieldsize);
	    }

	#if (defined(__DCACHE_PRESENT) && (__DCACHE_PRESENT == 1U))
	  if ((SCB->CCR & SCB_CCR_DC_Msk) != 0U) {      // If Data Cache is enabled
	    cache_addr = (uint32_t *)((uint32_t)ptr_buffer_RAM & (~0x1FU));
	    cache_size = (uint32_t)ptr_buffer_RAM + size - (uint32_t)cache_addr;
	    SCB_InvalidateDCache_by_Addr(cache_addr, cache_size);
	  }
	#endif

	  r = ptr_buffer_RAM[0];                // Read R0
	  msg_info->esi = (r >> 31);            // Get Error Status Indicator flag
	  if ((r & (1U << 30)) != 0U) {         // If Extended ID
	    msg_info->id = ((r      ) & 0x1FFFFFFFU) | ARM_CAN_ID_IDE_Msk;
	  } else {                              // If Standard ID
	    msg_info->id = ((r >> 18) & 0x7FFU);
	  }
	  if ((r & (1U << 29)) != 0U) {         // If RTR message
	    msg_info->rtr = 1U;
	  } else {                              // If Data message
	    msg_info->rtr = 0U;
	  }
	  r = ptr_buffer_RAM[1];                // Read R1
	  msg_info->edl = (r >> 21) &  1U;      // Get Extended Data Length flag
	  msg_info->brs = (r >> 20) &  1U;      // Get Baud Rate Switching flag
	  msg_info->dlc = (r >> 16) & 15U;      // Get Data Length Code

	  size = msg_info->dlc;

	  if (size > 0U) {
	    memcpy (data, (uint8_t *)(ptr_buffer_RAM + 2U), size);
	  }

	    // Acknowledge message received with Rx FIFO
	    if (fifo_num == 0U) {
	      ptr_CAN->MCAN_RXF0A = (ptr_CAN->MCAN_RXF0S & MCAN_RXF0S_F0GI_Msk) >> MCAN_RXF0S_F0GI_Pos;
	    } else {
	      ptr_CAN->MCAN_RXF1A = (ptr_CAN->MCAN_RXF1S & MCAN_RXF1S_F1GI_Msk) >> MCAN_RXF1S_F1GI_Pos;
	    }


	  return ((int32_t)size);
}

int32_t MCAN_MessageRead (Mcan *ptr_CAN, uint32_t obj_idx, ARM_CAN_MSG_INFO *msg_info, uint8_t *data, uint8_t size, MCAN_DRV_CONFIG_t can_conf, MCAN_RAM_PTR_t can_ram, uint32_t ram_len, uint8_t isFdMode) {

  uint32_t *ptr_buffer_RAM;
  uint32_t *cache_addr;
  uint32_t  r, cache_size;
  int32_t datafieldsize;

  if (obj_idx >= can_conf.RX_TOT_OBJ_NUM)                         { return ARM_DRIVER_ERROR_PARAMETER; }

  if (obj_idx < can_conf.RX_FIFO_OBJ_IDX_MAX) {
    // Read message received with Rx FIFO
    if (obj_idx == 0U) {
      datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.RX_FIFO0_ELEM_SIZE];
      ptr_buffer_RAM = can_ram.PTR_RX_FIFO0 + (((ptr_CAN->MCAN_RXF0S & MCAN_RXF0S_F0GI_Msk) >> MCAN_RXF0S_F0GI_Pos) * datafieldsize);
    } else {
      datafieldsize = MCAN_DATAFIELD_SIZE_DECODE[_can_config_sizes.RX_FIFO1_ELEM_SIZE];
      ptr_buffer_RAM = can_ram.PTR_RX_FIFO1 + (((ptr_CAN->MCAN_RXF1S & MCAN_RXF1S_F1GI_Msk) >> MCAN_RXF1S_F1GI_Pos) * datafieldsize);
    }
  } else {
    // Read message received with Rx Buffer
    ptr_buffer_RAM = can_ram.PTR_RX_BUFFER + ((obj_idx - can_conf.RX_FIFO_OBJ_NUM) * 18U);
  }

#if (defined(__DCACHE_PRESENT) && (__DCACHE_PRESENT == 1U))
  if ((SCB->CCR & SCB_CCR_DC_Msk) != 0U) {      // If Data Cache is enabled
    cache_addr = (uint32_t *)((uint32_t)ptr_buffer_RAM & (~0x1FU));
    cache_size = (uint32_t)ptr_buffer_RAM + size - (uint32_t)cache_addr;
    SCB_InvalidateDCache_by_Addr(cache_addr, cache_size);
  }
#endif

  r = ptr_buffer_RAM[0];                // Read R0
  msg_info->esi = (r >> 31);            // Get Error Status Indicator flag
  if ((r & (1U << 30)) != 0U) {         // If Extended ID
    msg_info->id = ((r      ) & 0x1FFFFFFFU) | ARM_CAN_ID_IDE_Msk;
  } else {                              // If Standard ID
    msg_info->id = ((r >> 18) & 0x7FFU);
  }
  if ((r & (1U << 29)) != 0U) {         // If RTR message
    msg_info->rtr = 1U;
  } else {                              // If Data message
    msg_info->rtr = 0U;
  }
  r = ptr_buffer_RAM[1];                // Read R1
  msg_info->edl = (r >> 21) &  1U;      // Get Extended Data Length flag
  msg_info->brs = (r >> 20) &  1U;      // Get Baud Rate Switching flag
  msg_info->dlc = (r >> 16) & 15U;      // Get Data Length Code
  if (msg_info->rtr == 0U) {            // If Data message read data bytes
    if ((msg_info->edl == 1U) && (isFdMode != 0U)) {       // If FD mode and Extended Data Length bit active
      if (size > MCAN_DLC_ENCODE[msg_info->dlc]) {
        size = MCAN_DLC_ENCODE[msg_info->dlc];
      }
    } else {
      if (size > msg_info->dlc) {       // If not FD mode or not Extended Data Length bit active
        size = msg_info->dlc;
      }
      if (size > 8U) {
        size = 8U;
      }
    }
  } else {
    size = 0U;
  }

  if (size > 0U) {
    memcpy (data, (uint8_t *)(ptr_buffer_RAM + 2U), size);
  }

  if (obj_idx < can_conf.RX_FIFO_OBJ_IDX_MAX) {
    // Acknowledge message received with Rx FIFO
    if (obj_idx == 0U) {
      ptr_CAN->MCAN_RXF0A = (ptr_CAN->MCAN_RXF0S & MCAN_RXF0S_F0GI_Msk) >> MCAN_RXF0S_F0GI_Pos;
    } else {
      ptr_CAN->MCAN_RXF1A = (ptr_CAN->MCAN_RXF1S & MCAN_RXF1S_F1GI_Msk) >> MCAN_RXF1S_F1GI_Pos;
    }
  } else {
    // Clear new data flag and release Rx buffer
    obj_idx -= can_conf.RX_FIFO_OBJ_NUM;
    if (obj_idx < 32U) {
      ptr_CAN->MCAN_NDAT1 = 1 <<  obj_idx;
    } else {
      ptr_CAN->MCAN_NDAT2 = 1 << (obj_idx - 32U);
    }
  }

  return ((int32_t)size);
}

void MCAN_ConfigureRam(Mcan *ptr_CAN, MCan_IntrTypeMode obj_cfg, uint32_t interruptType)
{
	switch (obj_cfg) {
	    case CAN_INTRTYPEMODE_DISABLE:
	    	ptr_CAN->MCAN_IE &= ~interruptType;
	    break;
	    case CAN_INTRTYPEMODE_ENABLE:
	    	ptr_CAN->MCAN_IE |= interruptType;
	}
}


