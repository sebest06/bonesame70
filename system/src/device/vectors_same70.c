/*
 * This file is part of the µOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2014 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// ----------------------------------------------------------------------------

#include "ExceptionHandlers.h"
#include <same70q21b.h>

// ----------------------------------------------------------------------------

void Default_Handler(void);

void Dummy_Handler(void);

// Forward declaration of the specific IRQ handlers. These are aliased
// to the Default_Handler, which is a 'forever' loop. When the application
// defines a handler (with the same name), this will automatically take
// precedence over these weak definitions
//

/* Cortex-M7 core handlers */
void NonMaskableInt_Handler ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void HardFault_Handler    ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void MemoryManagement_Handler ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void BusFault_Handler     ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void UsageFault_Handler   ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void SVCall_Handler       ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void DebugMonitor_Handler ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void PendSV_Handler       ( void ) __attribute__ ((weak, alias("Dummy_Handler")));
void SysTick_Handler      ( void ) __attribute__ ((weak, alias("Dummy_Handler")));


/* Peripherals handlers */
void SUPC_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void RSTC_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void RTC_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void RTT_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void WDT_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void PMC_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void EFC_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void UART0_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
void UART1_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
void PIOA_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void PIOB_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_PIOC_INSTANCE_
void PIOC_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_PIOC_INSTANCE_ */
#ifdef _SAME70_USART0_INSTANCE_
void USART0_Handler ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_USART0_INSTANCE_ */
#ifdef _SAME70_USART1_INSTANCE_
void USART1_Handler ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_USART1_INSTANCE_ */
#ifdef _SAME70_USART2_INSTANCE_
void USART2_Handler ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_USART2_INSTANCE_ */
void PIOD_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_PIOE_INSTANCE_
void PIOE_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_PIOE_INSTANCE_ */
#ifdef _SAME70_HSMCI_INSTANCE_
void HSMCI_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_HSMCI_INSTANCE_ */
void TWIHS0_Handler ( void ) __attribute__ ((weak, alias("Default_Handler")));
void TWIHS1_Handler ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_SPI0_INSTANCE_
void SPI0_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_SPI0_INSTANCE_ */
void SSC_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void TC0_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void TC1_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void TC2_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_TC1_INSTANCE_
void TC3_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_TC1_INSTANCE_ */
#ifdef _SAME70_TC1_INSTANCE_
void TC4_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_TC1_INSTANCE_ */
#ifdef _SAME70_TC1_INSTANCE_
void TC5_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_TC1_INSTANCE_ */
void AFEC0_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_DACC_INSTANCE_
void DACC_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_DACC_INSTANCE_ */
void PWM0_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void ICM_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void ACC_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void USBHS_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
void MCAN0_INT0_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void MCAN0_INT1_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_MCAN1_INSTANCE_
void MCAN1_INT0_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void MCAN1_INT1_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_MCAN1_INSTANCE_ */
void GMAC_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void AFEC1_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_TWIHS2_INSTANCE_
void TWIHS2_Handler ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_TWIHS2_INSTANCE_ */
#ifdef _SAME70_SPI1_INSTANCE_
void SPI1_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_SPI1_INSTANCE_ */
void QSPI_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void UART2_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_UART3_INSTANCE_
void UART3_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_UART3_INSTANCE_ */
#ifdef _SAME70_UART4_INSTANCE_
void UART4_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_UART4_INSTANCE_ */
#ifdef _SAME70_TC2_INSTANCE_
void TC6_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_TC2_INSTANCE_ */
#ifdef _SAME70_TC2_INSTANCE_
void TC7_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_TC2_INSTANCE_ */
#ifdef _SAME70_TC2_INSTANCE_
void TC8_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_TC2_INSTANCE_ */
void TC9_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void TC10_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void TC11_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void AES_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void TRNG_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void XDMAC_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
void ISI_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void PWM1_Handler   ( void ) __attribute__ ((weak, alias("Default_Handler")));
void FPU_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_SDRAMC_INSTANCE_
void SDRAMC_Handler ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_SDRAMC_INSTANCE_ */
void RSWDT_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
void CCW_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void CCF_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
void GMAC_Q1_Handler     ( void ) __attribute__ ((weak, alias("Default_Handler")));
void GMAC_Q2_Handler     ( void ) __attribute__ ((weak, alias("Default_Handler")));
void IXC_Handler    ( void ) __attribute__ ((weak, alias("Default_Handler")));
#ifdef _SAME70_I2SC0_INSTANCE_
void I2SC0_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_I2SC0_INSTANCE_ */
#ifdef _SAME70_I2SC1_INSTANCE_
void I2SC1_Handler  ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif /* _SAME70_I2SC1_INSTANCE_ */
#if (__SAM_M7_REVB == 1)
void GMAC_Q3_Handler     ( void ) __attribute__ ((weak, alias("Default_Handler")));
void GMAC_Q4_Handler     ( void ) __attribute__ ((weak, alias("Default_Handler")));
void GMAC_Q5_Handler     ( void ) __attribute__ ((weak, alias("Default_Handler")));
#endif

// ----------------------------------------------------------------------------

extern unsigned int _estack;

typedef void
(* const pHandler)(void);

// ----------------------------------------------------------------------------

// The vector table.
// This relies on the linker script to place at correct location in memory.

//#define VECTOR_CMSIS_DEFAULT

#ifdef VECTOR_CMSIS_DEFAULT
__attribute__ ((section(".isr_vector"),used))
pHandler __isr_vectors[] =
  { //
		(pHandler) &_estack,                          // The initial stack pointer
        Reset_Handler,                            // The reset handler

        NMI_Handler,                              // The NMI handler
        HardFault_Handler,                        // The hard fault handler

#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
        MemManage_Handler,                        // The MPU fault handler
        BusFault_Handler,// The bus fault handler
        UsageFault_Handler,// The usage fault handler
#else
        0, 0, 0,				  // Reserved
#endif
        0,                                        // Reserved
        0,                                        // Reserved
        0,                                        // Reserved
        0,                                        // Reserved
        SVC_Handler,                              // SVCall handler
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
        DebugMon_Handler,                         // Debug monitor handler
#else
        0,					  // Reserved
#endif
        0,                                        // Reserved
        PendSV_Handler,                           // The PendSV handler
        SysTick_Handler,                          // The SysTick handler

        // ----------------------------------------------------------------------
        // microchip vectors
		SUPC_Handler,   /* 0  Supply Controller */
		RSTC_Handler,   /* 1  Reset Controller */
		RTC_Handler,    /* 2  Real Time Clock */
		RTT_Handler,    /* 3  Real Time Timer */
		WDT_Handler,    /* 4  Watchdog Timer */
		PMC_Handler,    /* 5  Power Management Controller */
		EFC_Handler,    /* 6  Enhanced Embedded Flash Controller */
		UART0_Handler,  /* 7  UART 0 */
		UART1_Handler,  /* 8  UART 1 */
		(0UL),          /* 9  Reserved */
		PIOA_Handler,   /* 10 Parallel I/O Controller A */
		PIOB_Handler,   /* 11 Parallel I/O Controller B */
		#ifdef _SAME70_PIOC_INSTANCE_
		PIOC_Handler,   /* 12 Parallel I/O Controller C */
		#else
		(0UL),          /* 12 Reserved */
		#endif /* _SAME70_PIOC_INSTANCE_ */
		#ifdef _SAME70_USART0_INSTANCE_
		USART0_Handler, /* 13 USART 0 */
		#else
		(0UL),          /* 13 Reserved */
		#endif /* _SAME70_USART0_INSTANCE_ */
		#ifdef _SAME70_USART1_INSTANCE_
		USART1_Handler, /* 14 USART 1 */
		#else
		(0UL),          /* 14 Reserved */
		#endif /* _SAME70_USART1_INSTANCE_ */
		#ifdef _SAME70_USART2_INSTANCE_
		USART2_Handler, /* 15 USART 2 */
		#else
		(0UL),          /* 15 Reserved */
		#endif /* _SAME70_USART2_INSTANCE_ */
		PIOD_Handler,   /* 16 Parallel I/O Controller D */
		#ifdef _SAME70_PIOE_INSTANCE_
		PIOE_Handler,   /* 17 Parallel I/O Controller E */
		#else
		(0UL),          /* 17 Reserved */
		#endif /* _SAME70_PIOE_INSTANCE_ */
		#ifdef _SAME70_HSMCI_INSTANCE_
		HSMCI_Handler,  /* 18 Multimedia Card Interface */
		#else
		(0UL),          /* 18 Reserved */
		#endif /* _SAME70_HSMCI_INSTANCE_ */
		TWIHS0_Handler, /* 19 Two Wire Interface 0 HS */
		TWIHS1_Handler, /* 20 Two Wire Interface 1 HS */
		#ifdef _SAME70_SPI0_INSTANCE_
		SPI0_Handler,   /* 21 Serial Peripheral Interface 0 */
		#else
		(0UL),          /* 21 Reserved */
		#endif /* _SAME70_SPI0_INSTANCE_ */
		SSC_Handler,    /* 22 Synchronous Serial Controller */
		TC0_Handler,    /* 23 Timer/Counter 0 */
		TC1_Handler,    /* 24 Timer/Counter 1 */
		TC2_Handler,    /* 25 Timer/Counter 2 */
		#ifdef _SAME70_TC1_INSTANCE_
		TC3_Handler,    /* 26 Timer/Counter 3 */
		#else
		(0UL),          /* 26 Reserved */
		#endif /* _SAME70_TC1_INSTANCE_ */
		#ifdef _SAME70_TC1_INSTANCE_
		TC4_Handler,    /* 27 Timer/Counter 4 */
		#else
		(0UL),          /* 27 Reserved */
		#endif /* _SAME70_TC1_INSTANCE_ */
		#ifdef _SAME70_TC1_INSTANCE_
		TC5_Handler,    /* 28 Timer/Counter 5 */
		#else
		(0UL),          /* 28 Reserved */
		#endif /* _SAME70_TC1_INSTANCE_ */
		AFEC0_Handler,  /* 29 Analog Front End 0 */
		#ifdef _SAME70_DACC_INSTANCE_
		DACC_Handler,   /* 30 Digital To Analog Converter */
		#else
		(0UL),          /* 30 Reserved */
		#endif /* _SAME70_DACC_INSTANCE_ */
		PWM0_Handler,   /* 31 Pulse Width Modulation 0 */
		ICM_Handler,    /* 32 Integrity Check Monitor */
		ACC_Handler,    /* 33 Analog Comparator */
		USBHS_Handler,  /* 34 USB Host / Device Controller */
		MCAN0_INT0_Handler, /* 35 Controller Area Network */
		MCAN0_INT1_Handler, /* 36 Controller Area Network */
		#ifdef _SAME70_MCAN1_INSTANCE_
		MCAN1_INT0_Handler, /* 37 Controller Area Network */
		MCAN1_INT1_Handler, /* 38 Controller Area Network */
		#else
		(0UL),          /* 37 Reserved */
		(0UL),          /* 38 Reserved */
		#endif /* _SAME70_MCAN1_INSTANCE_ */
		GMAC_Handler,   /* 39 Ethernet MAC */
		AFEC1_Handler,  /* 40 Analog Front End 1 */
		#ifdef _SAME70_TWIHS2_INSTANCE_
		TWIHS2_Handler, /* 41 Two Wire Interface 2 HS */
		#else
		(0UL),          /* 41 Reserved */
		#endif /* _SAME70_TWIHS2_INSTANCE_ */
		#ifdef _SAME70_SPI1_INSTANCE_
		SPI1_Handler,   /* 42 Serial Peripheral Interface 1 */
		#else
		(0UL),          /* 42 Reserved */
		#endif /* _SAME70_SPI1_INSTANCE_ */
		QSPI_Handler,   /* 43 Quad I/O Serial Peripheral Interface */
		UART2_Handler,  /* 44 UART 2 */
		#ifdef _SAME70_UART3_INSTANCE_
		UART3_Handler,  /* 45 UART 3 */
		#else
		(0UL),          /* 45 Reserved */
		#endif /* _SAME70_UART3_INSTANCE_ */
		#ifdef _SAME70_UART4_INSTANCE_
		UART4_Handler,  /* 46 UART 4 */
		#else
		(0UL),          /* 46 Reserved */
		#endif /* _SAME70_UART4_INSTANCE_ */
		#ifdef _SAME70_TC2_INSTANCE_
		TC6_Handler,    /* 47 Timer/Counter 6 */
		#else
		(0UL),          /* 47 Reserved */
		#endif /* _SAME70_TC2_INSTANCE_ */
		#ifdef _SAME70_TC2_INSTANCE_
		TC7_Handler,    /* 48 Timer/Counter 7 */
		#else
		(0UL),          /* 48 Reserved */
		#endif /* _SAME70_TC2_INSTANCE_ */
		#ifdef _SAME70_TC2_INSTANCE_
		TC8_Handler,    /* 49 Timer/Counter 8 */
		#else
		(0UL),          /* 49 Reserved */
		#endif /* _SAME70_TC2_INSTANCE_ */
		TC9_Handler,    /* 50 Timer/Counter 9 */
		TC10_Handler,   /* 51 Timer/Counter 10 */
		TC11_Handler,   /* 52 Timer/Counter 11 */
		(0UL),          /* 53 Reserved */
		(0UL),          /* 54 Reserved */
		(0UL),          /* 55 Reserved */
		AES_Handler,    /* 56 AES */
		TRNG_Handler,   /* 57 True Random Generator */
		XDMAC_Handler,  /* 58 DMA */
		ISI_Handler,    /* 59 Camera Interface */
		PWM1_Handler,   /* 60 Pulse Width Modulation 1 */
		FPU_Handler,    /* 61 Floating Point Unit Registers */
		#ifdef _SAME70_SDRAMC_INSTANCE_
		SDRAMC_Handler, /* 62 SDRAM Controller */
		#else
		(0UL),          /* 62 Reserved */
		#endif /* _SAME70_SDRAMC_INSTANCE_ */
		RSWDT_Handler,  /* 63 Reinforced Secure Watchdog Timer */
		CCW_Handler,    /* 64 System Control Registers */
		CCF_Handler,    /* 65 System Control Registers */
		GMAC_Q1_Handler,/* 66 Gigabit Ethernet MAC */
		GMAC_Q2_Handler,/* 67 Gigabit Ethernet MAC */
		IXC_Handler,    /* 68 Floating Point Unit Registers */
		#ifdef _SAME70_I2SC0_INSTANCE_
		I2SC0_Handler,  /* 69 Inter-IC Sound controller */
		#else
		(0UL),          /* 69 Reserved */
		#endif /* _SAME70_I2SC0_INSTANCE_ */
		#ifdef _SAME70_I2SC1_INSTANCE_
		I2SC1_Handler,  /* 70 Inter-IC Sound controller */
		#else
		(0UL),          /* 70 Reserved */
		#endif /* _SAME70_I2SC1_INSTANCE_ */
		#if (__SAM_M7_REVB == 1)
		GMAC_Q3_Handler,/* 71 Gigabit Ethernet MAC */
		GMAC_Q4_Handler,/* 72 Gigabit Ethernet MAC */
		GMAC_Q5_Handler /* 73 Gigabit Ethernet MAC */
		#else
		(0UL),          /* 71 Reserved */
		(0UL),          /* 72 Reserved */
		(0UL)           /* 73 Reserved */
		#endif
    };

#else

/* Exception Table */
__attribute__ ((section(".isr_vector")))
const DeviceVectors exception_table = {

        /* Configure Initial Stack Pointer, using linker-generated symbols */
        .pvStack = (void*) (&_estack),

        .pfnReset_Handler              = (void*) Reset_Handler,
        .pfnNonMaskableInt_Handler     = (void*) NonMaskableInt_Handler,
        .pfnHardFault_Handler          = (void*) HardFault_Handler,
        .pfnMemoryManagement_Handler   = (void*) MemoryManagement_Handler,
        .pfnBusFault_Handler           = (void*) BusFault_Handler,
        .pfnUsageFault_Handler         = (void*) UsageFault_Handler,
        .pvReservedC9                  = (void*) (0UL), /* Reserved */
        .pvReservedC8                  = (void*) (0UL), /* Reserved */
        .pvReservedC7                  = (void*) (0UL), /* Reserved */
        .pvReservedC6                  = (void*) (0UL), /* Reserved */
        .pfnSVCall_Handler             = (void*) SVCall_Handler,
        .pfnDebugMonitor_Handler       = (void*) DebugMonitor_Handler,
        .pvReservedC3                  = (void*) (0UL), /* Reserved */
        .pfnPendSV_Handler             = (void*) PendSV_Handler,
        .pfnSysTick_Handler            = (void*) SysTick_Handler,

        /* Configurable interrupts */
        .pfnSUPC_Handler               = (void*) SUPC_Handler,   /* 0  Supply Controller */
        .pfnRSTC_Handler               = (void*) RSTC_Handler,   /* 1  Reset Controller */
        .pfnRTC_Handler                = (void*) RTC_Handler,    /* 2  Real-time Clock */
        .pfnRTT_Handler                = (void*) RTT_Handler,    /* 3  Real-time Timer */
        .pfnWDT_Handler                = (void*) WDT_Handler,    /* 4  Watchdog Timer */
        .pfnPMC_Handler                = (void*) PMC_Handler,    /* 5  Power Management Controller */
        .pfnEFC_Handler                = (void*) EFC_Handler,    /* 6  Embedded Flash Controller */
        .pfnUART0_Handler              = (void*) UART0_Handler,  /* 7  Universal Asynchronous Receiver Transmitter */
        .pfnUART1_Handler              = (void*) UART1_Handler,  /* 8  Universal Asynchronous Receiver Transmitter */
        .pvReserved9                   = (void*) (0UL),          /* 9  Reserved */
        .pfnPIOA_Handler               = (void*) PIOA_Handler,   /* 10 Parallel Input/Output Controller */
        .pfnPIOB_Handler               = (void*) PIOB_Handler,   /* 11 Parallel Input/Output Controller */
        .pfnPIOC_Handler               = (void*) PIOC_Handler,   /* 12 Parallel Input/Output Controller */
        .pfnUSART0_Handler             = (void*) USART0_Handler, /* 13 Universal Synchronous Asynchronous Receiver Transmitter */
        .pfnUSART1_Handler             = (void*) USART1_Handler, /* 14 Universal Synchronous Asynchronous Receiver Transmitter */
        .pfnUSART2_Handler             = (void*) USART2_Handler, /* 15 Universal Synchronous Asynchronous Receiver Transmitter */
        .pfnPIOD_Handler               = (void*) PIOD_Handler,   /* 16 Parallel Input/Output Controller */
        .pfnPIOE_Handler               = (void*) PIOE_Handler,   /* 17 Parallel Input/Output Controller */
        .pfnHSMCI_Handler              = (void*) HSMCI_Handler,  /* 18 High Speed MultiMedia Card Interface */
        .pfnTWIHS0_Handler             = (void*) TWIHS0_Handler, /* 19 Two-wire Interface High Speed */
        .pfnTWIHS1_Handler             = (void*) TWIHS1_Handler, /* 20 Two-wire Interface High Speed */
        .pfnSPI0_Handler               = (void*) SPI0_Handler,   /* 21 Serial Peripheral Interface */
        .pfnSSC_Handler                = (void*) SSC_Handler,    /* 22 Synchronous Serial Controller */
        .pfnTC0_Handler                = (void*) TC0_Handler,    /* 23 Timer Counter */
        .pfnTC1_Handler                = (void*) TC1_Handler,    /* 24 Timer Counter */
        .pfnTC2_Handler                = (void*) TC2_Handler,    /* 25 Timer Counter */
        .pfnTC3_Handler                = (void*) TC3_Handler,    /* 26 Timer Counter */
        .pfnTC4_Handler                = (void*) TC4_Handler,    /* 27 Timer Counter */
        .pfnTC5_Handler                = (void*) TC5_Handler,    /* 28 Timer Counter */
        .pfnAFEC0_Handler              = (void*) AFEC0_Handler,  /* 29 Analog Front-End Controller */
        .pfnDACC_Handler               = (void*) DACC_Handler,   /* 30 Digital-to-Analog Converter Controller */
        .pfnPWM0_Handler               = (void*) PWM0_Handler,   /* 31 Pulse Width Modulation Controller */
        .pfnICM_Handler                = (void*) ICM_Handler,    /* 32 Integrity Check Monitor */
        .pfnACC_Handler                = (void*) ACC_Handler,    /* 33 Analog Comparator Controller */
        .pfnUSBHS_Handler              = (void*) USBHS_Handler,  /* 34 USB High-Speed Interface */
        .pfnMCAN0_INT0_Handler         = (void*) MCAN0_INT0_Handler, /* 35 Controller Area Network */
        .pfnMCAN0_INT1_Handler         = (void*) MCAN0_INT1_Handler, /* 36 Controller Area Network */
        .pfnMCAN1_INT0_Handler         = (void*) MCAN1_INT0_Handler, /* 37 Controller Area Network */
        .pfnMCAN1_INT1_Handler         = (void*) MCAN1_INT1_Handler, /* 38 Controller Area Network */
        .pfnGMAC_Handler               = (void*) GMAC_Handler,   /* 39 Gigabit Ethernet MAC */
        .pfnAFEC1_Handler              = (void*) AFEC1_Handler,  /* 40 Analog Front-End Controller */
        .pfnTWIHS2_Handler             = (void*) TWIHS2_Handler, /* 41 Two-wire Interface High Speed */
        .pfnSPI1_Handler               = (void*) SPI1_Handler,   /* 42 Serial Peripheral Interface */
        .pfnQSPI_Handler               = (void*) QSPI_Handler,   /* 43 Quad Serial Peripheral Interface */
        .pfnUART2_Handler              = (void*) UART2_Handler,  /* 44 Universal Asynchronous Receiver Transmitter */
        .pfnUART3_Handler              = (void*) UART3_Handler,  /* 45 Universal Asynchronous Receiver Transmitter */
        .pfnUART4_Handler              = (void*) UART4_Handler,  /* 46 Universal Asynchronous Receiver Transmitter */
        .pfnTC6_Handler                = (void*) TC6_Handler,    /* 47 Timer Counter */
        .pfnTC7_Handler                = (void*) TC7_Handler,    /* 48 Timer Counter */
        .pfnTC8_Handler                = (void*) TC8_Handler,    /* 49 Timer Counter */
        .pfnTC9_Handler                = (void*) TC9_Handler,    /* 50 Timer Counter */
        .pfnTC10_Handler               = (void*) TC10_Handler,   /* 51 Timer Counter */
        .pfnTC11_Handler               = (void*) TC11_Handler,   /* 52 Timer Counter */
        .pvReserved53                  = (void*) (0UL),          /* 53 Reserved */
        .pvReserved54                  = (void*) (0UL),          /* 54 Reserved */
        .pvReserved55                  = (void*) (0UL),          /* 55 Reserved */
        .pfnAES_Handler                = (void*) AES_Handler,    /* 56 Advanced Encryption Standard */
        .pfnTRNG_Handler               = (void*) TRNG_Handler,   /* 57 True Random Number Generator */
        .pfnXDMAC_Handler              = (void*) XDMAC_Handler,  /* 58 Extensible DMA Controller */
        .pfnISI_Handler                = (void*) ISI_Handler,    /* 59 Image Sensor Interface */
        .pfnPWM1_Handler               = (void*) PWM1_Handler,   /* 60 Pulse Width Modulation Controller */
        .pfnFPU_Handler                = (void*) FPU_Handler,    /* 61 Floating Point Unit Registers */
        .pfnSDRAMC_Handler             = (void*) SDRAMC_Handler, /* 62 SDRAM Controller */
        .pfnRSWDT_Handler              = (void*) RSWDT_Handler,  /* 63 Reinforced Safety Watchdog Timer */
        .pfnCCW_Handler                = (void*) CCW_Handler,    /* 64 System Control Registers */
        .pfnCCF_Handler                = (void*) CCF_Handler,    /* 65 System Control Registers */
        .pfnGMAC_Q1_Handler            = (void*) GMAC_Q1_Handler, /* 66 Gigabit Ethernet MAC */
        .pfnGMAC_Q2_Handler            = (void*) GMAC_Q2_Handler, /* 67 Gigabit Ethernet MAC */
        .pfnIXC_Handler                = (void*) IXC_Handler     /* 68 Floating Point Unit Registers */
};
#endif
// ----------------------------------------------------------------------------

// Processor ends up here if an unexpected interrupt occurs or a specific
// handler is not present in the application code.

void /*__attribute__ ((section(".after_vectors")))*/
Default_Handler(void)
{
  while (1)
    {
    }
}

/**
 * \brief Default interrupt handler for unused IRQs.
 */
void Dummy_Handler(void)
{
        while (1) {
        }
}

// ----------------------------------------------------------------------------
